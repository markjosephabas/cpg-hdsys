import { useSession, signOut } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState, useEffect } from "react";
// import routes from "./utils/routes-list"
import { BiLogOut } from "react-icons/bi";
// import RouteIcons from "./utils/route-icons";
import {
  DashboardOutlined,
  CarOutlined,
  FileSearchOutlined,
  LineChartOutlined,
  UserOutlined,
  BellOutlined,
  DownOutlined,
  FileDoneOutlined,
  AreaChartOutlined,
  CaretRightOutlined,
  ExperimentOutlined,
  TransactionOutlined,
  MonitorOutlined,
  LogoutOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from "@ant-design/icons";

import {
  Spin,
  Menu,
  Avatar,
  Row,
  Col,
  Dropdown,
  Space,
  Badge,
  Button,
} from "antd";
interface Props {
  children: React.ReactNode;
}

import { Layout } from "antd";

const { Content, Footer, Sider, Header } = Layout;

const DashboardLayout = ({ children }: Props) => {
  const router = useRouter();
  const session = useSession();

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // Simulate a delay of 2 seconds to show the spinner
    setTimeout(() => setLoading(false), 300);
  }, []);

  function getItem(label: any, key: any, icon: any, children: any) {
    return {
      key,
      icon,
      label,
      children,
    };
  }
  const [openKeys, setOpenKeys] = useState<string[]>([]);
  const renderSideBarItems = (): React.ReactNode => {
    const permission = session?.data?.user?.permission;

    const clerk = [
      getItem(
        <Link href={"/dashboard/admin-dashboard"}>
          <a>Dashboard</a>
        </Link>,
        "/dashboard",
        <DashboardOutlined size={20} />,
        null
      ),

      getItem("Hemodialysis", "1", <FileSearchOutlined size={20} />, [
        getItem(
          <Link href={"/dashboard/beneficiary-transaction"}>
            <a>Session</a>
          </Link>,
          "/dashboard/beneficiary-transaction",
          <FileSearchOutlined size={20} />,
          null
        ),
        getItem(
          <Link href={"/dashboard/session_transaction"}>
            <a>Transactions</a>
          </Link>,
          "/dashboard/session_transaction",
          <FileSearchOutlined size={20} />,
          null
        ),
        getItem(
          <Link href={"/dashboard/beneficiary"}>
            <a>Beneficiaries</a>
          </Link>,
          "/dashboard/beneficiary",
          <UserOutlined size={20} />,
          null
        ),
        getItem(
          <Link href={"/dashboard/items"}>
            <a>Items</a>
          </Link>,
          "/dashboard/items",
          <FileDoneOutlined size={20} />,
          null
        ),
        getItem(
          "Statement",
          "sub-statement",
          <FileSearchOutlined size={20} />,
          [
            getItem(
              <Link href={"/dashboard/statement"}>
                <a>Create</a>
              </Link>,
              "/dashboard/statement",
              null,
              null
            ),
            getItem(
              <Link href={"/dashboard/statement_list"}>
                <a>View</a>
              </Link>,
              "/dashboard/statement_list",
              null,
              null
            ),
          ]
        ),
      ]),
    ];
    const laboratory_user = [
      getItem("Census", "2", <FileSearchOutlined size={20} />, [
        getItem(
          <Link href={"/sensus/sensus_transaction_logs"}>
            <a>Transactions</a>
          </Link>,
          "/sensus/sensus_transaction_logs",
          <FileSearchOutlined size={20} />,
          null
        ),
        getItem(
          <Link href={"/sensus/patients"}>
            <a>Patients</a>
          </Link>,
          "/sensus/patients",
          <UserOutlined size={20} />,
          null
        ),
        getItem(
          <Link href={"/sensus/sensus_report"}>
            <a>Report</a>
          </Link>,
          "/sensus/sensus_report",
          <FileSearchOutlined size={20} />,
          null
        ),
        // getItem("Philhealth", "phic_1", <FileSearchOutlined size={20} />, [
        //   getItem(
        //     <Link href={"/sensus/sensus_transaction"}>
        //       <a>Create</a>
        //     </Link>,
        //     "/sensus/sensus_transaction",
        //     null,
        //     null
        //   ),
        //   getItem(
        //     <Link href={"/sensus/sensus_transaction_logs"}>
        //       <a>View/Update</a>
        //     </Link>,
        //     "/sensus/sensus_transaction_logs",
        //     null,
        //     null
        //   ),
        // ]),
      ]),
      getItem("Laboratory", "3", <ExperimentOutlined size={20} />, [
        getItem("Transactions", "lab_1", <TransactionOutlined size={20} />, [
          getItem(
            <Link href={"/laboratory/laboratory_transaction"}>
              <a>Create</a>
            </Link>,
            "/laboratory/laboratory_transaction",
            null,
            null
          ),
          getItem(
            <Link href={"/laboratory/laboratory"}>
              <a>View/Update</a>
            </Link>,
            "/laboratory/laboratory",
            null,
            null
          ),
        ]),
        getItem(
          <Link href={"/laboratory/lab_items"}>
            <a>Procedures</a>
          </Link>,
          "/laboratory/lab_items",
          <FileSearchOutlined size={20} />,
          null
        ),
      ]),
      getItem("Radiology", "4", <MonitorOutlined size={20} />, [
        getItem("Transactions", "rad_1", <TransactionOutlined size={20} />, [
          getItem(
            <Link href={"/radiology/radiology_transaction"}>
              <a>Create</a>
            </Link>,
            "/radiology/radiology_transaction",
            null,
            null
          ),
          getItem(
            <Link href={"/radiology/radiology"}>
              <a>View/Update</a>
            </Link>,
            "/radiology/radiology",
            null,
            null
          ),
        ]),
        getItem(
          <Link href={"/radiology/rad_items"}>
            <a>Procedures</a>
          </Link>,
          "/radiology/rad_items",
          <FileSearchOutlined size={20} />,
          null
        ),
      ]),
      getItem(
        <Link href={"/sensus/phic_management"}>
          <a>PhilHealth Management</a>
        </Link>,
        "/sensus/phic_management",
        <FileSearchOutlined size={20} />,
        null
      ),
    ];
    const cpg_admin = [
      getItem(
        <Link href={"/dashboard/admin-dashboard"}>
          <a>Dashboard</a>
        </Link>,
        "/dashboard",
        <DashboardOutlined size={20} />,
        null
      ),
      getItem(
        <Link href={"/dashboard/session_transaction"}>
          <a>Transactions</a>
        </Link>,
        "/dashboard/session_transaction",
        <FileSearchOutlined size={20} />,
        null
      ),
      getItem(
        <Link href={"/dashboard/beneficiary"}>
          <a>Beneficiaries</a>
        </Link>,
        "/dashboard/beneficiary",
        <UserOutlined size={20} />,
        null
      ),
      getItem(
        <Link href={"/dashboard/items"}>
          <a>Items</a>
        </Link>,
        "/dashboard/items",
        <FileDoneOutlined size={20} />,
        null
      ),
      getItem(
        <Link href={"/dashboard/transaction-reports"}>
          <a>Reports</a>
        </Link>,
        "/dashboard/transaction-reports",
        <LineChartOutlined size={20} />,
        null
      ),
    ];
    const admin = [
      getItem(
        <Link href={"/dashboard/admin-dashboard"}>
          <a>Dashboard</a>
        </Link>,
        "/dashboard",
        <DashboardOutlined size={20} />,
        null
      ),
      getItem("Hemodialysis", "4", <MonitorOutlined size={20} />, [
        getItem(
          <Link href={"/dashboard/session_transaction"}>
            <a>Transactions</a>
          </Link>,
          "/dashboard/session_transaction",
          null,
          null
        ),
        getItem(
          <Link href={"/dashboard/items"}>
            <a>Items</a>
          </Link>,
          "/dashboard/items",
          null,
          null
        ),
        getItem(
          <Link href={"/dashboard/beneficiary"}>
            <a>Beneficiaries</a>
          </Link>,
          "/dashboard/beneficiary",
          null,
          null
        ),
        getItem(
          <Link href={"/dashboard/statement_admin"}>
            <a>Statement</a>
          </Link>,
          "/dashboard/statement_admin",
          null,
          null
        ),
        getItem(
          <Link href={"/report/summary_report"}>
            <a>Summary</a>
          </Link>,
          "/report/summary_report",
          null,
          null
        ),
      ]),
      getItem("Census", "census_1", <TransactionOutlined size={20} />, [
        getItem(
          <Link href={"/sensus/sensus_transaction_logs"}>
            <a>Transactions</a>
          </Link>,
          "/sensus/sensus_transaction_logs",
          null,
          null
        ),
        getItem(
          <Link href={"/sensus/patients"}>
            <a>Patients</a>
          </Link>,
          "/sensus/patients",
          null,
          null
        ),
        getItem(
          <Link href={"/sensus/sensus_report_admin"}>
            <a>Report</a>
          </Link>,
          "/sensus/sensus_report_admin",
          null,
          null
        ),
      ]),
      getItem("Laboratory", "Lab_main", <ExperimentOutlined size={20} />, [
        getItem(
          <Link href={"/laboratory/laboratory"}>
            <a>Transactions</a>
          </Link>,
          "/laboratory/laboratory",
          null,
          null
        ),
        getItem(
          <Link href={"/laboratory/lab_items"}>
            <a>Procedures</a>
          </Link>,
          "/laboratory/lab_items",
          null,
          null
        ),
      ]),
      getItem("Radiology", "Rad_main", <MonitorOutlined size={20} />, [
        getItem(
          <Link href={"/radiology/radiology"}>
            <a>Transactions</a>
          </Link>,
          "/radiology/radiology",
          null,
          null
        ),
        getItem(
          <Link href={"/radiology/rad_items"}>
            <a>Procedures</a>
          </Link>,
          "/radiology/rad_items",
          null,
          null
        ),
      ]),
      getItem(
        <Link href={"/sensus/phic_management"}>
          <a>PhilHealth Management</a>
        </Link>,
        "/sensus/phic_management",
        <FileSearchOutlined size={20} />,
        null
      ),
      getItem("Maintenance", "lab_2", <TransactionOutlined size={20} />, [
        getItem(
          <Link href={"/dashboard/hospital"}>
            <a>Hospital</a>
          </Link>,
          "/dashboard/hospital",
          null,
          null
        ),
        getItem(
          <Link href={"/admin/registration"}>
            <a>User Registration</a>
          </Link>,
          "/admin/registration",
          null,
          null
        ),
        getItem(
          <Link href={"/admin/user_management"}>
            <a>User Update</a>
          </Link>,
          "/admin/user_management",
          null,
          null
        ),
      ]),
    ];

    const items =
      permission === "admin"
        ? admin
        : permission === "clerk"
        ? clerk
        : permission === "laboratory-user"
        ? laboratory_user
        : permission === "cpg-admin"
        ? cpg_admin
        : [];

    const onOpenChange = (keys: string[]) => {
      const rootSubmenuKeys = items
        .filter(item => item.children)
        .map(item => item.key);
      const latestOpenKey = keys.find(key => !openKeys.includes(key));

      if (rootSubmenuKeys.includes(latestOpenKey!)) {
        setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
      } else {
        setOpenKeys(keys);
      }
    };
    return (
      <Menu
        mode="inline"
        theme="dark"
        items={items}
        openKeys={openKeys}
        onOpenChange={onOpenChange}
      />
    );
  };

  const permission = session?.data?.user?.permission;

  const items = [
    {
      label: <a onClick={() => signOut()}>Logout</a>,
      icon: <BiLogOut />,
      key: "item-1",
    }, // remember to pass the key prop
  ];

  const [collapsed, setCollapsed] = useState(false);

  const handleCollapse = (collapsed: any) => {
    setCollapsed(collapsed);
  };
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Layout className="site-layout">
        <Layout>
          <Sider
            width={250}
            breakpoint="sm"
            trigger={null}
            // collapsedWidth={0}
            // theme="light"
            collapsed={collapsed}
            onCollapse={handleCollapse}
            style={{
              position: "fixed",
              left: 0,
              height: "100vh",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              overflow: "auto",
            }}
          >
            <div>
              <div className="flex p-4 text-gray-300 mt-4">Manage </div>
              {renderSideBarItems()}
            </div>
          </Sider>

          <Layout>
            <Header
              style={{
                backgroundColor: "#fff",
                boxShadow: "0 4px 8px rgba(0, 0, 0, 0.06)",
                position: "fixed",
                zIndex: 1,
                width: collapsed ? "calc(100% - 80px)" : "calc(100% - 250px)",
                left: collapsed ? 80 : 250,
                transition: "width 0.2s, left 0.2s",
              }}
            >
              <div
                className="flex justify-between items-center"
                style={{ width: "100%" }}
              >
                {React.createElement(
                  collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: "trigger",
                    onClick: () => setCollapsed(!collapsed),
                  }
                )}
                <p className="font-bold text-center md:text-4xl ml-5">
                  {permission === "admin"
                    ? "ADMIN"
                    : session.data?.user?.municipality.toUpperCase()}
                </p>
                {/* <div className="  pb-5 mb-5 mt-10 leading-tight t  justify-center items-center">
                  {React.createElement(
                    collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                    {
                      className: "trigger",
                      onClick: () => setCollapsed(!collapsed),
                    }
                  )}
                  <p className="font-bold text-center md:text-4xl ml-5">
                    {permission === "admin"
                      ? "ADMIN"
                      : session.data?.user?.municipality.toUpperCase()}
                  </p>
                </div> */}
                <div className=" items-center ml-auto">
                  <Space>
                    <Badge count={0} offset={[-30, 10]} size="small">
                      <Avatar
                        icon={<BellOutlined />}
                        style={{
                          backgroundColor: "transparent",
                          color: "gray",
                          cursor: "pointer",
                        }}
                      />
                    </Badge>

                    <Dropdown menu={{ items }} trigger={["click"]}>
                      <Avatar
                        style={{
                          backgroundColor: "transparent",
                          color: "gray",
                          cursor: "pointer",
                        }}
                        icon={
                          <>
                            <UserOutlined />
                            <DownOutlined />
                          </>
                        }
                      />
                    </Dropdown>
                  </Space>
                </div>
              </div>
            </Header>
            <Content
              style={{
                padding: 44,
                minHeight: 280,
                marginTop: "50px",
                // marginLeft: "250px",
                // width: collapsed ? "calc(100% - 80px)" : "calc(100% - 250px)",
                marginLeft: collapsed ? 80 : 250,
                transition: "margin-left 0.2s",
              }}
            >
              <Row gutter={[16, 16]}>
                <Col span={24}>{loading ? <Spin /> : children}</Col>
              </Row>
            </Content>
            <Footer style={{ textAlign: "center" }}>
              Cebu Provincial Government ©2023 Version 1.0
            </Footer>
          </Layout>
        </Layout>
      </Layout>
    </Layout>
  );
};

export default DashboardLayout;
