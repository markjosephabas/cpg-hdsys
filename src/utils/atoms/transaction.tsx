import { atom } from "recoil";

export const transaction = atom({
  key: "transaction",
  default: {
    modalOpen: false,
    percentage: 0,
    trans_id: 0,
  },
});
