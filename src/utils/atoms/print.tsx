import { atom } from "recoil";

export const printItems = atom({
  key: "print",
  default: {
    date: "",
    decryptedOtp: "",
    inputtedOtp: ""
  },
});
