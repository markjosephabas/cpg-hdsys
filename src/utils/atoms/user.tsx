import { atom } from "recoil";

export const approvedUsers = atom({
  key: "approvedUsers",
  default: {
    isModalOpen: false,
    isPrintOpen: false,
    trans_type: "",
    trans_id: "",
    doctor:"",
    decryptedOtp: "",
    inputtedOtp: "",
    successOtp: false,
    userInfo: {
      id: null,
      first_name: null,
      last_name: null,
      address: null,
      email: null,
      image: null,
      mobile: null,
      status: null,
      wallet_balance: null,
      item_list: null,
    },
  },
});

