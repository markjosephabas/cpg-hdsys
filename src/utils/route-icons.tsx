import {
  BiUser,
  BiQuestionMark,
  BiFile,
  BiHomeAlt,
  BiEdit,
  BiTimeFive,
  BiCheckCircle,
  BiBarChart,
  BiReceipt,
} from "react-icons/bi";

interface RouteIconsProps {
  label: string | null | undefined;
}

const RouteIcons = ({ label }: RouteIconsProps): React.ReactElement => {
  const getIcons = () => {
    switch (label) {
      case "Dashboard":
        return <BiHomeAlt size={20} className="mr-2" />;
      case "Encode Tax Declaration":
        return <BiEdit size={20} className="mr-2" />;
      case "Pending Tax Declaration":
        return <BiTimeFive size={20} className="mr-2" />;
      case "Paid Tax Declaration":
        return <BiCheckCircle size={20} className="mr-2" />;
      case "Tax Declaration":
        return <BiFile size={20} className="mr-2" />;
      case "Reports":
        return <BiBarChart size={20} className="mr-2" />;
      case "User Registration":
        return <BiUser size={20} className="mr-2" />;
      case "User Management":
        return <BiUser size={20} className="mr-2" />;  
      case "Remittance":
        return <BiReceipt size={20} className="mr-2" />;
      default:
        return <BiQuestionMark size={20} />;
    }
  };

  return <div>{getIcons()}</div>;
};

export default RouteIcons;
