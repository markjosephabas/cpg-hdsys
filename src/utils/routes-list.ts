interface RouteItem {
  label: string;
  route: string;
}

interface RouteConfig {
  admin: Array<RouteItem>;
  LGU: Array<RouteItem>;
  TREASURY: Array<RouteItem>;
}

const routesList: RouteConfig = {
  admin: [
    { label: "Dashboard", route: "/dashboard" },
    { label: "Tax Declaration", route: "/treasury/declarations" },
    { label: "Reports", route: "/treasury/reports" },
    { label: "User Registration", route: "/admin/registration" },
    { label: "User Management", route: "/admin/user_management" },
    { label: "Remittance", route: "/lgu/remittance" },
  ],
  TREASURY: [
    { label: "Dashboard", route: "/dashboard" },
    { label: "Tax Declaration", route: "/treasury/declarations" },
    { label: "Reports", route: "/treasury/reports" },
    { label: "Remittance", route: "/lgu/remittance" },
  ],
  LGU: [
    { label: "Dashboard", route: "/dashboard" },
    { label: "Encode Tax Declaration", route: "/lgu/taxdec" },
    { label: "Tax Declaration", route: "/treasury/declarations" },
    { label: "Pending Tax Declaration", route: "/lgu/pending" },
    { label: "Paid Tax Declaration", route: "/lgu/paid" },
    { label: "Remittance", route: "/lgu/remittance" },
    
  ],
  
};

export default routesList;
