import CryptoES from "crypto-es";
import C from "crypto-js"


export const encryptText = (message) => {
  const encrypted = CryptoES.AES.encrypt(message, "kI1d4ObVI2WhZiWBO6pdj4upd70bBvp6").toString();
  return encrypted
}

export const decryptText = (encrypted) => {
  //import C = require("crypto-js");
  var Decrypted = C.AES.decrypt(encrypted, "kI1d4ObVI2WhZiWBO6pdj4upd70bBvp6");
  var result = Decrypted.toString(C.enc.Utf8);
  return result
}
