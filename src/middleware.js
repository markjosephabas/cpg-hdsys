export { default } from "next-auth/middleware"

export const config = { matcher: ["/dashboard/:path*", "/laboratory/:path*", "/radiology/:path*", "/admin/:path*", "/sensus/:path*"] }
