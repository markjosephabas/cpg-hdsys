import type { NextPage } from "next";

import SignIn from "./auth/credentials-signin";

const Home: NextPage = () => {

  return (
    <>
      <main>
        <SignIn />
      </main>
    </>
  );
};

export default Home;
