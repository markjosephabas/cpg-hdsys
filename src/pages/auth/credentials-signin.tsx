import { getCsrfToken, signIn, useSession } from "next-auth/react";
import { Alert, Button, Divider, Form, Input, Spin, Row, Col } from "antd";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";

import {
  UserOutlined,
  LockOutlined,
  FacebookOutlined,
  InstagramOutlined,
  TwitterOutlined,
} from "@ant-design/icons";
interface Payload {
  csrfToken: string;
  username: string;
  password: string;
}

export default function SignIn({ csrfToken }: any) {
  const [error, setError] = useState(false);
  const router = useRouter();
  const { status } = useSession();
  const session = useSession();
  const [form] = Form.useForm();

  // console.log(session)

  if (status === "authenticated") {
    router.push("/dashboard/admin-dashboard");
  }

  const [loading, setLoading] = useState(false);

  const handleSubmit = async (payload: Payload) => {
    setLoading(true);
    const login = await signIn("credentials", { ...payload, redirect: false });
    if (login?.error) {
      setError(true);
      setLoading(false);
      return;
    }
  };

  if (status === "unauthenticated") {
    return (
      <Row className="login-container">
        <Col sm={12}>
          <div className="login-form">
            <div style={{ marginBottom: "20px" }}>
              <h1 className="text-center text-4xl font-extrabold py-2 text-green-800">
                CPG
                <span className="block text-xl font-extrabold text-green-800">
                  HEALTH CENSUS SYSTEM
                </span>
              </h1>
              <Divider>
                <div className="text-center text-1xl font-extrabold text-green-800">
                  SIGN IN
                </div>
              </Divider>
            </div>
            {/* <img src="/capitol.png" style={{ height: "120px" }} /> */}
            <Form
              initialValues={{ remember: true }}
              form={form}
              onFinish={handleSubmit}
            >
              <Form.Item
                name="csrfToken"
                initialValue={csrfToken}
                className="hidden"
              >
                <Input autoFocus />
              </Form.Item>
              <Form.Item
                name="username"
                rules={[
                  { required: true, message: "Please input your username!" },
                ]}
              >
                <Input
                  placeholder="Username"
                  autoFocus
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  style={{
                    height: "50px",
                    width: "300px",
                  }}
                  className="text-green-800 border-green-800"
                />
              </Form.Item>

              <Form.Item
                name="password"
                rules={[
                  { required: true, message: "Please input your password!" },
                ]}
              >
                <Input.Password
                  placeholder="Password"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  style={{
                    height: "50px",
                    width: "300px",
                  }}
                  className="text-green-800 border-green-800"
                />
              </Form.Item>
              {error && (
                <Form.Item>
                  <Alert
                    // closable
                    message={`Sign in failed. Check the details you provided are correct.`}
                    type="error"
                    showIcon
                    style={{
                      height: "50px",
                      width: "300px",
                    }}
                  />
                </Form.Item>
              )}
              <Form.Item>
                <Button
                  // type="primary"
                  htmlType="submit"
                  style={{
                    height: "50px",
                    width: "300px",
                    color: "white",
                  }}
                  className="custom-button"
                >
                  {loading ? <Spin /> : "Login"}
                </Button>
              </Form.Item>
              <div className="mt-4">
                {/* Ant Design social media icons */}
                <a
                  href="#"
                  className="text-gray-500 hover:text-indigo-500 mx-2"
                >
                  <FacebookOutlined />
                </a>
                <a
                  href="#"
                  className="text-gray-500 hover:text-indigo-500 mx-2"
                >
                  <TwitterOutlined />
                </a>
                <a
                  href="#"
                  className="text-gray-500 hover:text-indigo-500 mx-2"
                >
                  <InstagramOutlined />
                </a>
              </div>
            </Form>
          </div>
        </Col>
        <Col sm={12} className="background-image">
          <img src="/LoginBG.svg" alt="Background" />
        </Col>
      </Row>
    );
  }

  return null;
}
export async function getServerSideProps(ctx: any) {
  const csrfToken = await getCsrfToken(ctx);

  if (!csrfToken) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {
      csrfToken: csrfToken || null,
    },
  };
}
