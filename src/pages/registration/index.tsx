import {
    Button,
    DatePicker,
    Form,
    Input,
    Upload,
    message,
    Select,
    Spin,
  } from "antd";
  import type { NextPage } from "next";
  import Head from "next/head";
  import { trpc } from "../../utils/trpc";
  import type { FormInstance } from "antd/es/form";
  import { useEffect, useRef, useState } from "react";
  import { useSession } from "next-auth/react";
  import { useRouter } from 'next/router'

  
  const Home: NextPage = () => {
    const formRef = useRef<FormInstance>();
    const session = useSession();
    const router = useRouter()

    //const office_id :any = session?.data?.user?.id

    const [form] = Form.useForm();
    const [submitted, setSubmitted] = useState<boolean>(false);
    const [image, setImage] = useState<any>(null);
    const [loading, setLoading] = useState<any>(false);

    //const { data: office, isLoading: officeLoading } = trpc.useQuery(["registration.get_office", session?.data?.user?.username]);

    // const insertUser = trpc.useMutation("registration.insert", {
    //   onSuccess() {
    //     message.success("Registered successfully!");
    //     form.resetFields();
    //     setSubmitted(true);
    //     setImage(null);
    //   },
    // });
    const handleFinishForm = async (values: any) => {
      console.log("values",values)
      setLoading(true);
      // insertUser.mutate({
      //   ...values,
      //   image: 'no image',
      //   assisting_office: session?.data?.user?.username
      // });
      // setLoading(false);
    };
  
    return (
      <>
        <Head>
          <title>Tax Declaration Registration</title>
          <meta name="description" content="Tax Declaration Registration" />
          <link rel="icon" href="../favicon.ico" />
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet"
          />
        </Head>
  
        <main className="flex flex-col h-screen items-center justify-center">
          <span className="mb-10 text-2xl font-bold text-blue-800">
          Tax Declaration Registration
          </span>
          <div className="">
            {!submitted ? (
              <Form form={form} layout="vertical" onFinish={handleFinishForm}>
                <div className="flex gap-2">
                  <Form.Item
                    name="first_name"
                    label="First name"
                    required
                    rules={[
                      {
                        required: true,
                        message: "First name is required",
                      },
                    ]}
                  >
                    <Input placeholder="First name" />
                  </Form.Item>
                </div>
                <div className="flex gap-2">
                  <Form.Item
                    name="last_name"
                    label="Last name"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Last name is required",
                      },
                    ]}
                  >
                    <Input placeholder="Last name" />
                  </Form.Item>
                </div>
                <div className="flex gap-2">
                  <Form.Item
                    name="middle_name"
                    label="Middle name"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Middle name is required",
                      },
                    ]}
                  >
                    <Input placeholder="Middle name" />
                  </Form.Item>
                  <Form.Item
                    name="birthday"
                    label="Birthday"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Birthday is required",
                      },
                    ]}
                  >
                    <DatePicker />
                  </Form.Item>
                </div>
                <div className="flex gap-2">
                  <Form.Item
                    name="gender"
                    label="Gender"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Gender is required",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value="male" key="male">
                        Male
                      </Select.Option>
                      <Select.Option value="female" key="female">
                        Female
                      </Select.Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="email"
                    label="Email"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Email is required",
                      },
                    ]}
                  >
                    <Input placeholder="Email" />
                  </Form.Item>
                </div>
                <div className="flex gap-2">
                  <Form.Item
                    name="address"
                    label="Address"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Address is required",
                      },
                    ]}
                  >
                    <Input placeholder="Address" />
                  </Form.Item>
                  <Form.Item
                    name="mobile"
                    label="Mobile number"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Mobile is required",
                      },
                    ]}
                  >
                    <Input placeholder="Mobile number" />
                  </Form.Item>
                </div>
                {/* <Button
                  disabled={insertUser.isLoading || loading}
                  htmlType="submit"
                  type="primary"
                  className="w-full mt-5"
                >
                  {insertUser.isLoading || loading ? (
                    <>
                      <Spin /> <span>Please wait..</span>
                    </>
                  ) : (
                    "Submit"
                  )}
                </Button> */}
              </Form>
            ) : (
              <div className="text-center">
                <span>Your registration has been submitted.</span>
                <Button
                  onClick={() => {
                    setSubmitted(false)
                    router.push("/dashboard/approved-users")
                  }}
                  type="primary"
                  className="w-full mt-10"
                >
                  Go back
                </Button>
              </div>
            )}
          </div>
        </main>
      </>
    );
  };
  
  export default Home;
  