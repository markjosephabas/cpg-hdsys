import {
  Button,
  Table,
  Tag,
  Popconfirm,
  Tooltip,
  Modal,
  Input,
  DatePicker,
  Select,
  Space,
  Row,
  Col,
  Form,
  Card,
} from "antd";
import { NextPage } from "next";
import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import {
  DownloadOutlined,
  SearchOutlined,
  FilterOutlined,
} from "@ant-design/icons";
import { useSession } from "next-auth/react";
import * as XLSX from "xlsx";
import moment, { Moment } from "moment";
import React, { useState, useEffect } from "react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import { CSVLink, CSVDownload } from "react-csv";

const Reports: NextPage = () => {
  const session = useSession();
  const [dateRange, setDateRange] = useState<
    [moment.Moment | null, moment.Moment | null]
  >([null, null]);
  const [dateRange2, setDateRange2] = useState<
    [moment.Moment | null, moment.Moment | null]
  >([null, null]);
  const [form] = Form.useForm();
  const startDate2 = dateRange2[0]
    ? moment(dateRange2[0]).format("YYYY-MM-DD")
    : "";
  const endDate2 = dateRange2[1]
    ? moment(dateRange2[1]).format("YYYY-MM-DD")
    : "";

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const { Option } = Select;

  const {
    data: data2,
    isLoading: loading2,
    refetch: refetch2,
  } = trpc.useQuery(["taxdec.getHospital"]);

  const handleDateRangeChange = (dates: any) => {
    setDateRange(dates);
  };

  const handleCheck = () => {
    dateRange ? setDateRange2(dateRange) : setDateRange2([null, null]);
  };

  const [classPatient, setclassPatient] = useState("");
  const [selectHospital, setSelectHospital] = useState("");

  const handleFinishForm = async (values: any) => {
    dateRange ? setDateRange2(dateRange) : setDateRange2([null, null]);
    setclassPatient(values.class);
    setSelectHospital(values.municipality);
    // insertRemmitance.mutate({
    //   date_from: startDate2,
    //   date_to: endDate2,
    //   municipality: office,
    // });
    // console.log("values:", values);
  };

  return (
    <div>
      <div>
        <div className="flex p-3 bg-custom-blue mb-5 rounded flex-col fade-in">
          <Row>
            <Col span={24}>
              <span className="uppercase text-2xl font-bold text-white">
                STATEMENTS REPORT
              </span>
            </Col>
          </Row>
          <Space>
            <Form
              form={form}
              onFinish={handleFinishForm}
              layout="vertical"
              className="w-full"
            >
              <Row gutter={12} className="mt-5">
                <Col>
                  <Form.Item
                    name="Date"
                    label={
                      <span className="text-white font-bold">SELECT DATES</span>
                    }
                    required
                    rules={[
                      {
                        required: true,
                        message: "Date is required",
                      },
                    ]}
                  >
                    <DatePicker.RangePicker
                      value={dateRange}
                      format="YYYY-MM-DD"
                      onChange={handleDateRangeChange}
                    />
                  </Form.Item>
                </Col>
                <Col>
                  <Form.Item
                    name="municipality"
                    label={
                      <span className="text-white font-bold">
                        SELECT HOSPITAL
                      </span>
                    }
                    // label="Hospital"
                    required
                    rules={[
                      {
                        required: true,
                        message: "hospital is required",
                      },
                    ]}
                  >
                    <Select
                      style={{ width: "250px" }}
                      showSearch
                      optionFilterProp="children"
                      onChange={value => console.log(`Selected: ${value}`)}
                      filterOption={(input: any, option: any) =>
                        option.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      {data2 &&
                        data2.map((item: any, index: any) => (
                          <Option
                            value={item.hospitalName}
                            key={item.hospitalName}
                          >
                            {item.hospitalName}
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col>
                  <Form.Item
                    name="class"
                    label={
                      <span className="text-white font-bold">SELECT CLASS</span>
                    }
                    required
                    rules={[
                      {
                        required: true,
                        message: "Class is required",
                      },
                    ]}
                    initialValue="Beneficiary (Indigent/POS/PWD)"
                  >
                    <Select style={{ width: "300px" }}>
                      <Select.Option value="Paying" key="Paying">
                        Paying
                      </Select.Option>
                      <Select.Option
                        value="Beneficiary (Indigent/POS/PWD)"
                        key="Beneficiary (Indigent/POS/PWD)"
                      >
                        Indigent
                      </Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col>
                  <Form.Item
                    name=""
                    label={<span className="text-white font-bold">ACTION</span>}
                  >
                    <Button
                      htmlType="submit"
                      // onClick={handleCheck}
                      className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
                    >
                      SUBMIT
                    </Button>
                  </Form.Item>
                </Col>
                {/* <Form.Item name="">
                <Button
                  onClick={handleCheck}
                  className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
                >
                  DOWNLOAD
                </Button>
              </Form.Item> */}

                <div className="w-full">
                  {dateRange2.includes(null) ? null : (
                    <Details
                      data={dateRange2}
                      class={classPatient}
                      hospital={selectHospital}
                    />
                  )}
                </div>
              </Row>
            </Form>
          </Space>
        </div>

        {/* <div className="mb-5">
          Select Dates: &nbsp;
          <DatePicker.RangePicker
            value={dateRange}
            onChange={handleDateRangeChange}
            format="YYYY-MM-DD"
          />
          <Select style={{ width: "300px" }}>
            <Select.Option value="Paying" key="Paying">
              Paying
            </Select.Option>
            <Select.Option
              value="Beneficiary (Indigent/POS/PWD)"
              key="Beneficiary (Indigent/POS/PWD)"
            >
              Beneficiary (Indigent/POS/PWD)
            </Select.Option>
          </Select>
          <Button onClick={handleCheck}>DISPLAY</Button>
          <br></br>
          <Details data={dateRange2} />
        </div> */}
      </div>
    </div>
  );
};
const Details = (props: any) => {
  //   props.data.includes(null) ? console.log("0") : console.log("1");
  // const session = useSession();
  const startDate2 = props.data[0]
    ? moment(props.data[0]).format("YYYY-MM-DD")
    : "";
  const endDate2 = props.data[1]
    ? moment(props.data[1]).format("YYYY-MM-DD")
    : "";
  const hospital = props.hospital;
  const classPatient = props.class;
  const {
    data,
    isLoading: loading,
    refetch,
  } = trpc.useQuery([
    "taxdec.getStatementSummaryByDates",
    {
      startDate: startDate2,
      endDate: endDate2,
      hospital: hospital,
      class: classPatient,
    },
  ]);

  const printDiv = () => {
    const divToPrint = document?.getElementById("printMe");

    if (!divToPrint) {
      return;
    }

    const printWindow = window.open("", "_blank", "height=800,width=1200");

    printWindow?.document.write(
      "<html><head><title>Print</title><style>table {font-size:12px;border-collapse:collapse;} p{margin:0} #deleteBtn {display:none}</style></head><body>"
    );

    // Clone the content to preserve the original content
    const clonedContent = divToPrint.cloneNode(true);
    printWindow?.document.body.appendChild(clonedContent);

    printWindow?.document.write("</body></html>");
    printWindow?.document.close();
    setTimeout(() => {
      printWindow?.print();
    }, 500);
  };

  function formatNumberExcel(number: any) {
    const decimal = number.toString().split(".")[1];

    console.log(number);
  }

  const billAmount = 3500;

  //   const totalAmount =
  //     data2?.reduce((total, currentItem) => {
  //       // Iterate over each item in sessions_itemUsed and sum their unitPrice * quantity
  //       const itemTotal = currentItem.sessions_itemUsed.reduce((acc, session) => {
  //         return acc + session.unitPrice * session.quantity;
  //       }, 0);

  //       // Add the total of unitPrice * quantity for the current beneficiary to the overall total
  //       return total + itemTotal;
  //     }, 0) ?? 0;
  const totalAmount = data?.reduce(
    (total, currentItem) =>
      total + billAmount * currentItem.sessionCount.beneficiary_id,
    0
  );
  const totalPH = data?.reduce(
    (total, currentItem) => total + (currentItem.sum.philhealth_deduction ?? 0),
    0
  );

  const totalAICS = data?.reduce(
    (total, currentItem) => total + (currentItem.sum.aics_deduction ?? 0),
    0
  );

  const totalMAIP = data?.reduce(
    (total, currentItem) => total + (currentItem.sum.maep_deduction ?? 0),
    0
  );

  const totalCash = data?.reduce(
    (total, currentItem) => total + (currentItem.sum.cash ?? 0),
    0
  );

  const totalMidtronics = data?.reduce(
    (total, currentItem) =>
      total + 1850 * currentItem.sessionCount.beneficiary_id,
    0
  );

  const totalQfs = data?.reduce(
    (total, currentItem) => total + (currentItem.sum.qfs ?? 0),
    0
  );

  const divContainerStyle: React.CSSProperties = {
    display: "flex",
    justifyContent: "space-between", // Distribute space between the two divs
    width: "100%", // Use the full width of the container
  };

  const divStyleLeft: React.CSSProperties = {
    borderTop: "2px solid black",
    width: "200px",
    textAlign: "center",
    marginTop: "50px",
  };

  const divStyleRight: React.CSSProperties = {
    borderTop: "2px solid black",
    width: "200px",
    textAlign: "center",
    marginTop: "50px",
    float: "right",
  };

  // // console.log(data);
  // function formatNumber(number: any) {
  //   if (number === undefined || number === null) {
  //     return ""; // return an empty string if number is undefined or null
  //   } else if (Number.isInteger(number)) {
  //     return number.toLocaleString(); // format as integer if no decimal places
  //   } else {
  //     return number.toLocaleString("en-US", { minimumFractionDigits: 2 }); // format as decimal with two decimal places and grouping separators
  //   }
  // }

  const headers = [
    { label: "Name", key: "Name" },
    { label: "Sessions", key: "Sessions" },
    { label: "Amount", key: "amount" },
    { label: "PHIC", key: "phic" },
    { label: "AICS", key: "aics" },
    { label: "MAIP", key: "maip" },
    { label: "PCSO", key: "pcso" },
    { label: "BALANCE AFTER ASSISTANCE", key: "bas" },
    { label: "QFS", key: "qfs" },
    { label: "MEDTRONICS", key: "medtronics" },
    { label: "PROVINCE SHARE", key: "ps" },
  ];

  const medtronicsValue = 1850;
  // Function to transform data into CSV-compatible format
  const transformDataToCSVFormat = (data: any) => {
    return data.map((item: any) => ({
      Name: `${item.lastName.toUpperCase()} , ${item.firstName.toUpperCase()} ${
        item.middleName ? item.middleName.toUpperCase() : item.middleName
      }`,
      Sessions: item.sessionCount.beneficiary_id,
      amount: (billAmount * item.sessionCount.beneficiary_id).toLocaleString(
        undefined,
        {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        }
      ),
      phic: item.sum.philhealth_deduction.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      aics: item.sum.aics_deduction
        ? item.sum.aics_deduction.toLocaleString(undefined, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
        : 0.0,
      maip: item.sum.maep_deduction
        ? item.sum.maep_deduction.toLocaleString(undefined, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
        : 0.0,
      pcso: item.cash
        ? item.sum.cash.toLocaleString(undefined, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
        : 0.0,
      bas: (
        billAmount * item.sessionCount.beneficiary_id -
        item.sum.philhealth_deduction -
        (item.sum.aics_deduction ?? 0) -
        (item.sum.maep_deduction ?? 0) -
        (item.sum.cash ?? 0)
      ).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      qfs: item.qfs
        ? item.sum.qfs.toLocaleString(undefined, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
        : 0.0,
      medtronics: (
        medtronicsValue * item.sessionCount.beneficiary_id
      ).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      ps:
        classPatient === "Paying"
          ? (
              billAmount * item.sessionCount.beneficiary_id +
              item.sum.philhealth_deduction +
              (item.sum.aics_deduction ?? 0) +
              (item.sum.maep_deduction ?? 0) +
              (item.sum.cash ?? 0) -
              1850
            ).toLocaleString(undefined, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })
          : (
              item.sum.philhealth_deduction +
              (item.sum.aics_deduction ?? 0) +
              (item.sum.maep_deduction ?? 0) +
              (item.sum.cash ?? 0) -
              1850 * item.sessionCount.beneficiary_id
            ).toLocaleString(undefined, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            }),
    }));
  };

  const calculateTotal = (data: any) => {
    const total = {
      Name: "",
      Sessions: "Total",
      amount: (totalAmount ? totalAmount : 0).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      phic: (totalPH ? totalPH : 0).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      aics: (totalAICS ? totalAICS : 0).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      maip: (totalMAIP ? totalMAIP : 0).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      pcso: (totalCash ? totalCash : 0).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      bas: (
        (totalAmount ?? 0) -
        (totalPH ?? 0) -
        (totalAICS ?? 0) -
        (totalMAIP ?? 0) -
        (totalCash ?? 0)
      ).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      qfs: (totalQfs ? totalQfs : 0).toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      medtronics: totalMidtronics?.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
      ps:
        classPatient === "Paying"
          ? (
              (totalAmount ?? 0) +
              (totalPH ?? 0) +
              (totalAICS ?? 0) +
              (totalMAIP ?? 0) +
              (totalCash ?? 0) -
              (totalQfs ?? 0) -
              (totalMidtronics ?? 0)
            ).toLocaleString(undefined, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })
          : (
              (totalPH ?? 0) +
              (totalAICS ?? 0) +
              (totalMAIP ?? 0) +
              (totalCash ?? 0) -
              (totalQfs ?? 0) -
              (totalMidtronics ?? 0)
            ).toLocaleString(undefined, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            }),
    };
    return total;
  };
  // Your component code...
  const [csvData, setCSVData] = useState<any[]>([]);

  // Once the data is loaded, transform it into CSV format
  useEffect(() => {
    if (data) {
      const csvFormattedData = transformDataToCSVFormat(data);
      const total = calculateTotal(csvFormattedData);
      setCSVData([...csvFormattedData, total]);
    }
  }, [data]);

  return (
    <Row gutter={[16, 16]}>
      <Col span={24}>
        <Card
          // title={
          //   <>
          //     <TagsOutlined /> &nbsp; SUPPLIES LEDGER CARD
          //   </>
          // }
          bordered={true}
        >
          {/* <div className="w-full mb-5" style={{backgroundColor:"#2c2b31", padding:"5px", textAlign:"center"}}>
                  <h1 style={{color:"white"}}>PURCHASE ORDER DETAILS</h1>
              </div> */}
          <Space>
            <Button className="bg-blue-600 text-white" onClick={printDiv}>
              Print
            </Button>
            <CSVLink
              data={csvData}
              headers={headers}
              className="ant-btn bg-black text-white"
              target="_blank"
              filename={`${hospital} hemodialysis summary(${classPatient}) as of ${startDate2} to ${endDate2}.csv`}
            >
              Export
            </CSVLink>
          </Space>

          <div id="printMe">
            <table
              style={{
                width: "100%",
                marginTop: "5px",
              }}
              className="summaryTable"
            >
              <thead>
                <tr>
                  <td
                    colSpan={12}
                    style={{
                      padding: "8px",
                      textAlign: "center",
                      paddingTop: "40px",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <div
                        style={{
                          marginLeft: "-30px",
                          marginTop: "-30px",
                          marginBottom: "-50px",
                        }}
                      >
                        {/* Your image */}
                        <img
                          src="/capitol.png"
                          alt="My Image"
                          width="100"
                          id="logo"
                        />
                      </div>

                      <div style={{ marginRight: "50px" }}>
                        {/* Your text */}
                        <p>REPUBLIC OF THE PHILIPPINES</p>
                        <p>PROVINCE OF CEBU</p>
                        {/* <p>{session.data?.user?.municipality.toUpperCase()}</p> */}
                        {/* <p style={{ fontSize: "5px" }}>Telephone.</p>
                    <p style={{ fontSize: "5px" }}>Email Address.</p> */}
                        <p style={{ fontWeight: "bold", fontSize: "14px" }}>
                          BILLING STATEMENT SUMMARY
                        </p>
                        <p style={{ fontStyle: "normal" }}>
                          <i>
                            <small>(HEMODIALYSIS SYSTEM)</small>
                          </i>
                        </p>
                      </div>
                    </div>
                    <b>
                      {hospital.toUpperCase()}
                      <br></br>
                      {classPatient.toUpperCase()}
                      <br></br>
                      FROM {moment(startDate2)
                        .format("ll")
                        .toUpperCase()} TO{" "}
                      {moment(endDate2).format("ll").toUpperCase()}
                    </b>
                  </td>
                </tr>
                {/* <tr>
                  <td colSpan={4}></td>
                  <td
                    colSpan={4}
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "5%",
                    }}
                  >
                    <b>ASSISTANCE</b>
                  </td>
                </tr> */}
                <tr>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "5%",
                    }}
                  >
                    <b>#</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>NAME</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>SESSIONS</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>AMOUNT</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>PHILHEALTH</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>AICS</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>MAIP</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>PCSO</b>
                    {/* <b>CASH</b> */}
                  </td>

                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "10%",
                    }}
                  >
                    <b>BALANCE AFTER ASSISTANCE</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>QFS</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>MEDTRONICS</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    <b>PROVINCE SHARE</b>
                  </td>
                </tr>
              </thead>
              <tbody>
                {data?.map((item: any, index) => {
                  const totalValue = item.sessions_itemUsed?.reduce(
                    (total: any, currentItem: any) =>
                      total +
                      (currentItem.unitPrice * currentItem.quantity ?? 0),
                    0
                  );
                  const medtronicsValue = 1850;
                  return (
                    <>
                      <tr key={index}>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          {index + 1}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "left",
                          }}
                        >
                          {item.lastName.toUpperCase()} ,{" "}
                          {item.firstName.toUpperCase()}{" "}
                          {item.middleName
                            ? item.middleName.toUpperCase()
                            : item.middleName}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          {item.sessionCount.beneficiary_id}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          ₱{" "}
                          {/* {totalValue.toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          })} */}
                          {(
                            billAmount * item.sessionCount.beneficiary_id
                          ).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          })}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          ₱{" "}
                          {item.sum.philhealth_deduction.toLocaleString(
                            undefined,
                            {
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2,
                            }
                          )}
                          {/* ₱{" "}
                      {item.UnitPrice.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })} */}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          ₱{" "}
                          {item.sum.aics_deduction
                            ? item.sum.aics_deduction.toLocaleString(
                                undefined,
                                {
                                  minimumFractionDigits: 2,
                                  maximumFractionDigits: 2,
                                }
                              )
                            : 0.0}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          ₱{" "}
                          {item.sum.maep_deduction
                            ? item.sum.maep_deduction.toLocaleString(
                                undefined,
                                {
                                  minimumFractionDigits: 2,
                                  maximumFractionDigits: 2,
                                }
                              )
                            : 0.0}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          ₱{" "}
                          {item.cash
                            ? item.sum.cash.toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2,
                              })
                            : 0.0}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          ₱{" "}
                          {(
                            billAmount * item.sessionCount.beneficiary_id -
                            item.sum.philhealth_deduction -
                            (item.sum.aics_deduction ?? 0) -
                            (item.sum.maep_deduction ?? 0) -
                            (item.sum.cash ?? 0)
                          ).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          })}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          ₱{" "}
                          {item.qfs
                            ? item.sum.qfs.toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2,
                              })
                            : 0.0}
                        </td>

                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          ₱{" "}
                          {(
                            medtronicsValue * item.sessionCount.beneficiary_id
                          ).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          })}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            padding: "8px",
                            textAlign: "center",
                          }}
                        >
                          ₱{" "}
                          {classPatient === "Paying"
                            ? (
                                billAmount * item.sessionCount.beneficiary_id +
                                item.sum.philhealth_deduction +
                                (item.sum.aics_deduction ?? 0) +
                                (item.sum.maep_deduction ?? 0) +
                                (item.sum.cash ?? 0) -
                                1850
                              ).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2,
                              })
                            : (
                                item.sum.philhealth_deduction +
                                (item.sum.aics_deduction ?? 0) +
                                (item.sum.maep_deduction ?? 0) +
                                (item.sum.cash ?? 0) -
                                1850 * item.sessionCount.beneficiary_id
                              ).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2,
                              })}
                          {/* ₱{" "}
                          {(
                            item.philhealth_deduction +
                            (item.aics_deduction ?? 0) +
                            (item.maep_deduction ?? 0) +
                            (item.cash ?? 0) -
                            1850
                          ).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          })} */}
                        </td>
                      </tr>
                    </>
                  );
                })}

                <tr>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  ></td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  ></td>

                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    TOTAL
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {(totalAmount ? totalAmount : 0).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {(totalPH ? totalPH : 0).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {(totalAICS ? totalAICS : 0).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {(totalMAIP ? totalMAIP : 0).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {(totalCash ? totalCash : 0).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {(
                      (totalAmount ?? 0) -
                      (totalPH ?? 0) -
                      (totalAICS ?? 0) -
                      (totalMAIP ?? 0) -
                      (totalCash ?? 0)
                    ).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                    {/* {(totalCash ? totalCash : 0).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })} */}
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {(totalQfs ? totalQfs : 0).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>

                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {totalMidtronics?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                    {/* {(totalCash ? totalCash : 0).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })} */}
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {classPatient === "Paying"
                      ? (
                          (totalAmount ?? 0) +
                          (totalPH ?? 0) +
                          (totalAICS ?? 0) +
                          (totalMAIP ?? 0) +
                          (totalCash ?? 0) -
                          (totalQfs ?? 0) -
                          (totalMidtronics ?? 0)
                        ).toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })
                      : (
                          (totalPH ?? 0) +
                          (totalAICS ?? 0) +
                          (totalMAIP ?? 0) +
                          (totalCash ?? 0) -
                          (totalQfs ?? 0) -
                          (totalMidtronics ?? 0)
                        ).toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })}
                    {/* {(
                      (totalPH ?? 0) +
                      (totalAICS ?? 0) +
                      (totalMAIP ?? 0) +
                      (totalCash ?? 0) -
                      (totalMidtronics ?? 0)
                    ).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })} */}
                    {/* {(totalCash ? totalCash : 0).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })} */}
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colSpan={5} style={{ height: "96px" }}>
                    &nbsp;
                  </td>
                </tr>
              </tfoot>
              {/* <tr style={{ border: "0" }}>
                <td colSpan={5}>
                  <br></br>
                  <div style={divContainerStyle}>
                    <div style={divStyleLeft}>
                      <p style={{ marginTop: "5px" }}>Clerk</p>
                    </div>

                    <div style={divStyleRight}>
                      <p style={{ marginTop: "5px" }}>HD Nurse</p>
                    </div>
                  </div>

                  <div style={divContainerStyle}>
                    <div style={divStyleLeft}>
                      <p style={{ marginTop: "5px" }}>Social Worker</p>
                    </div>
                    <div style={divStyleRight}>
                      <p style={{ marginTop: "5px" }}>Chief of Hospital</p>
                    </div>
                    <div style={divStyleLeft}>
                      <p style={{ marginTop: "5px" }}>Billing</p>
                    </div>
                  </div>
                </td>
              </tr> */}
            </table>
          </div>
        </Card>
      </Col>
    </Row>
  );
};

export default Reports;
