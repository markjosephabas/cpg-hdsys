import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  Card,
  Alert,
  Space,
  InputNumber,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";
import type { ColumnsType, ColumnType } from "antd/es/table";
import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality;
  const permission = session.data?.user?.permission;
  //   const queryKey =
  //     permission === "admin"
  //       ? "taxdec.getBenefiaryAll"
  //       : "taxdec.getSensusPatient";

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getRadItems"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);
  const [openAddItem, setopenAddItem] = useState(false);

  const updateLabItems = trpc.useMutation("taxdec.updateLabItems", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const handleUpdate = async (values: any) => {
    try {
      updateLabItems.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  // FOR TABLE
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const handleChange = (pagination: any, filters: any, sorter: any) => {
    setCurrentPage(pagination.current);
    if (pagination.current !== currentPage) {
      setCurrentPage(pagination.current);
    }

    if (pagination.pageSize !== pageSize) {
      setCurrentPage(1); // Reset to the first page when changing page size
      setPageSize(pagination.pageSize);
    }
  };
  const paginationConfig = {
    current: currentPage,
    pageSize: pageSize, // Adjust as needed
    showTotal: (total: any, range: any) =>
      `${range[0]}-${range[1]} of ${total} items`,
  };
  //TABLE

  const handleDelete = (id: number) => {
    deleteLabItems.mutate({
      id: id,
    });
  };

  const deleteLabItems = trpc.useMutation("taxdec.deleteLabItems", {
    onSuccess() {
      message.success("Deleted successfully!");
      refetch();
    },
    onError(error) {
      message.error("Something went wrong!");
    },
  });

  function handleClick(value: any) {
    Modal.confirm({
      title: `Are you sure you want to delete ${value.name}?`,
      onOk() {
        handleDelete(value.id);
      },
      onCancel() {
        // The user clicked Cancel
        // Do nothing or handle the cancel event here
      },
      okButtonProps: {
        className:
          "bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline",
      },
    });
  }

  const insertLabItems = trpc.useMutation("taxdec.insertLabItems", {
    onSuccess(data) {
      message.success("Added successfully!");
      refetch();
      form.resetFields();
      setopenAddItem(false);
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleUpdate2 = async (values: any) => {
    try {
      insertLabItems.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  interface DataType {
    key: string;
    name: string;
    id: number;
    status: string;
    area: string;
    bill_amount: number;
    laboratory_used: { amount: number }[];
    bill_date: string;
    created_at: string;
  }
  const columns: ColumnsType<DataType> = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      align: "center",
      render: (text, record, index) => (currentPage - 1) * pageSize + index + 1,
    },
    {
      title: "Procedure",
      dataIndex: "name",
      key: "name",
      width: 500,
      filterDropdown: ({
        setSelectedKeys,
        selectedKeys,
        confirm,
        clearFilters,
      }: CustomFilterDropdownProps) => (
        <div style={{ padding: 8 }}>
          <Input
            placeholder="Search Procedure"
            value={selectedKeys[0]}
            onChange={e =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => confirm()}
            style={{ marginBottom: 8, display: "block" }}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={() => {
                clearFilters?.();
                confirm();
              }}
              style={{ marginRight: 8 }}
              size="small"
            >
              Reset
            </Button>
            <Button onClick={() => confirm()} size="small">
              Filter
            </Button>
          </div>
        </div>
      ),
      filterIcon: () => (
        <Tooltip title="Search procedure">
          <SearchOutlined />
        </Tooltip>
      ),
      onFilter: (value: any, record: any) => {
        if (!value || value.length < 2) {
          return true;
        }
        return record.name.toLowerCase().includes(value.toLowerCase());
      },
      render: (text: string, record: any) => `${record.name}`.toUpperCase(),
      sorter: (a: any, b: any) => a.name.length - b.name.length,
      sortDirections: ["ascend" as const, "descend" as const],
    },
    {
      title: "Cost",
      dataIndex: "status",
      key: "status",
      align: "center",
      ellipsis: true,
      render: (text: any, record: any) =>
        record.amount.toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        }),
    },
    {
      title: "Actions",
      key: "id",
      align: "center",
      render: (item: any) => {
        return (
          <Space>
            <Tooltip title="Edit">
              <Button
                onClick={() => handleEdit(item)}
                className="bg-blue-600 text-white ml-1"
                shape="circle"
                icon={<FormOutlined />}
              ></Button>
            </Tooltip>
            <Tooltip title="Delete">
              <Button
                onClick={() => handleClick(item)}
                icon={<DeleteOutlined />}
                shape="circle"
                danger
                type="primary"
              />
            </Tooltip>
          </Space>
        );
      },
    },
  ];
  const dataSource =
    data?.map((item: any, index: any) => ({
      ...item,
      key: index,
    })) || [];
  if (permission === "admin") {
    const hospitalColumn: ColumnType<DataType> = {
      title: "Hospital",
      dataIndex: "hospital",
      width: 500,
      key: "hospital",
      align: "center",
      filterDropdown: ({
        setSelectedKeys,
        selectedKeys,
        confirm,
        clearFilters,
      }: CustomFilterDropdownProps) => (
        <div style={{ padding: 8 }}>
          <Input
            placeholder="Search by hospital name"
            value={selectedKeys[0]}
            onChange={e =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => confirm()}
            style={{ marginBottom: 8, display: "block" }}
          />
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <Button
              onClick={() => {
                clearFilters?.();
                confirm();
              }}
              style={{ marginRight: 8 }}
              size="small"
            >
              Reset
            </Button>
            <Button onClick={() => confirm()} size="small">
              Filter
            </Button>
          </div>
        </div>
      ),
      filterIcon: () => (
        <Tooltip title="Search by hospital name">
          <SearchOutlined />
        </Tooltip>
      ),
      onFilter: (value: any, record: any) =>
        record.hospital &&
        record.hospital.toLowerCase().includes(value.toLowerCase()), // Check if hospital property exists
      render: (value: any, record: any) =>
        record.hospital ? record.hospital.toUpperCase() : "", // Check if hospital property exists
      sorter: (a: any, b: any) => a.hospital.length - b.hospital.length,
      sortDirections: ["ascend" as const, "descend" as const],
    };
    // Insert the hospital column at the desired position (e.g., third position)
    columns.splice(3, 2, hospitalColumn);
    // Remove the column at index 8
    // columns.splice(8, 1);
  }

  return (
    <div>
      <Card className="fade-in">
        <div className="flex p-3 bg-custom-blue mb-5 rounded ">
          <span className="uppercase text-2xl font-bold text-white">
            RADIOLOGY PROCEDURE LISTS
          </span>
          <Button
            className="ml-auto text-custom-blue bg-white border-custom-blue flex items-center  rounded-full"
            icon={<PlusOutlined />}
            onClick={() => setopenAddItem(true)}
          >
            CREATE{" "}
          </Button>
        </div>
        <div>
          <Table
            columns={columns}
            size="middle"
            bordered
            dataSource={dataSource}
            onChange={handleChange}
            loading={loading}
            pagination={paginationConfig}
            scroll={{ x: true }}
            // responsive
          />
        </div>
      </Card>
      {/* ADD ITEM     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> ADD ITEM
            </Space>
          </>
        }
        open={openAddItem}
        onCancel={() => setopenAddItem(false)}
        footer={null}
        width={350}
      >
        <Form
          form={form}
          layout="vertical"
          onFinish={handleUpdate2}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24}>
              <Form.Item
                name="name"
                label="Item name"
                required
                rules={[
                  {
                    required: true,
                    message: "Item Name is required",
                  },
                ]}
              >
                <Input placeholder="Item Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="type"
                label="Type"
                required
                rules={[
                  {
                    required: true,
                    message: "Type is required",
                  },
                ]}
                initialValue="RADIOLOGY"
              >
                <Select disabled>
                  {/* <Select.Option
                    value="CLINICAL MICROSCOPY"
                    key="CLINICAL MICROSCOPY"
                  >
                    CLINICAL MICROSCOPY
                  </Select.Option>
                  <Select.Option value="HEMATOLOGY" key="HEMATOLOGY">
                    HEMATOLOGY
                  </Select.Option>
                  <Select.Option value="BLOOD BANK" key="BLOOD BANK">
                    BLOOD BANK
                  </Select.Option>
                  <Select.Option value="IMMUNOLOGY" key="IMMUNOLOGY">
                    IMMUNOLOGY
                  </Select.Option>
                  <Select.Option
                    value="CLINICAL CHEMISTRY"
                    key="CLINICAL CHEMISTRY"
                  >
                    CLINICAL CHEMISTRY
                  </Select.Option>
                  <Select.Option value="MICRO BIOLOGY" key="MICRO BIOLOGY">
                    MICRO BIOLOGY
                  </Select.Option>
                  <Select.Option value="HISTOPATHOLOGY" key="HISTOPATHOLOGY">
                    HISTOPATHOLOGY
                  </Select.Option> */}
                  {/* <Select.Option value="LABORATORY" key="LABORATORY">
                    LABORATORY
                  </Select.Option> */}
                  <Select.Option value="RADIOLOGY" key="RADIOLOGY">
                    RADIOLOGY
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="amount"
                label="Cost"
                required
                rules={[
                  {
                    required: true,
                    message: "Cost is required",
                  },
                ]}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>

            <Col span={24} style={{ marginTop: "20px", textAlign: "center" }}>
              <Button
                // disabled={insertPatient.isLoading}
                htmlType="submit"
                type="primary"
                // loading={insertPatient.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline"
              >
                {insertLabItems.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "SAVE"
                )}
                {/* SAVE */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
      {/* UPDATE ITEM     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> UPDATE ITEM
            </Space>
          </>
        }
        open={openUpdateItem}
        onCancel={() => setopenUpdateItem(false)}
        footer={null}
        width={350}
      >
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleUpdate}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="id"
                label="ID"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="name"
                label="Item Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Item Name is required",
                  },
                ]}
              >
                <Input placeholder="Item Name" />
              </Form.Item>
            </Col>

            <Col span={24}>
              <Form.Item
                name="type"
                label="Type"
                required
                rules={[
                  {
                    required: true,
                    message: "Type is required",
                  },
                ]}
              >
                <Select disabled>
                  {/*<Select.Option
                    value="CLINICAL MICROSCOPY"
                    key="CLINICAL MICROSCOPY"
                  >
                    CLINICAL MICROSCOPY
                  </Select.Option>
                  <Select.Option value="HEMATOLOGY" key="HEMATOLOGY">
                    HEMATOLOGY
                  </Select.Option>
                  <Select.Option value="BLOOD BANK" key="BLOOD BANK">
                    BLOOD BANK
                  </Select.Option>
                  <Select.Option value="IMMUNOLOGY" key="IMMUNOLOGY">
                    IMMUNOLOGY
                  </Select.Option>
                  <Select.Option
                    value="CLINICAL CHEMISTRY"
                    key="CLINICAL CHEMISTRY"
                  >
                    CLINICAL CHEMISTRY
                  </Select.Option>
                  <Select.Option value="MICRO BIOLOGY" key="MICRO BIOLOGY">
                    MICRO BIOLOGY
                  </Select.Option>
                  <Select.Option value="HISTOPATHOLOGY" key="HISTOPATHOLOGY">
                    HISTOPATHOLOGY
              </Select.Option>*/}
                  <Select.Option value="LABORATORY" key="LABORATORY">
                    LABORATORY
                  </Select.Option>
                  {/* <Select.Option value="RADIOLOGY" key="RADIOLOGY">
                    RADIOLOGY
                  </Select.Option> */}
                </Select>
              </Form.Item>
              {/* <div>

              </div> */}
            </Col>
            <Col span={12}>
              <Form.Item
                name="amount"
                label="Cost"
                required
                rules={[
                  {
                    required: true,
                    message: "Cost is required",
                  },
                ]}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>

            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={updateLabItems.isLoading}
                htmlType="submit"
                type="primary"
                loading={updateLabItems.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {updateLabItems.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};

export default PendingTaxDeclaration;
