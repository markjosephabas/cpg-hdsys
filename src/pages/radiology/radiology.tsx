import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  Card,
  Alert,
  Divider,
  Space,
  Statistic,
  DatePicker,
  InputNumber,
} from "antd";
import { NextPage } from "next";
import moment, { Moment } from "moment";
import type { ColumnsType, ColumnType } from "antd/es/table";
import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  FormOutlined,
  DeleteOutlined,
  PlusOutlined,
  TeamOutlined,
  UserOutlined,
  HomeOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";
import TextArea from "antd/lib/input/TextArea";
const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [txtmessage, setMessage] = useState("");
  const permission = session.data?.user?.permission;
  const [dateRange, setDateRange] = useState<
    [moment.Moment | null, moment.Moment | null]
  >([null, null]);
  const {
    data: data3,
    isLoading: loading3,
    refetch: refetch3,
  } = trpc.useQuery(["taxdec.getRadItems"]);

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);

  const updateLabPatient = trpc.useMutation("taxdec.updateLabPatient", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleUpdate = async (values: any) => {
    try {
      updateLabPatient.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const handleDelete = (id: number) => {
    deleteLabTransaction.mutate({
      id: id,
    });
  };

  const deleteLabTransaction = trpc.useMutation("taxdec.deleteLabTransaction", {
    onSuccess() {
      message.success("Deleted successfully!");
      refetch();
    },
    onError(error) {
      message.error("Something went wrong!");
    },
  });

  function handleClick(value: any) {
    Modal.confirm({
      title: `Are you sure you want to delete the transaction associated with  ${value.patients_name}?`,
      onOk() {
        handleDelete(value.id);
      },
      onCancel() {
        // The user clicked Cancel
        // Do nothing or handle the cancel event here
      },
      okButtonProps: {
        className:
          "bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline",
      },
    });
  }

  //TABLE LIVE
  const [filters, setFilters] = useState({
    // status: null,
    startDate: null as string | null,
    endDate: null as string | null,
    name: "",
    class: "",
    area: "",
    hospital: "",
    // location: "",
    page: 1,
    pageSize: 10,
  });

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getRadTranscationLive", { ...filters }]);

  const handleTableChange = (pagination: any) => {
    setFilters({
      ...filters,
      page: pagination.current,
      pageSize: pagination.pageSize,
    });
    refetch();
  };
  const changeHospital = (value: any) => {
    const hospital = value.target.value;
    setFilters({
      ...filters,
      hospital: hospital,
      page: 1,
    });
    refetch();
  };
  const changeFullName = (value: any) => {
    const fullName = value.target.value;
    setFilters({
      ...filters,
      name: fullName,
      page: 1,
    });
    refetch();
  };
  const changeClass = (value: any) => {
    const class_name = value;
    setFilters({
      ...filters,
      class: class_name,
      page: 1,
    });
    refetch();
  };
  const changeArea = (value: any) => {
    const area = value;
    setFilters({
      ...filters,
      area: area,
      page: 1,
    });
    refetch();
  };

  type Transaction = {
    created_at: Date | null;
    bill_date: Date | null;
    name: string;
    id: number;
    hospital: string;
    patients_name: string;
    classification: string;
    area: string;
    bill_amount: number;
    laboratory_used: { amount: number; item_name: string }[];
  };

  type Data = {
    transaction: Transaction[];
    total: number;
  };

  const transactionData = data as Data | undefined;
  const dataSource =
    transactionData?.transaction?.map((item, index) => ({
      ...item,
      key: index,
    })) || [];

  const handleDateRangeChange = (
    dates: [Moment | null, Moment | null] | null,
    formatString: [string, string]
  ) => {
    if (dates) {
      setFilters({
        ...filters,
        startDate: dates[0] ? dates[0].format("YYYY-MM-DD") : "",
        endDate: dates[1] ? dates[1].format("YYYY-MM-DD") : null,
        page: 1,
      });
      refetch();
    } else {
      setFilters({
        ...filters,
        startDate: null,
        endDate: null,
        page: 1,
      });
      refetch();
    }
  };

  //END LIVE

  const columns: ColumnsType<Transaction> = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      align: "center",
      render: (text: any, record: any, index: any) =>
        (filters.page - 1) * filters.pageSize + index + 1,
    },
    {
      title: "Patient",
      dataIndex: "patients_name",
      key: "patients_name",
      render: (text, record) => `${record.patients_name}`.toUpperCase(),
    },
    {
      title: "Class",
      dataIndex: "classification",
      key: "class",
      align: "center",
      ellipsis: true,
      render: (text: any, record: any) =>
        record.status === null ? (
          ""
        ) : record.classification === "Indigent" ? (
          <Tag color="green" className="rounded-full">
            INDIGENT
          </Tag>
        ) : record.classification === "Paying" ? (
          <Tag color="blue" className="rounded-full">
            PAYING
          </Tag>
        ) : record.classification === "Non Philhealth" ? (
          <Tag color="geekblue" className="rounded-full">
            NON PHILHEALTH
          </Tag>
        ) : record.classification === "Philhealth" ? (
          <Tag color="purple" className="rounded-full">
            PHILHEALTH
          </Tag>
        ) : (
          ""
        ),
    },
    {
      title: "Area",
      dataIndex: "area",
      key: "area",
      align: "center",
      render: (text, record) =>
        record.area === null ? (
          record.area
        ) : record.area === "DISCHARGE" ? (
          <Tag color="green">DISCHARGE</Tag>
        ) : record.area === "OPD" ? (
          <Tag color="blue">OPD</Tag>
        ) : record.area === "EMERGENCY" ? (
          <Tag color="purple">EMERGENCY</Tag>
        ) : record.area === "ADMITTED" ? (
          <Tag color="magenta" className="rounded-full">
            ADMITTED
          </Tag>
        ) : record.area === "WALK-IN" ? (
          <Tag color="magenta">WALK-IN</Tag>
        ) : (
          ""
        ),
    },
    {
      title: "Procedure",
      dataIndex: "bill_date",
      key: "bill_date",
      align: "center",
      render: (text, record) => {
        const itemCount = record.laboratory_used.length;
        const itemNames = record.laboratory_used
          .map((item, index) => `${index + 1}. ${item.item_name}`)
          .join("<br>");
        return (
          <Tooltip
            title={<p dangerouslySetInnerHTML={{ __html: itemNames }} />}
          >
            {itemCount > 1
              ? `${itemCount} items`
              : record.laboratory_used[0]?.item_name}
          </Tooltip>
        );
        // return `${itemCount + 1} items`;
      },
    },
    {
      title: "Total Amount",
      dataIndex: "bill_amount",
      key: "bill_amount",
      align: "center",
      render: (text, record) => {
        const totalAmount = record.laboratory_used.reduce(
          (total, item) => total + (item.amount || 0),
          0
        );
        return totalAmount.toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        });
      },
    },
    {
      title: "Date of Bill",
      dataIndex: "bill_date",
      key: "bill_date",
      align: "center",
      render: (text, record) =>
        record.bill_date ? moment(record.bill_date).format("LLL") : "",
    },
    {
      title: "Date Created",
      dataIndex: "created_at",
      key: "created_at",
      align: "center",
      render: (text, record) =>
        record.created_at ? moment(record.created_at).format("LLL") : "",
    },
    {
      title: "Actions",
      key: "id",
      align: "center",
      render: (item: any) => {
        return (
          <Space>
            {/* <Tooltip title="Edit">
              <Button
                onClick={() => handleEdit(item)}
                className="bg-blue-600 text-white ml-1"
                shape="circle"
                icon={<FormOutlined />}
              ></Button>
            </Tooltip> */}
            <Tooltip title="Delete">
              <Button
                onClick={() => handleClick(item)}
                icon={<DeleteOutlined />}
                shape="circle"
                danger
                type="primary"
              />
            </Tooltip>
          </Space>
        );
      },
    },
  ];

  type ColumnType = {
    title: string;
    dataIndex: string;
    key: string;
    align?: "left" | "right" | "center";
    render?: (text: any, record: any) => any;
  };

  if (permission === "admin") {
    const hospitalColumn: ColumnType = {
      title: "Hospital",
      dataIndex: "hospital",

      key: "hospital",
      align: "center",

      render: (value: any, record: any) =>
        record.hospital ? record.hospital.toUpperCase() : "", // Check if hospital property exists
    };
    // Insert the hospital column at the desired position (e.g., third position)
    columns.splice(7, 2, hospitalColumn);
    // Remove the column at index 8
    // columns.splice(8, 1);
  }
  return (
    <div>
      <Card className="fade-in">
        {/* <Divider style={{ marginBottom: "30px" }} orientation="left">
          <Space>LAB TRANSACTION</Space>
        </Divider> */}
        <div className="flex flex-wrap p-3 bg-custom-blue mb-5 rounded ">
          <div className=" flex flex-wrap gap-2 items-center">
            <span className="font-bold text-white text-2xl">
              RADIOLOGY TRANSACTIONS
            </span>
          </div>
        </div>
        <Row gutter={12}>
          <Col md={6} xs={24}>
            <Form layout="vertical">
              <Form.Item
                name="searchname"
                label={
                  <>
                    <UserOutlined className="mr-1" />
                    Name
                  </>
                }
              >
                <Input
                  placeholder="Search name..."
                  onChange={changeFullName}
                  autoComplete="off"
                  allowClear
                />
              </Form.Item>
            </Form>
          </Col>
          {permission === "admin" && (
            <Col md={4} xs={24}>
              <Form layout="vertical">
                <Form.Item
                  name="hospital"
                  label={
                    <>
                      <HomeOutlined className="mr-1" />
                      Hospital
                    </>
                  }
                >
                  <Input
                    placeholder="Search hospital..."
                    onChange={changeHospital}
                    autoComplete="off"
                    allowClear
                    disabled={permission !== "admin"}
                  />
                </Form.Item>
              </Form>
            </Col>
          )}
          <Col md={4} xs={24}>
            <Form layout="vertical">
              <Form.Item
                name="class"
                label={
                  <>
                    <TeamOutlined className="mr-1" />
                    Class
                  </>
                }
              >
                <Select onChange={changeClass}>
                  <Select.Option value="">All Class</Select.Option>
                  <Select.Option value="Indigent">Indigent</Select.Option>
                  <Select.Option value="Paying">Paying</Select.Option>
                  {/* <Select.Option value="POS">POS</Select.Option>
                      <Select.Option value="PWD">PWD</Select.Option> */}
                  <Select.Option value="Non Philhealth">
                    Non Philhealth
                  </Select.Option>
                  <Select.Option value="Philhealth">Philhealth</Select.Option>
                </Select>
              </Form.Item>
            </Form>
          </Col>
          <Col md={4} xs={24}>
            <Form layout="vertical">
              <Form.Item
                name="class"
                label={
                  <>
                    <TeamOutlined className="mr-1" />
                    Area
                  </>
                }
              >
                <Select onChange={changeArea}>
                  <Select.Option value="">ALL AREA</Select.Option>
                  <Select.Option value="DISCHARGE" key="DISCHARGE">
                    DISCHARGE
                  </Select.Option>
                  <Select.Option value="OPD" key="OPD">
                    OPD
                  </Select.Option>
                  <Select.Option value="EMERGENCY" key="EMERGENCY">
                    EMERGENCY
                  </Select.Option>
                  <Select.Option value="ADMITTED" key="ADMITTED">
                    ADMITTED
                  </Select.Option>
                  <Select.Option value="WALK-IN" key="WALK-IN">
                    WALK-IN
                  </Select.Option>
                </Select>
              </Form.Item>
            </Form>
          </Col>
          <Col md={6} xs={24}>
            <Form layout="vertical">
              <Form.Item
                name="hospital"
                label={
                  <>
                    <CalendarOutlined className="mr-1" />
                    Date of Bill Coverage
                  </>
                }
              >
                <DatePicker.RangePicker
                  value={dateRange}
                  onChange={handleDateRangeChange}
                  format="YYYY-MM-DD"
                />
              </Form.Item>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Table
              columns={columns}
              size="middle"
              bordered
              dataSource={dataSource}
              // onChange={handleChange}
              loading={loading}
              pagination={{
                current: filters.page,
                pageSize: filters.pageSize,
                total: transactionData?.total,
                showTotal: (total, range) =>
                  `${range[0]}-${range[1]} of ${total.toLocaleString()} items`,
              }}
              scroll={{ x: true }}
              onChange={handleTableChange}
              // responsive
            />
          </Col>
        </Row>
      </Card>
      {/* UPDATE BENEFICIARY     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> UPDATE TRANSACTION
            </Space>
          </>
        }
        open={openUpdateItem}
        onCancel={() => setopenUpdateItem(false)}
        footer={null}
      >
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleUpdate}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="id"
                label="ID"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>
            <Col md={24} lg={12}>
              <Form.Item
                label="Patient Name"
                required
                name="patients_name"
                rules={[
                  {
                    required: true,
                    message: "Name is required",
                  },
                ]}
              >
                <Input placeholder="Name" readOnly />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="equipment_used"
                label="Lab Equipment Used"
                required
                rules={[
                  {
                    required: true,
                    message: "Classification is required",
                  },
                ]}
              >
                <Select disabled>
                  {data3 &&
                    data3.map(data => (
                      <Select.Option value={data.id} key={data.id}>
                        {/* Your JSX code */}
                        <div>{data.name}</div>
                      </Select.Option>
                    ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name="remarks" label="Remarks">
                <TextArea />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="used"
                label="No. of Used"
                required
                rules={[
                  {
                    required: true,
                    message: "No. of Used is required",
                  },
                ]}
                initialValue={1}
              >
                <InputNumber min={1} style={{ width: "100%" }} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="bill_amount"
                label="Total Bill"
                required
                rules={[
                  {
                    required: true,
                    message: "Total bill is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="bill_date"
                label="Date of Billing"
                required
                rules={[
                  {
                    required: true,
                    message: "Date is required",
                  },
                ]}
              >
                <DatePicker format="YYYY-MM-DD" className="w-full" />
              </Form.Item>
            </Col>
            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={updateLabPatient.isLoading}
                htmlType="submit"
                type="primary"
                loading={updateLabPatient.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {updateLabPatient.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};

export default PendingTaxDeclaration;
