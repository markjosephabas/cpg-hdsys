import { NextPage } from "next";
import { useSession, signOut } from "next-auth/react";
import { trpc } from "../../utils/trpc";
import { useRouter } from "next/router";
import {
  CalendarOutlined,
  DollarCircleOutlined,
  CarOutlined,
  DownloadOutlined,
} from "@ant-design/icons";
import {
  Button,
  DatePicker,
  Form,
  Row,
  Col,
  Descriptions,
  Card,
  Spin,
  Select,
} from "antd";
import React, { useState, useEffect } from "react";
import moment, { Moment } from "moment";
import * as XLSX from "xlsx";

const Dashboard: NextPage = () => {
  const session = useSession();
  const route = useRouter();
  const [form] = Form.useForm();
  const formData = form.getFieldsValue();
  const [dateRange, setDateRange] = useState<
    [Moment | null, Moment | null] | null
  >(null);
  const [dateRange2, setDateRange2] = useState<[Moment | null, Moment | null]>([
    null,
    null,
  ]);

  const handleDateRangeChange = (
    dates: [Moment | null, Moment | null] | null,
    dateStrings: [string, string]
  ) => {
    setDateRange(dates);
  };
  const [location, setLocation] = useState("");
  const [classification, setclassification] = useState("");
  const [hospital, setHospital] = useState("");

  // const handleCheck = () => {
  //   dateRange ? setDateRange2(dateRange) : setDateRange2([null, null]);
  //   setLocation(formData.location);
  //   // console.log(location);
  // };

  // const handleDateRangeChange = (
  //   dates: [Moment | null, Moment | null] | null,
  //   formatString: [string, string]
  // ) => {
  //   if (dates) {
  //     setFilters({
  //       ...filters,
  //       startDate: dates[0] ? dates[0].format("YYYY-MM-DD") : "",
  //       endDate: dates[1] ? dates[1].format("YYYY-MM-DD") : null,
  //     });
  //     refetch();
  //   } else {
  //     setFilters({
  //       ...filters,
  //       startDate: null,
  //       endDate: null,
  //     });
  //     refetch();
  //   }
  // };

  const handleFinishForm = async (values: any) => {
    const selectedDateRange = dateRange || [null, null];
    setDateRange2(selectedDateRange);
    setLocation(values.area);
    setclassification(values.classification);
    setHospital(values.hospital);
  };

  const {
    data: data3,
    isLoading: loading3,
    refetch: refetch3,
  } = trpc.useQuery(["taxdec.getHospital"]);

  return (
    <div className="text-2xl font-bold mb-10 text-black-200 fade-in">
      <div className="flex flex-wrap p-3 bg-custom-blue mb-5 rounded ">
        <Form
          form={form}
          layout="vertical"
          onFinish={handleFinishForm}
          className="w-full"
        >
          <div className="flex">
            <div className="flex gap-2 flex-wrap">
              <Form.Item
                name="Date"
                required
                label={<span className="text-white">SELECT DATES</span>}
                rules={[
                  {
                    required: true,
                    message: "Date is required",
                  },
                ]}
              >
                <DatePicker.RangePicker
                  value={dateRange}
                  format="YYYY-MM-DD"
                  className="w-full md:w-72"
                  onChange={handleDateRangeChange}
                />
              </Form.Item>
              <Form.Item
                name="area"
                required
                label={<span className="text-white">AREA</span>}
                rules={[
                  {
                    required: true,
                    message: "Area is required",
                  },
                ]}
              >
                <Select style={{ width: "200px" }}>
                  <Select.Option value="DISCHARGE" key="DISCHARGE">
                    DISCHARGE
                  </Select.Option>
                  <Select.Option value="OPD" key="OPD">
                    OPD
                  </Select.Option>
                  <Select.Option value="EMERGENCY" key="EMERGENCY">
                    EMERGENCY
                  </Select.Option>
                  <Select.Option value="ADMITTED" key="ADMITTED">
                    ADMITTED
                  </Select.Option>
                  <Select.Option value="WALK-IN" key="WALK-IN">
                    WALK-IN
                  </Select.Option>
                </Select>
              </Form.Item>
              <Form.Item
                name="classification"
                label={<span className="text-white">CLASSIFICATION</span>}
                required
                rules={[
                  {
                    required: true,
                    message: "Classification is required",
                  },
                ]}
                initialValue={"Indigent"}
              >
                <Select style={{ width: "200px" }}>
                  <Select.Option value="Indigent" key="Indigent">
                    Indigent
                  </Select.Option>
                  <Select.Option value="Paying" key="Paying">
                    Paying
                  </Select.Option>
                  <Select.Option value="Philhealth" key="Philhealth">
                    Philhealth
                  </Select.Option>
                  <Select.Option value="Non Philhealth" key="Non Philhealth">
                    Non Philhealth
                  </Select.Option>
                </Select>
              </Form.Item>
              <Form.Item
                name="hospital"
                label={<span className="text-white">HOSPITAL</span>}
              >
                <Select
                  style={{
                    inlineSize: 200,
                  }}
                >
                  {(data3 || []).map((item, index) => (
                    <Select.Option
                      value={item.hospitalName}
                      key={index}
                      title={item.hospitalName}
                    >
                      {item.hospitalName}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name=""
                label={<span className="text-white">ACTION</span>}
              >
                <Button
                  // onClick={handleCheck}
                  htmlType="submit"
                  className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
                >
                  SUBMIT
                </Button>
              </Form.Item>
              {/* <Form.Item name="">
                <Button
                  onClick={handleCheck}
                  className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
                >
                  DOWNLOAD
                </Button>
              </Form.Item> */}
            </div>
          </div>
        </Form>
      </div>
      <div className="w-full">
        {dateRange2.includes(null) ? null : (
          <Details
            data={dateRange2}
            location={location}
            classification={classification}
            hospital={hospital}
          />
        )}
      </div>
      {/* <div className="text-xl font-medium text-gray-600 mb-8">REPORTS</div> */}
    </div>
  );
};
const Details = (props: any) => {
  // props.data.includes(null) ? console.log('0'): console.log('1')
  const session = useSession();

  // console.log(props);
  const startDate2 = props.data[0]
    ? moment(props.data[0]).format("YYYY-MM-DD")
    : "";
  const endDate2 = props.data[1]
    ? moment(props.data[1]).format("YYYY-MM-DD")
    : "";

  //   console.log(props.location);
  const {
    data,
    isLoading: loading,
    refetch,
  } = trpc.useQuery([
    "taxdec.getSensusReportAdmin",
    {
      startDate: startDate2,
      endDate: endDate2,
      area: props.location,
      classification: props.classification,
      hospital: props.hospital,
    },
  ]);

  const totalSeniorDiscount = data?.reduce((totalDiscount, item) => {
    const discount = item.discount_senior;
    return totalDiscount + discount;
  }, 0);
  const totalPwdDiscount = data?.reduce((totalDiscount, item) => {
    const discount = item.discount_pwd;
    return totalDiscount + discount;
  }, 0);
  const totalPhic = data?.reduce((totalDiscount, item) => {
    const discount = item.philhealth_deduction;
    return totalDiscount + discount;
  }, 0);
  const totalMaip = data?.reduce((totalDiscount, item) => {
    const discount = item.maip_deduction;
    return totalDiscount + discount;
  }, 0);
  const totalAics = data?.reduce((totalDiscount, item) => {
    const discount = item.aics_deduction;
    return totalDiscount + discount;
  }, 0);
  const totalHimsugbo = data?.reduce((totalDiscount, item) => {
    const discount = item.himsugbo;
    return totalDiscount + discount;
  }, 0);
  const totalMalasakit = data?.reduce((totalDiscount, item) => {
    const discount = item.malaskit;
    return totalDiscount + discount;
  }, 0);
  const totalPcso = data?.reduce((totalDiscount, item) => {
    const discount = item.pcso;
    return totalDiscount + discount;
  }, 0);
  const totalCash = data?.reduce((totalDiscount, item) => {
    const discount = item.cash;
    return totalDiscount + discount;
  }, 0);
  const totalQfs = data?.reduce((totalDiscount, item) => {
    const discount = item.qfs;
    return totalDiscount + discount;
  }, 0);
  const totalPromisori = data?.reduce((totalDiscount, item) => {
    const discount = item.promisri_note;
    return totalDiscount + discount;
  }, 0);

  //DOWNLOAD
  const handleExport = () => {
    if (!data) {
      return;
    }

    const formatDate = (dateString: any) => {
      if (!dateString) return "";

      const [year, month, day] = dateString.split("-");
      const monthName = moment()
        .month(parseInt(month) - 1)
        .format("MMMM");
      return `Month of ${year}: ${monthName}`;
    };

    const formattedStartDate = formatDate(startDate2);

    const header = [
      [
        "NAME OF HOSPITAL: " + props.hospital.toUpperCase(),
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
      ],
      [
        formattedStartDate.toUpperCase(),
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
      ],
      [
        "AREA: " + props.location.toUpperCase(),
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
      ],
      [
        "CLASSIFICATION: " + props.classification.toUpperCase(),
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
      ],
      [
        "",
        "",
        "",
        "",
        "Discount",
        "",
        "",
        "Medical Assistance",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
      ],
      [
        "No.",
        "Date Discharge",
        "Patient Name",
        "Total Hospital Bill",
        "Senior",
        "PWD",
        "Bal. after Discount",
        "Philhealth",
        "MAIP",
        "AICS",
        "HIMSUGBO",
        "MALASAKIT",
        "PCSO",
        "Balance After Assistance",
        "CASH",
        "QFS",
        "Promissory Note",
      ],
    ];

    // Define the body
    const body = data.map((item, index) => [
      index + 1,
      item.bill_date ? item.bill_date.toISOString().split("T")[0] : "",
      item.patients_name.toUpperCase(),
      item.bill_amount,
      item.discount_senior,
      item.discount_pwd,
      item.bill_amount - (item.discount_senior + item.discount_pwd),
      item.philhealth_deduction,
      item.maip_deduction,
      item.aics_deduction,
      item.himsugbo,
      item.malaskit,
      item.pcso,
      item.bill_amount -
        (item.discount_senior +
          item.discount_pwd +
          item.philhealth_deduction +
          item.maip_deduction +
          item.aics_deduction +
          item.himsugbo +
          item.malaskit +
          item.pcso),
      item.cash,
      item.qfs,
      item.promisri_note,
    ]);

    // Calculate totals
    const totals = data.reduce(
      (acc, item) => {
        const balAfterDiscount =
          item.bill_amount - (item.discount_senior + item.discount_pwd);

        const balAfterAssistance =
          item.bill_amount -
          (item.discount_senior +
            item.discount_pwd +
            item.philhealth_deduction +
            item.maip_deduction +
            item.aics_deduction +
            item.himsugbo +
            item.malaskit +
            item.pcso);
        acc.totalBill += item.bill_amount;
        acc.totalSeniorDiscount += item.discount_senior;
        acc.totalPwdDiscount += item.discount_pwd;
        acc.totalBalAfterDiscount += balAfterDiscount;
        acc.totalPhic += item.philhealth_deduction;
        acc.totalMaip += item.maip_deduction;
        acc.totalAics += item.aics_deduction;
        acc.totalHimsugbo += item.himsugbo;
        acc.totalMalasakit += item.malaskit;
        acc.totalPcso += item.pcso;
        acc.totalbalAfterAssistance += balAfterAssistance;
        acc.totalCash += item.cash;
        acc.totalQfs += item.qfs;
        acc.totalPromisori += item.promisri_note;
        return acc;
      },
      {
        totalBill: 0,
        totalSeniorDiscount: 0,
        totalPwdDiscount: 0,
        totalBalAfterDiscount: 0,
        totalPhic: 0,
        totalMaip: 0,
        totalAics: 0,
        totalHimsugbo: 0,
        totalMalasakit: 0,
        totalPcso: 0,
        totalCash: 0,
        totalbalAfterAssistance: 0,
        totalQfs: 0,
        totalPromisori: 0,
      }
    );
    // Define the totals row
    const totalsRow = [
      "",
      "",
      "TOTAL",
      totals.totalBill,
      totals.totalSeniorDiscount,
      totals.totalPwdDiscount,
      totals.totalBalAfterDiscount,
      totals.totalPhic,
      totals.totalMaip,
      totals.totalAics,
      totals.totalHimsugbo,
      totals.totalMalasakit,
      totals.totalPcso,
      totals.totalbalAfterAssistance,
      totals.totalCash,
      totals.totalQfs,
      totals.totalPromisori,
    ];
    // Combine header and body
    const worksheetData = [...header, ...body, totalsRow];

    // Create a new workbook
    const workbook = XLSX.utils.book_new();

    // Convert data to worksheet
    const worksheet = XLSX.utils.aoa_to_sheet(worksheetData);

    // Merge cells
    worksheet["!merges"] = [
      { s: { r: 0, c: 0 }, e: { r: 0, c: 16 } }, // Merging row 1, columns 1-17
      { s: { r: 1, c: 0 }, e: { r: 1, c: 16 } }, // Merging row 2, columns 1-17
      { s: { r: 2, c: 0 }, e: { r: 2, c: 16 } }, // Merging row 3, columns 1-17
      { s: { r: 3, c: 0 }, e: { r: 3, c: 16 } }, // Merging row 4, columns 1-17
      { s: { r: 4, c: 4 }, e: { r: 4, c: 6 } }, // Merging row 5, columns 5-7
      { s: { r: 4, c: 7 }, e: { r: 4, c: 12 } }, // Merging row 5, columns 8-13
    ];

    XLSX.utils.book_append_sheet(workbook, worksheet, `MONTHLY DATA`);

    XLSX.writeFile(
      workbook,
      `${props.hospital.toUpperCase()} - ${
        props.location
      }- ${formattedStartDate}.xlsx`
    );
  };

  return (
    <div className="py-4">
      <Row gutter={[16, 16]}>
        <Col span={24}>
          <Card style={{ overflow: "auto" }}>
            {/* <DownloadOutlined className="text-2xl mr-2 text-blue-500" /> */}
            {/* <span className="text-lg font-semibold">Export to Excel</span> */}
            <Button
              onClick={handleExport}
              icon={<DownloadOutlined />}
              className="flex mr-2 text-white bg-blue-600 justify-center items-center"
            >
              {" "}
              Export to Excel{" "}
            </Button>
            <table
              style={{
                width: "2000px",
                marginTop: "5px",
              }}
              //   className="summaryTable"
            >
              <thead>
                <tr>
                  <td
                    colSpan={17}
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    {props.hospital.toUpperCase()}
                  </td>
                </tr>
                <tr>
                  <td
                    colSpan={4}
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  ></td>
                  <td
                    colSpan={3}
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Discount
                  </td>
                  <td
                    colSpan={6}
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Medical Assistance
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  ></td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  ></td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  ></td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  ></td>
                </tr>
                <tr>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    No.
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Date Discharge
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Patient Name
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Total Hospital Bill
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Senior
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    PWD
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Bal. after <br />
                    Discount
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Philhealth
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    MAIP
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    AICS
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    HIMSUGBO
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    MALASAKIT
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    PCSO
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Balance After <br /> Assistance
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    CASH
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    QFS
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Promisori Note
                  </td>
                </tr>
              </thead>
              <tbody>
                {data?.map((item: any, index) => (
                  <tr key={index}>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "20px",
                      }}
                    >
                      {index + 1}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {moment(item.bill_date).format("YYYY-MM-DD")}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "200px",
                      }}
                    >
                      {item.patients_name.toUpperCase()}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.bill_amount.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.discount_senior.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.discount_pwd.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {(
                        item.bill_amount -
                        (item.discount_senior + item.discount_pwd)
                      ).toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.philhealth_deduction.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.maip_deduction.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.aics_deduction.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.himsugbo.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.malaskit.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.pcso.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {(
                        item.bill_amount -
                        (item.discount_senior +
                          item.discount_pwd +
                          item.philhealth_deduction +
                          item.maip_deduction +
                          item.aics_deduction +
                          item.himsugbo +
                          item.malaskit +
                          item.pcso)
                      ).toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.cash.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.qfs.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                        width: "100px",
                      }}
                    >
                      {item.promisri_note.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                  </tr>
                ))}
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    TOTAL
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalSeniorDiscount?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalPwdDiscount?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {/* {(
                      (totalSeniorDiscount ?? 0) + (totalPwdDiscount ?? 0)
                    ).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })} */}
                    --
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalPhic?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalMaip?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalAics?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalHimsugbo?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalMalasakit?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalPcso?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    --
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalCash?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalQfs?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "100px",
                    }}
                  >
                    {totalPromisori?.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                </tr>
              </tbody>
            </table>
          </Card>
        </Col>
      </Row>
    </div>
  );
};
export default Dashboard;
