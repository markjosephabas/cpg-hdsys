import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Select,
  Card,
  Space,
  InputNumber,
} from "antd";
import { NextPage } from "next";
import type { ColumnsType, ColumnType } from "antd/es/table";
import moment from "moment";
import { trpc } from "../../utils/trpc";
import React, { useState } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import * as XLSX from "xlsx";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  DeleteOutlined,
  FileExcelOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const permission = session.data?.user?.permission;

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getphicmanagement"]);

  const clickable = () => {
    const {
      data: data,
      isLoading: loading,
      refetch: refetch,
    } = trpc.useQuery(["taxdec.getphicmanagement"]);

    console.log(data);
  };
  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);
  const [openAddItem, setopenAddItem] = useState(false);

  const updatephicmanagement = trpc.useMutation("taxdec.updatephicmanagement", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const handleUpdate = async (values: any) => {
    try {
      const { id, ...data } = values;
      // console.log("id:", id);
      // console.log("data:", data);
      // console.log(id);
      await updatephicmanagement.mutate({
        id: id,
        data: {
          ...data,
        },
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  // FOR TABLE
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const handleChange = (pagination: any, filters: any, sorter: any) => {
    setCurrentPage(pagination.current);
    if (pagination.current !== currentPage) {
      setCurrentPage(pagination.current);
    }

    if (pagination.pageSize !== pageSize) {
      setCurrentPage(1); // Reset to the first page when changing page size
      setPageSize(pagination.pageSize);
    }
  };
  const paginationConfig = {
    current: currentPage,
    pageSize: pageSize, // Adjust as needed
    showTotal: (total: any, range: any) =>
      `${range[0]}-${range[1]} of ${total} items`,
  };
  //TABLE

  const handleDelete = (id: number) => {
    deletePhic.mutate({
      id: id,
    });
  };

  const deletePhic = trpc.useMutation("taxdec.deletePhic", {
    onSuccess() {
      message.success("Deleted successfully!");
      refetch();
    },
    onError(error) {
      message.error("Something went wrong!");
    },
  });

  function handleClick(value: any) {
    Modal.confirm({
      title: `Are you sure you want to delete ${value.name}?`,
      onOk() {
        handleDelete(value.id);
      },
      onCancel() {
        // The user clicked Cancel
        // Do nothing or handle the cancel event here
      },
      okButtonProps: {
        className:
          "bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline",
      },
    });
  }

  const insertphicmanagement = trpc.useMutation("taxdec.insertphicmanagement", {
    onSuccess(data) {
      message.success("Added successfully!");
      refetch();
      form.resetFields();
      setopenAddItem(false);
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleUpdate2 = async (values: any) => {
    try {
      insertphicmanagement.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  interface DataType {
    key: string;
    name: string;
    id: number;
    year: string;
  }
  const columns: ColumnsType<DataType> = [
    {
      title: "#",
      dataIndex: "id",
      key: "id",
      align: "center",
      render: (text, record, index) => (currentPage - 1) * pageSize + index + 1,
    },
    {
      title: "Year",
      dataIndex: "id",
      key: "id",
      align: "center",
      render: (text, record, index) => record.year,
    },
    {
      title: "Date Created",
      dataIndex: "created_at",
      key: "created_at",
      align: "center" as "left" | "center" | "right",
      render: (text: any, record: any) =>
        record.created_at ? moment(record.created_at).format("LLL") : "",
    },
    {
      title: "Last Update",
      dataIndex: "updated_at",
      key: "updated_at",
      align: "center" as "left" | "center" | "right",
      render: (text: any, record: any) =>
        record.updated_at ? moment(record.updated_at).format("LLL") : "",
    },
    {
      title: "Actions",
      key: "id",
      align: "center",
      render: (item: any) => {
        return (
          <Space>
            <Tooltip title="Edit">
              <Button
                onClick={() => handleEdit(item)}
                className="bg-blue-600 text-white ml-1"
                shape="circle"
                icon={<FormOutlined />}
              ></Button>
            </Tooltip>
            <ExportComponent id={item.id} />
            <Tooltip title="Delete">
              <Button
                onClick={() => handleClick(item)}
                icon={<DeleteOutlined />}
                shape="circle"
                danger
                type="primary"
              />
            </Tooltip>
          </Space>
        );
      },
    },
  ];
  const dataSource =
    data?.map((item: any, index: any) => ({
      ...item,
      key: index,
    })) || [];
  if (permission === "admin") {
    const hospitalColumn: ColumnType<DataType> = {
      title: "Hospital",
      dataIndex: "hospital",
      width: 500,
      key: "hospital",
      align: "center",
      filterDropdown: ({
        setSelectedKeys,
        selectedKeys,
        confirm,
        clearFilters,
      }: CustomFilterDropdownProps) => (
        <div style={{ padding: 8 }}>
          <Input
            placeholder="Search by hospital name"
            value={selectedKeys[0]}
            onChange={e =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => confirm()}
            style={{ marginBottom: 8, display: "block" }}
          />
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <Button
              onClick={() => {
                clearFilters?.();
                confirm();
              }}
              style={{ marginRight: 8 }}
              size="small"
            >
              Reset
            </Button>
            <Button onClick={() => confirm()} size="small">
              Filter
            </Button>
          </div>
        </div>
      ),
      filterIcon: () => (
        <Tooltip title="Search by hospital name">
          <SearchOutlined />
        </Tooltip>
      ),
      onFilter: (value: any, record: any) =>
        record.hospital &&
        record.hospital.toLowerCase().includes(value.toLowerCase()), // Check if hospital property exists
      render: (value: any, record: any) =>
        record.hospital ? record.hospital.toUpperCase() : "", // Check if hospital property exists
      sorter: (a: any, b: any) => a.hospital.length - b.hospital.length,
      sortDirections: ["ascend" as const, "descend" as const],
    };
    // Insert the hospital column at the desired position (e.g., third position)
    columns.splice(3, 2, hospitalColumn);
    // Remove the column at index 8
    // columns.splice(8, 1);
  }
  if (permission === "admin") {
    const DlColumn: ColumnType<DataType> = {
      title: "Export",
      key: "id",
      align: "center",
      render: (item: any) => {
        return (
          <Space>
            <ExportComponent id={item.id} />
          </Space>
        );
      },
    };
    // Insert the hospital column at the desired position (e.g., third position)
    columns.splice(4, 0, DlColumn);
    // Remove the column at index 8
    // columns.splice(8, 1);
  }
  return (
    <div>
      <Card className="fade-in">
        <div className="flex p-3 bg-custom-blue mb-5 rounded ">
          <span className="uppercase text-2xl font-bold text-white">
            PHILHEATLH MANAGEMENT
          </span>
          {permission !== "admin" && (
            <Button
              className="ml-auto flex items-center text-custom-blue bg-white border-custom-blue rounded-full"
              icon={<PlusOutlined />}
              onClick={() => setopenAddItem(true)}
            >
              CREATE{" "}
            </Button>
          )}
        </div>
        <div>
          <Table
            columns={columns}
            size="middle"
            bordered
            dataSource={dataSource}
            onChange={handleChange}
            loading={loading}
            pagination={paginationConfig}
            scroll={{ x: true }}
            // responsive
          />
        </div>
      </Card>
      {/* ADD ITEM     */}
      <Modal
        open={openAddItem}
        footer={[
          <Button
            key="cancel"
            onClick={() => setopenAddItem(false)}
            className="border-custom-blue"
          >
            Cancel
          </Button>,
          <Button
            disabled={insertphicmanagement.isLoading}
            key="submit"
            type="primary"
            htmlType="submit"
            onClick={() => form.submit()}
          >
            {insertphicmanagement.isLoading ? (
              <>
                <Spin /> <span>Please wait..</span>
              </>
            ) : (
              "SAVE"
            )}
          </Button>,
        ]}
        closable={false}
        width={350}
      >
        <Form
          form={form}
          layout="vertical"
          onFinish={handleUpdate2}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24}>
              <Form.Item
                name="year"
                label="Select Year"
                required
                rules={[
                  {
                    required: true,
                    message: "Type is required",
                  },
                ]}
                initialValue="2024"
              >
                <Select>
                  <Select.Option value="2024" key="2024">
                    2024
                  </Select.Option>
                  <Select.Option value="2025" key="2025">
                    2025
                  </Select.Option>
                  <Select.Option value="2026" key="2026">
                    2026
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
      {/* UPDATE ITEM     */}
      <Modal
        open={openUpdateItem}
        footer={[
          <Button
            key="cancel"
            onClick={() => setopenUpdateItem(false)}
            className="border-custom-blue"
          >
            Cancel
          </Button>,
          <Button
            key="submit"
            type="primary"
            htmlType="submit"
            onClick={() => form2.submit()}
          >
            Update
          </Button>,
        ]}
        width={600}
        closable={false}
      >
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleUpdate}
          className="w-full"
          autoComplete="off"
        >
          <div
            style={{
              height: "600px",
              overflow: "scroll",
              padding: "15px",
            }}
          >
            <Row gutter={12}>
              <Col span={24} className="hidden">
                <Form.Item
                  name="id"
                  label="ID"
                  required
                  rules={[
                    {
                      required: true,
                      message: "First Name is required",
                    },
                  ]}
                >
                  <Input placeholder="First Name" />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  JANUARY
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="january_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="january_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="january_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="january_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="january_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="january_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="january_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="january_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  FEBRUARY
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="february_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="february_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="february_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="february_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="february_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="february_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="february_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="february_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  MARCH
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="march_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="march_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="march_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="march_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="march_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="march_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="march_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="march_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  APRIL
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="april_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="april_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="april_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="april_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="april_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="april_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="april_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="april_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  MAY
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="may_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="may_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="may_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="may_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="may_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="may_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="may_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="may_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  JUNE
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="june_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="june_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="june_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="june_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="june_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="june_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="june_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="june_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  JULY
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="july_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="july_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="july_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="july_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="july_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="july_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="july_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="july_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  AUGUST
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="august_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="august_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="august_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="august_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="august_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="august_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="august_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="august_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  SEPTEMBER
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="september_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="september_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="september_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="september_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="september_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="september_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="september_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="september_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  OCTOBER
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="october_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="october_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="october_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="october_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="october_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="october_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="october_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="october_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  NOVEMBER
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="november_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="november_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="november_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="november_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="november_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="november_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="november_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="november_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
                  DECEMBER
                </div>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="december_total_denied"
                  label="Total Denied"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="december_denied_amount"
                  label="Denied Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="december_total_returned"
                  label="Returned"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="december_returned_amount"
                  label="Returned Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="december_total_onprocess"
                  label="On Process"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="december_onprocess_amount"
                  label="On Process Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="december_total_successful"
                  label="Successfull"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Returned is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="december_successful_amount"
                  label="Successfull Amount"
                  required
                  rules={[
                    {
                      required: true,
                      message: "Total Denied is required",
                    },
                  ]}
                  initialValue={0}
                >
                  <InputNumber
                    min={0}
                    style={{ width: "100%" }}
                    formatter={value =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                  />
                </Form.Item>
              </Col>
            </Row>
          </div>
          {/* <Row>
            <Col span={24}>
              <Button
                disabled={updateLabItems.isLoading}
                htmlType="submit"
                type="primary"
                loading={updateLabItems.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {updateLabItems.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
              </Button>
            </Col>
          </Row> */}
        </Form>
      </Modal>
    </div>
  );
};

const ExportComponent = (props: any) => {
  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getphicmanagementexport", { id: props.id }]);

  // const handleExport = () => {
  //   console.log("id", props.id);
  //   console.log("data", data);
  // };
  const handleExport = async () => {
    if (!data) {
      return;
    }

    await refetch();

    const totalDenied = data?.reduce((totalDiscount, item) => {
      const discount =
        item.january_total_denied +
        item.february_total_denied +
        item.march_total_denied +
        item.april_total_denied +
        item.may_total_denied +
        item.june_total_denied +
        item.july_total_denied +
        item.august_total_denied +
        item.september_total_denied +
        item.october_total_denied +
        item.november_total_denied +
        item.december_total_denied;
      return totalDiscount + discount;
    }, 0);
    const totalDeniedAmount = data?.reduce((totalDiscount, item) => {
      const discount =
        item.january_denied_amount +
        item.february_denied_amount +
        item.march_denied_amount +
        item.april_denied_amount +
        item.may_denied_amount +
        item.june_denied_amount +
        item.july_denied_amount +
        item.august_denied_amount +
        item.september_denied_amount +
        item.october_denied_amount +
        item.november_denied_amount +
        item.december_denied_amount;
      return totalDiscount + discount;
    }, 0);
    const totalReturned = data?.reduce((totalDiscount, item) => {
      const discount =
        item.january_total_returned +
        item.february_total_returned +
        item.march_total_returned +
        item.april_total_returned +
        item.may_total_returned +
        item.june_total_returned +
        item.july_total_returned +
        item.august_total_returned +
        item.september_total_returned +
        item.october_total_returned +
        item.november_total_returned +
        item.december_total_returned;
      return totalDiscount + discount;
    }, 0);
    const totalReturnedAmount = data?.reduce((totalDiscount, item) => {
      const discount =
        item.january_returned_amount +
        item.february_returned_amount +
        item.march_returned_amount +
        item.april_returned_amount +
        item.may_returned_amount +
        item.june_returned_amount +
        item.july_returned_amount +
        item.august_returned_amount +
        item.september_returned_amount +
        item.october_returned_amount +
        item.november_returned_amount +
        item.december_returned_amount;
      return totalDiscount + discount;
    }, 0);
    const totalOnprocess = data?.reduce((totalDiscount, item) => {
      const discount =
        item.january_total_onprocess +
        item.february_total_onprocess +
        item.march_total_onprocess +
        item.april_total_onprocess +
        item.may_total_onprocess +
        item.june_total_onprocess +
        item.july_total_onprocess +
        item.august_total_onprocess +
        item.september_total_onprocess +
        item.october_total_onprocess +
        item.november_total_onprocess +
        item.december_total_onprocess;
      return totalDiscount + discount;
    }, 0);
    const totalOnprocessAmount = data?.reduce((totalDiscount, item) => {
      const discount =
        item.january_onprocess_amount +
        item.february_onprocess_amount +
        item.march_onprocess_amount +
        item.april_onprocess_amount +
        item.may_onprocess_amount +
        item.june_onprocess_amount +
        item.july_onprocess_amount +
        item.august_onprocess_amount +
        item.september_onprocess_amount +
        item.october_onprocess_amount +
        item.november_onprocess_amount +
        item.december_onprocess_amount;
      return totalDiscount + discount;
    }, 0);
    const totalFirstQuarterSuccessful = data?.reduce((totalDiscount, item) => {
      const discount =
        item.january_total_successful +
        item.february_total_successful +
        item.march_total_successful;
      return totalDiscount + discount;
    }, 0);
    const totalFirstQuarterSuccessfulAmount = data?.reduce(
      (totalDiscount, item) => {
        const discount =
          item.january_successful_amount +
          item.february_successful_amount +
          item.march_successful_amount;
        return totalDiscount + discount;
      },
      0
    );
    const totalSecondQuarterSuccessful = data?.reduce((totalDiscount, item) => {
      const discount =
        item.april_total_successful +
        item.may_total_successful +
        item.june_total_successful;
      return totalDiscount + discount;
    }, 0);
    const totalSecondQuarterSuccessfulAmount = data?.reduce(
      (totalDiscount, item) => {
        const discount =
          item.april_successful_amount +
          item.may_successful_amount +
          item.june_successful_amount;
        return totalDiscount + discount;
      },
      0
    );
    const totalThirdQuarterSuccessful = data?.reduce((totalDiscount, item) => {
      const discount =
        item.july_total_successful +
        item.august_total_successful +
        item.september_total_successful;
      return totalDiscount + discount;
    }, 0);
    const totalThirdQuarterSuccessfulAmount = data?.reduce(
      (totalDiscount, item) => {
        const discount =
          item.july_successful_amount +
          item.august_successful_amount +
          item.september_successful_amount;
        return totalDiscount + discount;
      },
      0
    );
    const totalFourthQuarterSuccessful = data?.reduce((totalDiscount, item) => {
      const discount =
        item.october_total_successful +
        item.november_total_successful +
        item.december_total_successful;
      return totalDiscount + discount;
    }, 0);
    const totalFourthQuarterSuccessfulAmount = data?.reduce(
      (totalDiscount, item) => {
        const discount =
          item.october_successful_amount +
          item.november_successful_amount +
          item.december_successful_amount;
        return totalDiscount + discount;
      },
      0
    );
    const totalFinalSuccessful = data?.reduce((totalDiscount, item) => {
      const discount =
        item.january_total_successful +
        item.february_total_successful +
        item.march_total_successful +
        item.april_total_successful +
        item.may_total_successful +
        item.june_total_successful +
        item.july_total_successful +
        item.august_total_successful +
        item.september_total_successful +
        item.october_total_successful +
        item.november_total_successful +
        item.december_total_successful;
      return totalDiscount + discount;
    }, 0);
    const totalFinalSuccessfulAmount = data?.reduce((totalDiscount, item) => {
      const discount =
        item.january_successful_amount +
        item.february_successful_amount +
        item.march_successful_amount +
        item.april_successful_amount +
        item.may_successful_amount +
        item.june_successful_amount +
        item.july_successful_amount +
        item.august_successful_amount +
        item.september_successful_amount +
        item.october_successful_amount +
        item.november_successful_amount +
        item.december_successful_amount;
      return totalDiscount + discount;
    }, 0);
    const header = [
      [
        "",
        "Philhealth Insurance Corp. Monthly Monitoring Report", //1-6
        "",
        "",
        "",
        "",
        "",
        "1st Quarter Jan-March", //7-8
        "", // First

        "2nd Quarter Apr.-June", //9-10
        "", // Second

        "3rd Quarter July.-Dec.", //11-12
        "", // Third

        "4th Quarter Oct.-Dec.", //13-14
        "Sucessful Amount", //Forth

        "Year" + data[0]?.year.toUpperCase(), //15-16
        "", // Final
      ],
      [
        "YEAR " + data[0]?.year.toUpperCase(),

        "Total Denied",
        "Denied Amount",

        "Returned",
        "Returned Amount",

        "On Process",
        "On Process Amount",

        "Sucessful",
        "Sucessful Amount", // First

        "Sucessful",
        "Sucessful Amount", // Second

        "Sucessful",
        "Sucessful Amount", // Third

        "Sucessful",
        "Sucessful Amount", //Forth

        "Sucessful",
        "Sucessful Amount", // Final
      ],
      [
        "JANUARY", // JANUARY
        data[0]?.january_total_denied,
        data[0]?.january_denied_amount,
        data[0]?.january_total_returned,
        data[0]?.january_returned_amount,
        data[0]?.january_total_onprocess,
        data[0]?.january_onprocess_amount,
        data[0]?.january_total_successful,
        data[0]?.january_successful_amount,
        "x",
        "x", // Second
        "x",
        "x", // Third
        "x",
        "x", //Forth
        data[0]?.january_total_successful,
        data[0]?.january_successful_amount, // Final
      ],
      [
        "FEBRUARY", // february
        data[0]?.february_total_denied,
        data[0]?.february_denied_amount,
        data[0]?.february_total_returned,
        data[0]?.february_returned_amount,
        data[0]?.february_total_onprocess,
        data[0]?.february_onprocess_amount,
        data[0]?.february_total_successful,
        data[0]?.february_successful_amount,
        "x",
        "x", // Second
        "x",
        "x", // Third
        "x",
        "x", //Forth
        data[0]?.february_total_successful,
        data[0]?.february_successful_amount, // Final
      ],
      [
        "MARCH", // march
        data[0]?.march_total_denied,
        data[0]?.march_denied_amount,
        data[0]?.march_total_returned,
        data[0]?.march_returned_amount,
        data[0]?.march_total_onprocess,
        data[0]?.march_onprocess_amount,
        data[0]?.march_total_successful,
        data[0]?.march_successful_amount,
        "x",
        "x", // Second
        "x",
        "x", // Third
        "x",
        "x", //Forth
        data[0]?.march_total_successful,
        data[0]?.march_successful_amount, // Final
      ],
      [
        "APRIL", // april
        data[0]?.april_total_denied,
        data[0]?.april_denied_amount,
        data[0]?.april_total_returned,
        data[0]?.april_returned_amount,
        data[0]?.april_total_onprocess,
        data[0]?.april_onprocess_amount,
        "x",
        "x", // Second
        data[0]?.april_total_successful,
        data[0]?.april_successful_amount,
        "x",
        "x", // Third
        "x",
        "x", //Forth
        data[0]?.april_total_successful,
        data[0]?.april_successful_amount, // Final
      ],
      [
        "MAY", // may
        data[0]?.may_total_denied,
        data[0]?.may_denied_amount,
        data[0]?.may_total_returned,
        data[0]?.may_returned_amount,
        data[0]?.may_total_onprocess,
        data[0]?.may_onprocess_amount,
        "x",
        "x", // Second
        data[0]?.may_total_successful,
        data[0]?.may_successful_amount,
        "x",
        "x", // Third
        "x",
        "x", //Forth
        data[0]?.may_total_successful,
        data[0]?.may_successful_amount, // Final
      ],
      [
        "JUNE", // june
        data[0]?.june_total_denied,
        data[0]?.june_denied_amount,
        data[0]?.june_total_returned,
        data[0]?.june_returned_amount,
        data[0]?.june_total_onprocess,
        data[0]?.june_onprocess_amount,
        "x",
        "x", // Second
        data[0]?.june_total_successful,
        data[0]?.june_successful_amount,
        "x",
        "x", // Third
        "x",
        "x", //Forth
        data[0]?.june_total_successful,
        data[0]?.june_successful_amount, // Final
      ],
      [
        "JULY", // july
        data[0]?.july_total_denied,
        data[0]?.july_denied_amount,
        data[0]?.july_total_returned,
        data[0]?.july_returned_amount,
        data[0]?.july_total_onprocess,
        data[0]?.july_onprocess_amount,
        "x",
        "x", // Second
        "x",
        "x", // Third
        data[0]?.july_total_successful,
        data[0]?.july_successful_amount,
        "x",
        "x", //Forth
        data[0]?.july_total_successful,
        data[0]?.july_successful_amount, // Final
      ],
      [
        "AUGUST", // august
        data[0]?.august_total_denied,
        data[0]?.august_denied_amount,
        data[0]?.august_total_returned,
        data[0]?.august_returned_amount,
        data[0]?.august_total_onprocess,
        data[0]?.august_onprocess_amount,
        "x",
        "x", // Second
        "x",
        "x", // Third
        data[0]?.august_total_successful,
        data[0]?.august_successful_amount,
        "x",
        "x", //Forth
        data[0]?.august_total_successful,
        data[0]?.august_successful_amount, // Final
      ],
      [
        "SEPTEMBER", // september
        data[0]?.september_total_denied,
        data[0]?.september_denied_amount,
        data[0]?.september_total_returned,
        data[0]?.september_returned_amount,
        data[0]?.september_total_onprocess,
        data[0]?.september_onprocess_amount,
        "x",
        "x", // Second
        "x",
        "x", // Third
        data[0]?.september_total_successful,
        data[0]?.september_successful_amount,
        "x",
        "x", //Forth
        data[0]?.september_total_successful,
        data[0]?.september_successful_amount, // Final
      ],
      [
        "OCTOBER", // october
        data[0]?.october_total_denied,
        data[0]?.october_denied_amount,
        data[0]?.october_total_returned,
        data[0]?.october_returned_amount,
        data[0]?.october_total_onprocess,
        data[0]?.october_onprocess_amount,
        "x",
        "x", // Second
        "x",
        "x", // Third
        "x",
        "x", //Forth
        data[0]?.october_total_successful,
        data[0]?.october_successful_amount,
        data[0]?.october_total_successful,
        data[0]?.october_successful_amount, // Final
      ],
      [
        "NOVEMBER", // november
        data[0]?.november_total_denied,
        data[0]?.november_denied_amount,
        data[0]?.november_total_returned,
        data[0]?.november_returned_amount,
        data[0]?.november_total_onprocess,
        data[0]?.november_onprocess_amount,
        "x",
        "x", // Second
        "x",
        "x", // Third
        "x",
        "x", //Forth
        data[0]?.november_total_successful,
        data[0]?.november_successful_amount,
        data[0]?.november_total_successful,
        data[0]?.november_successful_amount, // Final
      ],
      [
        "DECEMBER", // december
        data[0]?.december_total_denied,
        data[0]?.december_denied_amount,
        data[0]?.december_total_returned,
        data[0]?.december_returned_amount,
        data[0]?.december_total_onprocess,
        data[0]?.december_onprocess_amount,
        "x",
        "x", // Second
        "x",
        "x", // Third
        "x",
        "x", //Forth
        data[0]?.december_total_successful,
        data[0]?.december_successful_amount,
        data[0]?.december_total_successful,
        data[0]?.december_successful_amount, // Final
      ],
      [
        "TOTAL", // december
        totalDenied,
        totalDeniedAmount,
        totalReturned,
        totalReturnedAmount,
        totalOnprocess,
        totalOnprocessAmount,
        totalFirstQuarterSuccessful,
        totalFirstQuarterSuccessfulAmount,
        totalSecondQuarterSuccessful,
        totalSecondQuarterSuccessfulAmount, // Third
        totalThirdQuarterSuccessful,
        totalThirdQuarterSuccessfulAmount, //Forth
        totalFourthQuarterSuccessful,
        totalFourthQuarterSuccessfulAmount,
        totalFinalSuccessful,
        totalFinalSuccessfulAmount, // Final
      ],
    ];

    const worksheetData = [...header];

    // Create a new workbook
    const workbook = XLSX.utils.book_new();

    // Convert data to worksheet
    const worksheet = XLSX.utils.aoa_to_sheet(worksheetData);

    // Merge cells
    worksheet["!merges"] = [
      { s: { r: 0, c: 1 }, e: { r: 0, c: 6 } },
      { s: { r: 0, c: 7 }, e: { r: 0, c: 8 } },
      { s: { r: 0, c: 9 }, e: { r: 0, c: 10 } }, // Merging row 2, columns 1-17
      { s: { r: 0, c: 11 }, e: { r: 0, c: 12 } }, // Merging row 2, columns 1-17
      { s: { r: 0, c: 13 }, e: { r: 0, c: 14 } }, // Merging row 2, columns 1-17
      { s: { r: 0, c: 15 }, e: { r: 0, c: 16 } }, // Merging row 2, columns 1-17
    ];

    XLSX.utils.book_append_sheet(workbook, worksheet, `Philhealth Summary`);

    XLSX.writeFile(
      workbook,
      `${data[0]?.hospital.toUpperCase()} - YEAR ${data[0]?.year}.xlsx`
    );
  };
  return (
    <Tooltip title="Export">
      <Button
        onClick={() => handleExport()}
        className="bg-green-600 text-white ml-1"
        shape="circle"
        icon={<FileExcelOutlined />}
      ></Button>
    </Tooltip>
  );
};
export default PendingTaxDeclaration;
