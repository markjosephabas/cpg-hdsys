import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  InputNumber,
  Card,
  Alert,
  Space,
  Divider,
  DatePicker,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  FileDoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";
import TextArea from "antd/lib/input/TextArea";
import moment, { Moment } from "moment";

interface InsertPatientStatus {
  type: "success" | "error";
  text: string;
}

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality;
  const permission = session.data?.user?.permission;

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getHospital"]);

  const {
    data: data2,
    isLoading: loading2,
    refetch: refetch2,
  } = trpc.useQuery(["taxdec.getSensusPatient"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);
  const [alertVisible, setAlertVisible] = useState(true);

  const updateHospital = trpc.useMutation("taxdec.updateHospital", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const [insertPatientStatus, setInsertPatientStatus] =
    useState<InsertPatientStatus | null>(null);

  const insertsensustransaction = trpc.useMutation(
    "taxdec.insertsensustransaction",
    {
      onSuccess(data) {
        setInsertPatientStatus({
          type: "success",
          text: "Record inserted successfully.",
        });
        refetch();
        form.resetFields();
        setAlertVisible(true);
      },
      onError(error) {
        setInsertPatientStatus({ type: "error", text: error.message });
        message.error(`${error.message}`);
        setAlertVisible(true);
      },
    }
  );

  const handleAlertClose = () => {
    setInsertPatientStatus(null);
  };

  useEffect(() => {
    if (insertPatientStatus && insertPatientStatus.text !== "") {
      // Automatically close the alert after 3 seconds
      const timer = setTimeout(() => {
        handleAlertClose(); // Close the alert
      }, 3000);

      // Clear the timer if the component unmounts or insertPatientStatus changes
      return () => clearTimeout(timer);
    }
  }, [insertPatientStatus]);

  const handleUpdate2 = async (values: any) => {
    try {
      insertsensustransaction.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const handleUpdate = async (values: any) => {
    try {
      updateHospital.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const setData = (value: any) => {
    const selectedObject2 = value
      ? data2?.find(item => item.id === value)
      : null;

    if (selectedObject2 && selectedObject2.id != null) {
      const newItem = {
        classification: selectedObject2.classification,
        laboratory_patients_id: selectedObject2.id,
        patients_name:
          selectedObject2.firstName +
          " " +
          selectedObject2.middleName +
          " " +
          selectedObject2.lastName,
      };

      form.setFieldsValue(newItem);
    } else {
      // Handle the case when selectedObject2 is null or undefined
      console.error("selectedObject2 is null or undefined");
    }

    console.log(value);
  };

  return (
    <div>
      <Row>
        <Col sm={24} md={24} lg={24} xs={24}>
          <Card>
            <Form
              form={form}
              layout="vertical"
              onFinish={handleUpdate2}
              className="w-full"
              autoComplete="off"
            >
              <Row gutter={12}>
                <Col
                  span={12}
                  style={{ borderRight: "1px dotted black", padding: "20px" }}
                >
                  <Row gutter={12}>
                    <Divider style={{ marginBottom: "30px" }}>
                      <Space>SENSUS TRANSACTION</Space>
                    </Divider>
                    <Col md={24} lg={12}>
                      <Form.Item
                        label="Select Patient"
                        required
                        name="patients_name2"
                        rules={[
                          {
                            required: true,
                            message: "Name is required",
                          },
                        ]}
                      >
                        <Select
                          showSearch
                          onChange={setData}
                          optionFilterProp="children"
                          filterOption={(input: any, option: any) => {
                            const optionText =
                              option.children && option.children.props
                                ? option.children.props.children
                                : "";
                            return (
                              optionText
                                .toString()
                                .toLowerCase()
                                .indexOf(input.toLowerCase()) >= 0
                            );
                          }}
                        >
                          {data2 &&
                            data2.map(data => (
                              <Select.Option value={data.id} key={data.id}>
                                {/* Your JSX code */}
                                <div>
                                  {data.firstName} {data.middleName}{" "}
                                  {data.lastName}
                                </div>
                              </Select.Option>
                            ))}
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={24} style={{ display: "none" }}>
                      <Form.Item
                        name="patients_name"
                        label="Name"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Classification is required",
                          },
                        ]}
                      >
                        <Input disabled />
                      </Form.Item>
                    </Col>
                    <Col span={24} style={{ display: "none" }}>
                      <Form.Item
                        name="laboratory_patients_id"
                        label="ID"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Classification is required",
                          },
                        ]}
                      >
                        <Input disabled />
                      </Form.Item>
                    </Col>
                    <Col md={24} lg={12}>
                      <Form.Item
                        name="classification"
                        label="Classification"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Classification is required",
                          },
                        ]}
                      >
                        <Input disabled />
                      </Form.Item>
                    </Col>
                    <Col span={24}>
                      <Form.Item
                        name="area"
                        label="Area"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Area is required",
                          },
                        ]}
                        initialValue={"DISCHARGE"}
                      >
                        <Select>
                          <Select.Option value="DISCHARGE" key="DISCHARGE">
                            DISCHARGE
                          </Select.Option>
                          <Select.Option value="OPD" key="OPD">
                            OPD
                          </Select.Option>
                          <Select.Option value="EMERGENCY" key="EMERGENCY">
                            EMERGENCY
                          </Select.Option>
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="bill_amount"
                        label="Total Bill Amount"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Total bill is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="bill_date"
                        label="Date of Billing"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Date is required",
                          },
                        ]}
                        initialValue={moment()}
                      >
                        <DatePicker format="YYYY-MM-DD" className="w-full" />
                      </Form.Item>
                    </Col>
                    <Divider style={{ marginBottom: "30px" }}>
                      <Space>DISCOUNT</Space>
                    </Divider>
                    <Col span={12}>
                      <Form.Item
                        name="discount_senior"
                        label="Senior"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Total bill is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="discount_pwd"
                        label="PWD"
                        required
                        rules={[
                          {
                            required: true,
                            message: "PWD is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>
                <Col
                  span={12}
                  style={{ borderLeft: "1px dotted black", padding: "20px" }}
                >
                  <Row gutter={12}>
                    <Divider style={{ marginBottom: "30px" }}>
                      <Space>MEDICAL ASSISTANCE</Space>
                    </Divider>
                    <Col span={12}>
                      <Form.Item
                        name="philhealth_deduction"
                        label="Philhealth"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Philhealth is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="maip_deduction"
                        label="MAIP"
                        required
                        rules={[
                          {
                            required: true,
                            message: "MAIP is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="aics_deduction"
                        label="AICS"
                        required
                        rules={[
                          {
                            required: true,
                            message: "AICS is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="himsugbo"
                        label="Himsugbo"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Himsugbo is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="malaskit"
                        label="Malasakit"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Malasakit is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="pcso"
                        label="PCSO"
                        required
                        rules={[
                          {
                            required: true,
                            message: "PCSO is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Divider style={{ marginBottom: "30px" }}>
                      <Space>OTHERS</Space>
                    </Divider>
                    <Col span={12}>
                      <Form.Item
                        name="cash"
                        label="Cash"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Cash is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="qfs"
                        label="QFS"
                        required
                        rules={[
                          {
                            required: true,
                            message: "QFS is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="promisri_note"
                        label="Promisori Note"
                        required
                        rules={[
                          {
                            required: true,
                            message: "Promisori Note is required",
                          },
                        ]}
                        initialValue={0}
                      >
                        <InputNumber
                          min={0}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                        />
                      </Form.Item>
                    </Col>
                    <Col span={24}>
                      {insertPatientStatus && alertVisible && (
                        <Alert
                          message={insertPatientStatus.text}
                          type={
                            insertPatientStatus.type === "success"
                              ? "success"
                              : "error"
                          }
                          showIcon
                          closable
                          onClose={handleAlertClose}
                        />
                      )}
                    </Col>
                    <Col
                      span={24}
                      style={{ marginTop: "20px", textAlign: "center" }}
                    >
                      <Divider>
                        <Button
                          // disabled={insertPatient.isLoading}
                          htmlType="submit"
                          type="primary"
                          // loading={insertPatient.isLoading}
                          className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline"
                        >
                          {insertsensustransaction.isLoading ? (
                            <>
                              <Spin /> <span>Please wait..</span>
                            </>
                          ) : (
                            "SAVE"
                          )}
                          {/* SAVE */}
                        </Button>
                      </Divider>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Form>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default PendingTaxDeclaration;
