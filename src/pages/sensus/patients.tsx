import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  Card,
  Alert,
  Space,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  FormOutlined,
  DeleteOutlined,
  PlusOutlined,
  TeamOutlined,
  UserOutlined,
  HomeOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const permission = session.data?.user?.permission;

  // const {
  //   data: data,
  //   isLoading: loading,
  //   refetch: refetch,
  // } = trpc.useQuery(["taxdec.getSensusPatient"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);
  const [openAddItem, setopenAddItem] = useState(false);

  const updatePatient = trpc.useMutation("taxdec.updatePatient", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const handleUpdate = async (values: any) => {
    try {
      updatePatient.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  //TABLE LIVE
  const [filters, setFilters] = useState({
    // status: null,
    startDate: null as string | null,
    endDate: null as string | null,
    name: "",
    class: "",
    area: "",
    hospital: "",
    // location: "",
    page: 1,
    pageSize: 10,
  });

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getSensusPatientLive", { ...filters }]);

  const handleTableChange = (pagination: any) => {
    setFilters({
      ...filters,
      page: pagination.current,
      pageSize: pagination.pageSize,
    });
    refetch();
  };
  const changeHospital = (value: any) => {
    const hospital = value.target.value;
    setFilters({
      ...filters,
      hospital: hospital,
      page: 1,
    });
    refetch();
  };
  const changeFullName = (value: any) => {
    const fullName = value.target.value;
    setFilters({
      ...filters,
      name: fullName,
      page: 1,
    });
    refetch();
  };
  const changeClass = (value: any) => {
    const class_name = value;
    setFilters({
      ...filters,
      class: class_name,
      page: 1,
    });
    refetch();
  };

  type Transaction = {
    created_at: Date | null;
    bill_date: Date | null;
    name: string;
    id: number;
    hospital: string;
    patients_name: string;
    class: string;
    area: string;
    bill_amount: number;
  };

  type Data = {
    transaction: Transaction[];
    total: number;
  };

  const transactionData = data as Data | undefined;
  const dataSource =
    transactionData?.transaction?.map((item, index) => ({
      ...item,
      key: index,
    })) || [];

  //END LIVE

  const handleDelete = (id: number) => {
    deletePatient.mutate({
      id: id,
    });
  };

  const deletePatient = trpc.useMutation("taxdec.deletePatient", {
    onSuccess() {
      message.success("Deleted successfully!");
      refetch();
    },
    onError(error) {
      message.error("Something went wrong!");
    },
  });

  function handleClick(value: any) {
    Modal.confirm({
      title: `Are you sure you want to delete ${value.lastName}, ${value.firstName}?`,
      onOk() {
        handleDelete(value.id);
      },
      onCancel() {
        // The user clicked Cancel
        // Do nothing or handle the cancel event here
      },
      okButtonProps: {
        className:
          "bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline",
      },
    });
  }

  const insertPatient = trpc.useMutation("taxdec.insertPatient", {
    onSuccess(data) {
      refetch();
      form.resetFields();
      setopenAddItem(false);
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });
  const handleUpdate2 = async (values: any) => {
    try {
      insertPatient.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  //TABLE
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      width: 100,
      align: "center" as "left" | "center" | "right",
      render: (text: any, record: any, index: any) =>
        (filters.page - 1) * filters.pageSize + index + 1,
    },
    {
      title: "Full Name",
      dataIndex: "firstName",
      width: 400,
      key: "firstName",
      render: (text: string, record: any) =>
        `${record.lastName}, ${record.firstName} ${
          record.middleName || ""
        }`.toUpperCase(),
    },

    {
      title: "Class",
      dataIndex: "class",
      key: "class",
      width: 200,
      align: "center" as "left" | "center" | "right",
      render: (text: any, record: any) =>
        record.status === null ? (
          ""
        ) : record.classification === "Indigent" ? (
          <Tag color="green" className="rounded-full">
            INDIGENT
          </Tag>
        ) : record.classification === "Paying" ? (
          <Tag color="blue" className="rounded-full">
            PAYING
          </Tag>
        ) : record.classification === "Non Philhealth" ? (
          <Tag color="geekblue" className="rounded-full">
            NON PHILHEALTH
          </Tag>
        ) : record.classification === "Philhealth" ? (
          <Tag color="purple" className="rounded-full">
            PHILHEALTH
          </Tag>
        ) : (
          ""
        ),
    },
    {
      title: "Hospital",
      dataIndex: "hospital",
      width: 500,
      key: "hospital",
      align: "center" as "left" | "center" | "right",
      render: (value: any, record: any) => record.hospital.toUpperCase(),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      align: "center" as "left" | "center" | "right",
      filters: [
        {
          text: "Active",
          value: "Active",
        },
        {
          text: "Inactive",
          value: "Inactive",
        },
        {
          text: "Expired",
          value: "Expired",
        },
      ],
      onFilter: (value: any, record: any) => record.status.includes(value),
      ellipsis: true,
      render: (text: any, record: any) =>
        record.status === null ? (
          ""
        ) : record.status === "Active" ? (
          <Tag color="green" className="rounded-full">
            ACTIVE
          </Tag>
        ) : record.status === "Inactive" ? (
          <Tag color="volcano" className="rounded-full">
            INACTIVE
          </Tag>
        ) : record.status === "Expired" ? (
          <Tag color="magenta" className="rounded-full">
            EXPIRED
          </Tag>
        ) : (
          ""
        ),
    },
    {
      title: "Actions",
      key: "id",
      align: "center" as "left" | "center" | "right",
      render: (item: any) => {
        return (
          <Space>
            <Tooltip title="Edit">
              <Button
                onClick={() => handleEdit(item)}
                className="bg-blue-600 text-white ml-1"
                shape="circle"
                icon={<FormOutlined />}
              ></Button>
            </Tooltip>
            <Tooltip title="Delete">
              <Button
                onClick={() => handleClick(item)}
                icon={<DeleteOutlined />}
                shape="circle"
                danger
                type="primary"
              />
            </Tooltip>
          </Space>
        );
      },
    },
  ];

  if (permission === "admin") {
    // Remove the column at index 8
    columns.splice(4, 2);
  }
  return (
    <div>
      <Card className="fade-in">
        <div className="flex p-3 bg-custom-blue mb-5 rounded ">
          <span className="uppercase text-2xl font-bold text-white">
            Patient Master List
          </span>
          {permission !== "admin" && (
            <Button
              className="ml-auto text-custom-blue bg-white border-custom-blue flex items-center  rounded-full"
              icon={<PlusOutlined />}
              onClick={() => setopenAddItem(true)}
            >
              CREATE{" "}
            </Button>
          )}
        </div>
        <Row gutter={12}>
          <Col md={6} xs={24}>
            <Form layout="vertical">
              <Form.Item
                name="searchname"
                label={
                  <>
                    <UserOutlined className="mr-1" />
                    Name
                  </>
                }
              >
                <Input
                  placeholder="Search name..."
                  onChange={changeFullName}
                  autoComplete="off"
                  allowClear
                />
              </Form.Item>
            </Form>
          </Col>
          {permission === "admin" && (
            <Col md={6} xs={24}>
              <Form layout="vertical">
                <Form.Item
                  name="hospital"
                  label={
                    <>
                      <HomeOutlined className="mr-1" />
                      Hospital
                    </>
                  }
                >
                  <Input
                    placeholder="Search hospital..."
                    onChange={changeHospital}
                    autoComplete="off"
                    allowClear
                    disabled={permission !== "admin"}
                  />
                </Form.Item>
              </Form>
            </Col>
          )}
          <Col md={4} xs={24}>
            <Form layout="vertical">
              <Form.Item
                name="class"
                label={
                  <>
                    <TeamOutlined className="mr-1" />
                    Class
                  </>
                }
              >
                <Select onChange={changeClass}>
                  <Select.Option value="">All Class</Select.Option>
                  <Select.Option value="Indigent">Indigent</Select.Option>
                  <Select.Option value="Paying">Paying</Select.Option>
                  {/* <Select.Option value="POS">POS</Select.Option>
                      <Select.Option value="PWD">PWD</Select.Option> */}
                  <Select.Option value="Non Philhealth">
                    Non Philhealth
                  </Select.Option>
                  <Select.Option value="Philhealth">Philhealth</Select.Option>
                </Select>
              </Form.Item>
            </Form>
          </Col>
        </Row>
        <div>
          <Table
            columns={columns}
            size="middle"
            dataSource={dataSource}
            // onChange={handleChange}
            loading={loading}
            pagination={{
              current: filters.page,
              pageSize: filters.pageSize,
              total: transactionData?.total,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total.toLocaleString()} items`,
            }}
            scroll={{ x: true }}
            onChange={handleTableChange}
            // responsive
          />
        </div>
      </Card>
      {/* ADD PATIENT     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> ADD ITEM
            </Space>
          </>
        }
        open={openAddItem}
        onCancel={() => setopenAddItem(false)}
        footer={null}
        width={350}
      >
        <Form
          form={form}
          layout="vertical"
          onFinish={handleUpdate2}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24}>
              <Form.Item
                name="lastName"
                label="Last Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Last Name is required",
                  },
                ]}
              >
                <Input placeholder="Last Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="firstName"
                label="First Name"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name="middleName" label="Middle Name">
                <Input placeholder="Middle Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="classification"
                label="Classification"
                required
                rules={[
                  {
                    required: true,
                    message: "Classification is required",
                  },
                ]}
                initialValue={"Indigent"}
              >
                <Select>
                  <Select.Option value="Indigent" key="Indigent">
                    Indigent
                  </Select.Option>
                  <Select.Option value="Paying" key="Paying">
                    Paying
                  </Select.Option>
                  <Select.Option value="Non Philhealth" key="Non Philhealth">
                    Non Philhealth
                  </Select.Option>
                  <Select.Option value="Philhealth" key="Philhealth">
                    Philhealth
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>

            <Col span={24} style={{ textAlign: "center" }}>
              <Button
                // disabled={insertPatient.isLoading}
                htmlType="submit"
                type="primary"
                style={{ width: "100px" }}
                // loading={insertPatient.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline"
              >
                {insertPatient.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "SAVE"
                )}
                {/* SAVE */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
      {/* UPDATE PATIENT     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> UPDATE PATIENT
            </Space>
          </>
        }
        open={openUpdateItem}
        onCancel={() => setopenUpdateItem(false)}
        footer={null}
      >
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleUpdate}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="id"
                label="ID"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="firstName"
                label="First Name"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item name="middleName" label="Middle Name">
                <Input placeholder="Middle Name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="lastName"
                label="Last Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Last Name is required",
                  },
                ]}
              >
                <Input placeholder="Last Name" />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="classification"
                label="Class"
                required
                rules={[
                  {
                    required: true,
                    message: "Class is required",
                  },
                ]}
              >
                <Select disabled>
                  <Select.Option value="Paying" key="Paying">
                    Paying
                  </Select.Option>
                  <Select.Option value="Indigent" key="Indigent">
                    Indigent
                  </Select.Option>
                  <Select.Option value="Non Philhealth" key="Non Philhealth">
                    Non Philhealth
                  </Select.Option>
                  <Select.Option value="Philhealth" key="Philhealth">
                    Philhealth
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="status"
                label="Status"
                required
                rules={[
                  {
                    required: true,
                    message: "Gender is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="Active" key="Active">
                    Active
                  </Select.Option>
                  <Select.Option value="Inactive" key="Inactive">
                    Inactive
                  </Select.Option>
                  <Select.Option value="Expired" key="Expired">
                    Expired
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>

            <Col span={24}>
              <Button
                disabled={updatePatient.isLoading}
                htmlType="submit"
                type="primary"
                loading={updatePatient.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {updatePatient.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};

export default PendingTaxDeclaration;
