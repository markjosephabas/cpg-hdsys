import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  Card,
  Alert,
  Space,
  DatePicker,
  InputNumber,
} from "antd";
import { NextPage } from "next";
import moment, { Moment } from "moment";
import { trpc } from "../../utils/trpc";
import React, { useState } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import type { ColumnsType, ColumnType } from "antd/es/table";

import {
  SearchOutlined,
  FormOutlined,
  DeleteOutlined,
  PlusOutlined,
  TeamOutlined,
  UserOutlined,
  HomeOutlined,
  CalendarOutlined,
} from "@ant-design/icons";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [dateRange, setDateRange] = useState<
    [moment.Moment | null, moment.Moment | null]
  >([null, null]);

  const [dateRange3, setDateRange3] = useState<
    [moment.Moment | null, moment.Moment | null]
  >([null, null]);
  const [dateRange4, setDateRange4] = useState<
    [moment.Moment | null, moment.Moment | null]
  >([null, null]);
  const handleCheck = () => {
    setDateRange4(dateRange3);
  };
  const handleDateRangeChange2 = (dates: any) => {
    dates ? setDateRange3(dates) : setDateRange3([null, null]);
  };

  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const [openAddItem, setopenAddItem] = useState(false);
  const permission = session.data?.user?.permission;

  const startDate = dateRange4[0]
    ? moment(dateRange4[0]).format("YYYY-MM-DD")
    : moment().format("YYYY-MM-DD");
  const endDate = dateRange4[1]
    ? moment(dateRange4[1]).format("YYYY-MM-DD")
    : moment().format("YYYY-MM-DD");

  const {
    data: data2,
    isLoading: loading2,
    refetch: refetch2,
  } = trpc.useQuery(["taxdec.getSensusPatient"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);

  const updateSensusTransaction = trpc.useMutation(
    "taxdec.updateSensusTransaction",
    {
      onSuccess(data) {
        refetch();
        form2.resetFields();
        setopenUpdateItem(false);
        message.success("Updated susccessfully");
      },
      onError(error) {
        message.error(`${error.message}`);
      },
    }
  );

  const handleEdit = (item: any) => {
    const bill_date = moment(item.bill_date);

    form2.setFieldsValue({
      ...item,
      bill_date: bill_date,
    });
    setopenUpdateItem(true);
  };

  const handleUpdate = async (values: any) => {
    try {
      updateSensusTransaction.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const handleDelete = (id: number) => {
    deleteSensusTransaction.mutate({
      id: id,
    });
  };

  const deleteSensusTransaction = trpc.useMutation(
    "taxdec.deleteSensusTransaction",
    {
      onSuccess() {
        message.success("Deleted successfully!");
        refetch();
      },
      onError(error) {
        message.error("Something went wrong!");
      },
    }
  );

  function handleClick(value: any) {
    Modal.confirm({
      title: `Are you sure you want to delete the transaction associated with  ${value.patients_name}?`,
      onOk() {
        handleDelete(value.id);
      },
      onCancel() {
        // The user clicked Cancel
        // Do nothing or handle the cancel event here
      },
      okButtonProps: {
        className:
          "bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline",
      },
    });
  }

  const insertsensustransaction = trpc.useMutation(
    "taxdec.insertsensustransaction",
    {
      onSuccess(data) {
        message.success("Added successfully!");
        refetch();
        form.resetFields();
        setopenAddItem(false);
      },
      onError(error) {
        message.error(`${error.message}`);
      },
    }
  );

  const handleUpdate2 = async (values: any) => {
    try {
      insertsensustransaction.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const setData = (value: any) => {
    const selectedObject2 = value
      ? data2?.find(item => item.id === value)
      : null;

    if (selectedObject2 && selectedObject2.id != null) {
      const middleName = selectedObject2.middleName
        ? selectedObject2.middleName
        : "";
      const newItem = {
        classification: selectedObject2.classification,
        laboratory_patients_id: selectedObject2.id,
        patients_name:
          selectedObject2.lastName +
          ", " +
          selectedObject2.firstName +
          " " +
          middleName,
      };

      form.setFieldsValue(newItem);
    } else {
      // Handle the case when selectedObject2 is null or undefined
      console.error("selectedObject2 is null or undefined");
    }

    // console.log(value);
  };

  const checkBalance = () => {
    const val = form.getFieldsValue();
    const balance =
      val.bill_amount -
      (val.cash +
        val.discount_pwd +
        val.discount_senior +
        val.himsugbo +
        val.maip_deduction +
        val.malaskit +
        val.pcso +
        val.philhealth_deduction +
        val.promisri_note +
        val.aics_deduction +
        val.qfs);

    form.setFieldsValue({
      balance: balance,
    });

    // console.log(val.bill_amount);
  };

  //TABLE LIVE
  const [filters, setFilters] = useState({
    // status: null,
    startDate: null as string | null,
    endDate: null as string | null,
    name: "",
    class: "",
    area: "",
    hospital: "",
    // location: "",
    page: 1,
    pageSize: 10,
  });

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getSensusTranscationLive", { ...filters }]);

  const handleTableChange = (pagination: any) => {
    setFilters({
      ...filters,
      page: pagination.current,
      pageSize: pagination.pageSize,
    });
    refetch();
  };
  const changeHospital = (value: any) => {
    const hospital = value.target.value;
    setFilters({
      ...filters,
      hospital: hospital,
      page: 1,
    });
    refetch();
  };
  const changeFullName = (value: any) => {
    const fullName = value.target.value;
    setFilters({
      ...filters,
      name: fullName,
      page: 1,
    });
    refetch();
  };
  const changeClass = (value: any) => {
    const class_name = value;
    setFilters({
      ...filters,
      class: class_name,
      page: 1,
    });
    refetch();
  };
  const changeArea = (value: any) => {
    const area = value;
    setFilters({
      ...filters,
      area: area,
      page: 1,
    });
    refetch();
  };

  type Transaction = {
    created_at: Date | null;
    bill_date: Date | null;
    name: string;
    id: number;
    hospital: string;
    patients_name: string;
    class: string;
    area: string;
    bill_amount: number;
  };

  type Data = {
    transaction: Transaction[];
    total: number;
  };

  const transactionData = data as Data | undefined;
  const dataSource =
    transactionData?.transaction?.map((item, index) => ({
      ...item,
      key: index,
    })) || [];

  const handleDateRangeChange = (
    dates: [Moment | null, Moment | null] | null,
    formatString: [string, string]
  ) => {
    if (dates) {
      setFilters({
        ...filters,
        startDate: dates[0] ? dates[0].format("YYYY-MM-DD") : "",
        endDate: dates[1] ? dates[1].format("YYYY-MM-DD") : null,
        page: 1,
      });
      refetch();
    } else {
      setFilters({
        ...filters,
        startDate: null,
        endDate: null,
        page: 1,
      });
      refetch();
    }
  };
  //END LIVE

  const columns: ColumnsType<Transaction> = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      align: "center",
      render: (text: any, record: any, index: any) =>
        (filters.page - 1) * filters.pageSize + index + 1,
    },
    {
      title: "Patient",
      dataIndex: "patients_name",
      key: "patients_name",

      render: (text: string, record: any) =>
        `${record.patients_name}`.toUpperCase(),
    },
    {
      title: "Class",
      dataIndex: "class",
      key: "class",
      align: "center",
      render: (text: any, record: any) =>
        record.status === null ? (
          ""
        ) : record.classification === "Indigent" ? (
          <Tag color="green" className="rounded-full">
            INDIGENT
          </Tag>
        ) : record.classification === "Paying" ? (
          <Tag color="blue" className="rounded-full">
            PAYING
          </Tag>
        ) : record.classification === "Non Philhealth" ? (
          <Tag color="geekblue" className="rounded-full">
            NON PHILHEALTH
          </Tag>
        ) : record.classification === "Philhealth" ? (
          <Tag color="purple" className="rounded-full">
            PHILHEALTH
          </Tag>
        ) : (
          ""
        ),
    },
    {
      title: "Area",
      dataIndex: "area",
      key: "area",
      align: "center",
      render: (text, record) =>
        record.area === null ? (
          ""
        ) : record.area === "DISCHARGE" ? (
          <Tag color="green" className="rounded-full">
            DISCHARGE
          </Tag>
        ) : record.area === "OPD" ? (
          <Tag color="blue" className="rounded-full">
            OPD
          </Tag>
        ) : record.area === "EMERGENCY" ? (
          <Tag color="purple" className="rounded-full">
            EMERGENCY
          </Tag>
        ) : record.area === "ADMITTED" ? (
          <Tag color="magenta" className="rounded-full">
            ADMITTED
          </Tag>
        ) : (
          ""
        ),
    },
    {
      title: "Total Bill",
      dataIndex: "bill_amount",
      key: "bill_amount",
      align: "center",
      render: (text: string, record: any) =>
        record.bill_amount.toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        }),
    },
    {
      title: "Date of Bill",
      dataIndex: "bill_date",
      key: "bill_date",
      align: "center",
      render: (text: any, record: any) =>
        record.bill_date ? moment(record.bill_date).format("LLL") : "",
    },
    {
      title: "Date Created",
      dataIndex: "created_at",
      key: "created_at",
      align: "center",
      render: (text: any, record: any) =>
        record.created_at ? moment(record.created_at).format("LLL") : "",
    },
    {
      title: "Actions",
      key: "id",
      align: "center",
      render: (item: any) => {
        return (
          <Space>
            <Tooltip title="Edit">
              <Button
                onClick={() => handleEdit(item)}
                className="bg-blue-600 text-white ml-1"
                shape="circle"
                icon={<FormOutlined />}
              ></Button>
            </Tooltip>
            <Tooltip title="Delete">
              <Button
                onClick={() => handleClick(item)}
                icon={<DeleteOutlined />}
                shape="circle"
                danger
                type="primary"
              />
            </Tooltip>
          </Space>
        );
      },
    },
  ];

  // Define the type for your columns
  type ColumnType = {
    title: string;
    dataIndex: string;
    key: string;
    align?: "left" | "right" | "center";
    render?: (text: any, record: any) => any;
  };

  if (permission === "admin") {
    const hospitalColumn: ColumnType = {
      title: "Hospital",
      dataIndex: "hospital",
      key: "hospital",
      align: "center",
      render: (text: any, record: any) => record.hospital.toUpperCase(),
    };
    // Insert the hospital column at the desired position (e.g., third position)
    columns.splice(7, 1, hospitalColumn);
    // Remove the column at index 8
    // columns.splice(8, 1);
  }
  return (
    <div>
      <Card className="fade-in">
        <Row>
          <Col span={24}>
            <div className="flex flex-wrap p-3 bg-custom-blue mb-5 rounded ">
              <div className=" flex flex-wrap gap-2 items-center">
                <span className="font-bold text-white text-2xl">
                  TRANSACTIONS
                </span>
              </div>
              {permission !== "admin" && (
                <Tooltip title="CREATE">
                  <Button
                    className="ml-auto flex text-custom-blue bg-white border-custom-blue rounded-full justify-center items-center"
                    icon={<PlusOutlined />}
                    onClick={() => setopenAddItem(true)}
                  >
                    CREATE
                  </Button>
                </Tooltip>
              )}
            </div>
            <span className="text-2xl font-bold">FILTER</span>
            <Row gutter={12}>
              <Col md={6} xs={24}>
                <Form layout="vertical">
                  <Form.Item
                    name="searchname"
                    label={
                      <>
                        <UserOutlined className="mr-1" />
                        Name
                      </>
                    }
                  >
                    <Input
                      placeholder="Search name..."
                      onChange={changeFullName}
                      autoComplete="off"
                      allowClear
                    />
                  </Form.Item>
                </Form>
              </Col>
              {permission === "admin" && (
                <Col md={4} xs={24}>
                  <Form layout="vertical">
                    <Form.Item
                      name="hospital"
                      label={
                        <>
                          <HomeOutlined className="mr-1" />
                          Hospital
                        </>
                      }
                    >
                      <Input
                        placeholder="Search hospital..."
                        onChange={changeHospital}
                        autoComplete="off"
                        allowClear
                        disabled={permission !== "admin"}
                      />
                    </Form.Item>
                  </Form>
                </Col>
              )}
              <Col md={4} xs={24}>
                <Form layout="vertical">
                  <Form.Item
                    name="class"
                    label={
                      <>
                        <TeamOutlined className="mr-1" />
                        Class
                      </>
                    }
                  >
                    <Select onChange={changeClass}>
                      <Select.Option value="">All Class</Select.Option>
                      <Select.Option value="Indigent">Indigent</Select.Option>
                      <Select.Option value="Paying">Paying</Select.Option>
                      {/* <Select.Option value="POS">POS</Select.Option>
                      <Select.Option value="PWD">PWD</Select.Option> */}
                      <Select.Option value="Non Philhealth">
                        Non Philhealth
                      </Select.Option>
                      <Select.Option value="Philhealth">
                        Philhealth
                      </Select.Option>
                    </Select>
                  </Form.Item>
                </Form>
              </Col>
              <Col md={4} xs={24}>
                <Form layout="vertical">
                  <Form.Item
                    name="class"
                    label={
                      <>
                        <TeamOutlined className="mr-1" />
                        Area
                      </>
                    }
                  >
                    <Select onChange={changeArea}>
                      <Select.Option value="">ALL AREA</Select.Option>
                      <Select.Option value="DISCHARGE" key="DISCHARGE">
                        DISCHARGE
                      </Select.Option>
                      <Select.Option value="OPD" key="OPD">
                        OPD
                      </Select.Option>
                      <Select.Option value="EMERGENCY" key="EMERGENCY">
                        EMERGENCY
                      </Select.Option>
                      <Select.Option value="ADMITTED" key="ADMITTED">
                        ADMITTED
                      </Select.Option>
                      <Select.Option value="WALK-IN" key="WALK-IN">
                        WALK-IN
                      </Select.Option>
                    </Select>
                  </Form.Item>
                </Form>
              </Col>
              <Col md={6} xs={24}>
                <Form layout="vertical">
                  <Form.Item
                    name="hospital"
                    label={
                      <>
                        <CalendarOutlined className="mr-1" />
                        Date of Bill Coverage
                      </>
                    }
                  >
                    <DatePicker.RangePicker
                      value={dateRange}
                      onChange={handleDateRangeChange}
                      format="YYYY-MM-DD"
                    />
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            <Table
              size="middle"
              bordered
              columns={columns}
              dataSource={dataSource}
              // onChange={handleChange}
              loading={loading}
              pagination={{
                current: filters.page,
                pageSize: filters.pageSize,
                total: transactionData?.total,
                showTotal: (total, range) =>
                  `${range[0]}-${range[1]} of ${total.toLocaleString()} items`,
              }}
              scroll={{ x: true }}
              onChange={handleTableChange}
              // responsive
            />
          </Col>
        </Row>
      </Card>
      <Modal
        open={openAddItem}
        onCancel={() => setopenAddItem(false)}
        footer={null}
        width={600}
      >
        <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center mt-5">
          PERSONAL INFORMATION
        </div>
        <Form
          form={form}
          layout="vertical"
          onFinish={handleUpdate2}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={12}>
              <Form.Item
                label="Select Patient"
                required
                name="patients_name2"
                rules={[
                  {
                    required: true,
                    message: "Name is required",
                  },
                ]}
              >
                <Select
                  showSearch
                  onChange={setData}
                  optionFilterProp="children"
                  filterOption={(input: any, option: any) => {
                    const optionText =
                      option.children && option.children.props
                        ? option.children.props.children
                        : "";
                    return (
                      optionText
                        .toString()
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    );
                  }}
                >
                  {data2 &&
                    data2.map(data => (
                      <Select.Option value={data.id} key={data.id}>
                        <div>
                          {data.firstName} {data.middleName} {data.lastName}
                        </div>
                      </Select.Option>
                    ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="patients_name"
                label="Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Classification is required",
                  },
                ]}
              >
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="laboratory_patients_id"
                label="ID"
                required
                rules={[
                  {
                    required: true,
                    message: "Classification is required",
                  },
                ]}
              >
                <Input disabled />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="classification"
                label="Classification"
                required
                rules={[
                  {
                    required: true,
                    message: "Classification is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="Indigent" key="Indigent">
                    Indigent
                  </Select.Option>
                  <Select.Option value="Paying" key="Paying">
                    Paying
                  </Select.Option>
                  <Select.Option value="Non Philhealth" key="Non Philhealth">
                    Non Philhealth
                  </Select.Option>
                  <Select.Option value="Philhealth" key="Philhealth">
                    Philhealth
                  </Select.Option>
                </Select>
                {/* <Input disabled /> */}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="area"
                label="Area"
                required
                rules={[
                  {
                    required: true,
                    message: "Area is required",
                  },
                ]}
                initialValue={"DISCHARGE"}
              >
                <Select>
                  <Select.Option value="DISCHARGE" key="DISCHARGE">
                    DISCHARGE
                  </Select.Option>
                  <Select.Option value="OPD" key="OPD">
                    OPD
                  </Select.Option>
                  <Select.Option value="EMERGENCY" key="EMERGENCY">
                    EMERGENCY
                  </Select.Option>
                  <Select.Option value="ADMITTED" key="ADMITTED">
                    ADMITTED
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="bill_date"
                label="Date of Billing"
                required
                rules={[
                  {
                    required: true,
                    message: "Date is required",
                  },
                ]}
                initialValue={moment()}
              >
                <DatePicker format="YYYY-MM-DD" className="w-full" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="bill_amount"
                label="Total Bill Amount"
                required
                rules={[
                  {
                    required: true,
                    message: "Total bill is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="balance" label="Balance">
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                  readOnly
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center">
                DISCOUNT
              </div>
            </Col>
            <Col span={12}>
              <Form.Item
                name="discount_senior"
                label="Senior"
                required
                rules={[
                  {
                    required: true,
                    message: "Total bill is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="discount_pwd"
                label="PWD"
                required
                rules={[
                  {
                    required: true,
                    message: "PWD is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center">
                MEDICAL ASSISTANCE
              </div>
            </Col>
            <Col span={12}>
              <Form.Item
                name="philhealth_deduction"
                label="Philhealth"
                required
                rules={[
                  {
                    required: true,
                    message: "Philhealth is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="maip_deduction"
                label="MAIP"
                required
                rules={[
                  {
                    required: true,
                    message: "MAIP is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="aics_deduction"
                label="AICS"
                required
                rules={[
                  {
                    required: true,
                    message: "AICS is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="himsugbo"
                label="Himsugbo"
                required
                rules={[
                  {
                    required: true,
                    message: "Himsugbo is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="malaskit"
                label="Malasakit"
                required
                rules={[
                  {
                    required: true,
                    message: "Malasakit is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="pcso"
                label="PCSO"
                required
                rules={[
                  {
                    required: true,
                    message: "PCSO is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>

            <Col span={24}>
              <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center">
                OTHERS
              </div>
            </Col>
            <Col span={12}>
              <Form.Item
                name="cash"
                label="Cash"
                required
                rules={[
                  {
                    required: true,
                    message: "Cash is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="qfs"
                label="QFS"
                required
                rules={[
                  {
                    required: true,
                    message: "QFS is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="promisri_note"
                label="Promisori Note"
                required
                rules={[
                  {
                    required: true,
                    message: "Promisori Note is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  onChange={checkBalance}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={updateSensusTransaction.isLoading}
                htmlType="submit"
                type="primary"
                loading={updateSensusTransaction.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {updateSensusTransaction.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "SAVE"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
      {/* UPDATE SENSUS     */}
      <Modal
        // title={
        //   <>
        //     <Space>
        //       <BiUser /> UPDATE TRANSACTION
        //     </Space>
        //   </>
        // }
        open={openUpdateItem}
        onCancel={() => setopenUpdateItem(false)}
        footer={null}
        width={600}
      >
        <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center">
          PERSONAL INFORMATION
        </div>
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleUpdate}
          className="w-full"
          autoComplete="off"
        >
          <Col span={24} style={{ display: "none" }}>
            <Form.Item
              name="id"
              label="ID"
              required
              rules={[
                {
                  required: true,
                  message: "First Name is required",
                },
              ]}
            >
              <Input placeholder="First Name" />
            </Form.Item>
          </Col>

          <Row gutter={12}>
            <Col md={24} lg={12}>
              <Form.Item
                label="Patient Name"
                required
                name="patients_name"
                rules={[
                  {
                    required: true,
                    message: "Name is required",
                  },
                ]}
              >
                <Input placeholder="Name" readOnly />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="classification"
                label="Classification"
                required
                rules={[
                  {
                    required: true,
                    message: "Classification is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="Indigent" key="Indigent">
                    Indigent
                  </Select.Option>
                  <Select.Option value="Paying" key="Paying">
                    Paying
                  </Select.Option>
                  <Select.Option value="Non Philhealth" key="Non Philhealth">
                    Non Philhealth
                  </Select.Option>
                  <Select.Option value="Philhealth" key="Philhealth">
                    Philhealth
                  </Select.Option>
                </Select>
                {/* <Input disabled /> */}
              </Form.Item>
            </Col>
            <Col span={24} lg={12}>
              <Form.Item
                name="area"
                label="Area"
                required
                rules={[
                  {
                    required: true,
                    message: "Area is required",
                  },
                ]}
                initialValue={"DISCHARGE"}
              >
                <Select>
                  <Select.Option value="DISCHARGE" key="DISCHARGE">
                    DISCHARGE
                  </Select.Option>
                  <Select.Option value="OPD" key="OPD">
                    OPD
                  </Select.Option>
                  <Select.Option value="EMERGENCY" key="EMERGENCY">
                    EMERGENCY
                  </Select.Option>
                  <Select.Option value="ADMITTED" key="ADMITTED">
                    ADMITTED
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="bill_amount"
                label="Total Bill Amount"
                required
                rules={[
                  {
                    required: true,
                    message: "Total bill is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="bill_date"
                label="Date of Billing"
                required
                rules={[
                  {
                    required: true,
                    message: "Date is required",
                  },
                ]}
              >
                <DatePicker format="YYYY-MM-DD" className="w-full" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center">
                DISCOUNT
              </div>
            </Col>
            <Col span={12}>
              <Form.Item
                name="discount_senior"
                label="Senior"
                required
                rules={[
                  {
                    required: true,
                    message: "Total bill is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="discount_pwd"
                label="PWD"
                required
                rules={[
                  {
                    required: true,
                    message: "PWD is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center">
                MEDICAL ASSISTANCE
              </div>
            </Col>
            <Col span={12}>
              <Form.Item
                name="philhealth_deduction"
                label="Philhealth"
                required
                rules={[
                  {
                    required: true,
                    message: "Philhealth is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="maip_deduction"
                label="MAIP"
                required
                rules={[
                  {
                    required: true,
                    message: "MAIP is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="aics_deduction"
                label="AICS"
                required
                rules={[
                  {
                    required: true,
                    message: "AICS is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="himsugbo"
                label="Himsugbo"
                required
                rules={[
                  {
                    required: true,
                    message: "Himsugbo is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="malaskit"
                label="Malasakit"
                required
                rules={[
                  {
                    required: true,
                    message: "Malasakit is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="pcso"
                label="PCSO"
                required
                rules={[
                  {
                    required: true,
                    message: "PCSO is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>

            <Col span={24}>
              <div className="flex flex-wrap p-2 font-bold bg-slate-900 text-white mb-4 rounded justify-center">
                OTHERS
              </div>
            </Col>
            <Col span={12}>
              <Form.Item
                name="cash"
                label="Cash"
                required
                rules={[
                  {
                    required: true,
                    message: "Cash is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="qfs"
                label="QFS"
                required
                rules={[
                  {
                    required: true,
                    message: "QFS is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="promisri_note"
                label="Promisori Note"
                required
                rules={[
                  {
                    required: true,
                    message: "Promisori Note is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={updateSensusTransaction.isLoading}
                htmlType="submit"
                type="primary"
                loading={updateSensusTransaction.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {updateSensusTransaction.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};

export default PendingTaxDeclaration;
