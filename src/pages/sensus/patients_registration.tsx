import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  InputNumber,
  Card,
  Alert,
  Space,
  Divider,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  FileDoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

interface InsertPatientStatus {
  type: "success" | "error";
  text: string;
}

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality;
  const permission = session.data?.user?.permission;

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getHospital"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);
  const [alertVisible, setAlertVisible] = useState(true);

  const updateHospital = trpc.useMutation("taxdec.updateHospital", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const [insertPatientStatus, setInsertPatientStatus] =
    useState<InsertPatientStatus | null>(null);

  const insertPatient = trpc.useMutation("taxdec.insertPatient", {
    onSuccess(data) {
      setInsertPatientStatus({
        type: "success",
        text: "Patient inserted successfully.",
      });
      refetch();
      form.resetFields();
      setAlertVisible(true);
    },
    onError(error) {
      setInsertPatientStatus({ type: "error", text: error.message });
      message.error(`${error.message}`);
      setAlertVisible(true);
    },
  });

  const handleAlertClose = () => {
    setInsertPatientStatus(null);
  };

  useEffect(() => {
    if (insertPatientStatus && insertPatientStatus.text !== "") {
      // Automatically close the alert after 3 seconds
      const timer = setTimeout(() => {
        handleAlertClose(); // Close the alert
      }, 3000);

      // Clear the timer if the component unmounts or insertPatientStatus changes
      return () => clearTimeout(timer);
    }
  }, [insertPatientStatus]);

  const handleUpdate2 = async (values: any) => {
    try {
      insertPatient.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const handleUpdate = async (values: any) => {
    try {
      updateHospital.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Row>
        <Col>
          <Card
            style={{ width: "400px" }}
            title={
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  gap: 5,
                }}
              >
                <UserOutlined size={20} />
                <p>PATIENTS REGISTRATION</p>
              </div>
            }
          >
            <Form
              form={form}
              layout="vertical"
              onFinish={handleUpdate2}
              className="w-full"
              autoComplete="off"
            >
              <Row gutter={12}>
                <Col span={24}>
                  <Form.Item
                    name="lastName"
                    label="Last Name"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Last Name is required",
                      },
                    ]}
                  >
                    <Input placeholder="Last Name" />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    name="firstName"
                    label="First Name"
                    required
                    rules={[
                      {
                        required: true,
                        message: "First Name is required",
                      },
                    ]}
                  >
                    <Input placeholder="First Name" />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    name="middleName"
                    label="Middle Name"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Middle Name is required",
                      },
                    ]}
                  >
                    <Input placeholder="Middle Name" />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    name="classification"
                    label="Classification"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Classification is required",
                      },
                    ]}
                    initialValue={"Indigent"}
                  >
                    <Select>
                      <Select.Option value="Indigent" key="Indigent">
                        Indigent
                      </Select.Option>
                      <Select.Option value="Paying" key="Paying">
                        Paying
                      </Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={24}>
                  {insertPatientStatus && alertVisible && (
                    <Alert
                      message={insertPatientStatus.text}
                      type={
                        insertPatientStatus.type === "success"
                          ? "success"
                          : "error"
                      }
                      showIcon
                      closable
                      onClose={handleAlertClose}
                    />
                  )}
                </Col>
                <Col span={24} style={{ textAlign: "center" }}>
                  <Button
                    // disabled={insertPatient.isLoading}
                    htmlType="submit"
                    type="primary"
                    style={{ width: "100px" }}
                    // loading={insertPatient.isLoading}
                    className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline"
                  >
                    {insertPatient.isLoading ? (
                      <>
                        <Spin /> <span>Please wait..</span>
                      </>
                    ) : (
                      "SAVE"
                    )}
                    {/* SAVE */}
                  </Button>
                </Col>
              </Row>
            </Form>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default PendingTaxDeclaration;
