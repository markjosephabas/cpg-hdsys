import { Button, Input, message, Form, Select, Spin, Card } from "antd";
import { NextPage } from "next";

import { trpc } from "../../utils/trpc";
import { useSession } from "next-auth/react";
import React, { useState } from "react";
// import { useRouter } from "next/router";
// import { useSession } from "next-auth/react";
import { BiUser, BiLockAlt } from "react-icons/bi";

import { hash } from "bcryptjs";

const Registeration: NextPage = () => {
  const session = useSession();
  const hasPermission = session.data?.user?.permission === "admin";
  // const [municipalities] = useState([
  //   "Cebu Province",
  //   "District Hospital - Danao",
  //   "District Hospital - Bogo",
  //   "District Hospital - Camotes",
  //   "District Hospital - Balamban",
  // ]);

  const {
    data: data,
    isLoading: loading2,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getHospital"]);

  const [form] = Form.useForm();
  // const [submitted, setSubmitted] = useState<boolean>(false);
  const [loading, setLoading] = useState<any>(false);
  // const router = useRouter()

  const insertUser = trpc.useMutation("taxdec.register", {
    onSuccess() {
      message.success("Registered successfully!");
      form.resetFields();
      // setSubmitted(true);
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleFinishForm = async (values: any) => {
    // console.log("values",values)
    const newPass = await hash(values.password, 10);
    setLoading(true);
    insertUser.mutate({
      ...values,
      newPass: newPass,
    });
    setLoading(false);
  };

  const { Option } = Select;

  if (!hasPermission) {
    return (
      <div
        style={{
          textAlign: "center",
          padding: "20px",
          border: "1px solid #ff4d4f",
          borderRadius: "5px",
          backgroundColor: "#fff1f0",
          color: "#ff4d4f",
        }}
      >
        <p
          style={{ marginBottom: "10px", fontSize: "18px", fontWeight: "bold" }}
        >
          Access Denied
        </p>
        <p>{"You don't have the required permission to access this page."}</p>
      </div>
    );
  }

  return (
    <Card style={{ width: "400px" }} className="fade-in">
      <div className="flex p-3 bg-custom-blue mb-5 rounded ">
        <span className="uppercase text-2xl font-bold text-white">
          USER REGISTRATION
        </span>
      </div>
      <Form form={form} layout="vertical" onFinish={handleFinishForm}>
        <Form.Item name="lastname" label="Last name">
          <Input placeholder="Last name" allowClear prefix={<BiUser />} />
        </Form.Item>

        <Form.Item
          name="firstname"
          label="First name"
          required
          rules={[
            {
              required: true,
              message: "First name is required",
            },
          ]}
        >
          <Input placeholder="First name" allowClear prefix={<BiUser />} />
        </Form.Item>

        <Form.Item name="middlename" label="Middle name">
          <Input placeholder="Middle name" allowClear prefix={<BiUser />} />
        </Form.Item>

        <Form.Item
          name="username"
          label="Username"
          required
          rules={[
            {
              required: true,
              message: "Username is required",
            },
          ]}
        >
          <Input placeholder="Username" allowClear prefix={<BiUser />} />
        </Form.Item>

        <Form.Item
          name="password"
          label="Password"
          required
          rules={[
            {
              required: true,
              message: "Password is required",
            },
          ]}
        >
          <Input.Password prefix={<BiLockAlt />} />
        </Form.Item>

        <Form.Item
          name="permission"
          label="Permission"
          required
          rules={[
            {
              required: true,
              message: "Permission is required",
            },
          ]}
        >
          <Select>
            <Select.Option value="admin" key="admin">
              ADMIN
            </Select.Option>
            <Select.Option value="cpg-admin" key="cpg-admin">
              CPG-ADMIN
            </Select.Option>
            <Select.Option value="clerk" key="clerk">
              CLERK
            </Select.Option>
            <Select.Option value="laboratory-user" key="laboratory-user">
              LABORATORY-USER
            </Select.Option>
          </Select>
        </Form.Item>

        <Form.Item
          name="municipality"
          label="Hospital"
          required
          rules={[
            {
              required: true,
              message: "municipality is required",
            },
          ]}
        >
          <Select
            showSearch
            optionFilterProp="children"
            onChange={value => console.log(`Selected: ${value}`)}
            filterOption={(input: any, option: any) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {data &&
              data.map((item: any, index: any) => (
                <Option value={item.hospitalName} key={item.hospitalName}>
                  {item.hospitalName}
                </Option>
              ))}
          </Select>
        </Form.Item>

        <div className="w-full">
          <Button
            disabled={insertUser.isLoading || loading}
            htmlType="submit"
            type="primary"
            className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
          >
            {insertUser.isLoading || loading ? (
              <>
                <Spin /> <span>Please wait..</span>
              </>
            ) : (
              "Submit"
            )}
          </Button>
        </div>
      </Form>
    </Card>
  );
};

export default Registeration;
