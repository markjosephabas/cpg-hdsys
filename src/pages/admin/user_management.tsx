import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Popconfirm,
  Dropdown,
  Menu,
  Modal,
  Form,
  Select,
  Spin,
  Card,
} from "antd";
import { NextPage } from "next";
import {
  SearchOutlined,
  EditOutlined,
  UnlockOutlined,
  DeleteOutlined,
} from "@ant-design/icons";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState } from "react";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import { BiUser, BiLockAlt } from "react-icons/bi";
import { hash } from "bcryptjs";

const UserManagement: NextPage = () => {
  const session = useSession();
  const hasPermission = session.data?.user?.permission === "admin";

  const {
    data,
    isLoading: loading,
    refetch,
  } = trpc.useQuery(["taxdec.getUser"]);

  const {
    data: data2,
    isLoading: loading2,
    refetch: refetch2,
  } = trpc.useQuery(["taxdec.getHospital"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const menu = (item: any) => (
    <Menu>
      <Menu.Item
        key="1"
        icon={<EditOutlined />}
        onClick={() => handleShowModal(item)}
      >
        Edit Info
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<UnlockOutlined />}
        onClick={() => handleShowModalPass(item)}
      >
        Change Password
      </Menu.Item>
      <Menu.Item key="3" icon={<DeleteOutlined />} className="text-red-500">
        <Popconfirm
          key={2}
          title={`Are you sure to delete this user ${item.username}?`}
          onConfirm={() => handleDelete(item.id)}
          okText="Yes"
          cancelText="No"
        >
          Delete
        </Popconfirm>
      </Menu.Item>
    </Menu>
  );

  //   MODAL
  const [visible, setVisible] = useState(false);
  const [visiblePass, setvisiblePass] = useState(false);

  const handleShowModal = (item: any) => {
    setVisible(true);
    // console.log(item)
    form.setFieldsValue(item);
  };

  const handleHideModal = () => {
    setVisible(false);
  };

  const handleShowModalPass = (item: any) => {
    setvisiblePass(true);
    // console.log(item)
    form2.setFieldsValue(item);
    form2.setFieldsValue({
      newPassword: "",
    });
  };

  const handleHidePass = () => {
    setvisiblePass(false);
  };

  const { Option } = Select;

  // const [municipalities] = useState([
  //   "Cebu Province",
  //   "District Hospital - Danao",
  //   "District Hospital - Bogo",
  //   "District Hospital - Camotes",
  //   "District Hospital - Balamban",
  // ]);

  const [form] = Form.useForm();
  const [form2] = Form.useForm();
  const [submitted, setSubmitted] = useState<boolean>(false);
  const [updateLoading, setupdateLoading] = useState<boolean>(false);
  const [changePassLoading, setchangePassLoading] = useState<boolean>(false);

  const updateUser = trpc.useMutation("taxdec.updateuser", {
    onSuccess() {
      message.success("Updated successfully!");
      form.resetFields();
      setSubmitted(true);
      refetch();
    },
    onError(error) {
      message.error("Something went wrong!");
    },
  });

  const handleEditInfo = async (values: any) => {
    // console.log("values",values)
    // const newPass = await hash(values.password, 10);
    setupdateLoading(true);
    updateUser.mutate({
      ...values,
    });
    setupdateLoading(false);

    console.log(values);
  };
  const changePassword = trpc.useMutation("taxdec.changePassword", {
    onSuccess() {
      message.success("changed password successfully!");
      form2.resetFields();
      setSubmitted(true);
      refetch();
    },
    onError(error) {
      message.error("Something went wrong!");
    },
  });
  const handleChangePass = async (values: any) => {
    // console.log("values",values)
    const newPass = await hash(values.newPassword, 10);
    setchangePassLoading(true);
    changePassword.mutate({
      ...values,
      newPass: newPass,
    });
    setchangePassLoading(false);

    // console.log(newPass)
  };

  const deleteUser = trpc.useMutation("taxdec.deleteUser", {
    onSuccess() {
      message.success("User deleted successfully!");
      refetch();
    },
    onError(error) {
      message.error("Something went wrong!");
    },
  });
  const handleDelete = (id: number) => {
    deleteUser.mutate({
      id: id,
    });
    // console.log(id)
  };

  if (!hasPermission) {
    return (
      <div
        style={{
          textAlign: "center",
          padding: "20px",
          border: "1px solid #ff4d4f",
          borderRadius: "5px",
          backgroundColor: "#fff1f0",
          color: "#ff4d4f",
        }}
      >
        <p
          style={{ marginBottom: "10px", fontSize: "18px", fontWeight: "bold" }}
        >
          Access Denied
        </p>
        <p>{"You don't have the required permission to access this page."}</p>
      </div>
    );
  }

  return (
    <Card>
      <div>
        <div className="flex p-3 bg-custom-blue mb-5 rounded ">
          <span className="uppercase text-2xl font-bold text-white">
            USER MANAGEMENT
          </span>
        </div>
        <Table
          columns={[
            {
              title: "Username",
              dataIndex: "username",
              key: "username",
              align: "center",
            },
            {
              title: "Last Name",
              dataIndex: "lastname",
              key: "lastname",
              align: "center",
            },
            {
              title: "First Name",
              dataIndex: "firstname",
              key: "firstname",
              align: "center",
              filterDropdown: ({
                setSelectedKeys,
                selectedKeys,
                confirm,
                clearFilters,
              }: CustomFilterDropdownProps) => (
                <div style={{ padding: 8 }}>
                  <Input
                    placeholder="Search name"
                    value={selectedKeys[0]}
                    onChange={e =>
                      setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() => confirm()}
                    style={{ marginBottom: 8, display: "block" }}
                  />
                  <div style={{ display: "flex", justifyContent: "flex-end" }}>
                    <Button
                      onClick={() => {
                        clearFilters?.();
                        confirm();
                      }}
                      style={{ marginRight: 8 }}
                      size="small"
                    >
                      Reset
                    </Button>
                    <Button onClick={() => confirm()} size="small">
                      Filter
                    </Button>
                  </div>
                </div>
              ),
              filterIcon: () => (
                <Tooltip title="Search by owner name">
                  <SearchOutlined />
                </Tooltip>
              ),
              onFilter: (value: any, record: any) => {
                if (!value || value.length < 2) {
                  return true;
                }
                return record.firstname
                  .toLowerCase()
                  .includes(value.toLowerCase());
              },
              // render: (text: string) => {text && text.toUpperCase()}
            },
            {
              title: "Middle Name",
              dataIndex: "middlename",
              key: "middlename",
              align: "center",
            },
            {
              title: "Hospital ",
              dataIndex: "municipality",
              key: "municipality",
              align: "center",
              filterDropdown: ({
                setSelectedKeys,
                selectedKeys,
                confirm,
                clearFilters,
              }: CustomFilterDropdownProps) => (
                <div style={{ padding: 8 }}>
                  <Input
                    placeholder="Search Hospital"
                    value={selectedKeys[0]}
                    onChange={e =>
                      setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() => confirm()}
                    style={{ marginBottom: 8, display: "block" }}
                  />
                  <div style={{ display: "flex", justifyContent: "flex-end" }}>
                    <Button
                      onClick={() => {
                        clearFilters?.();
                        confirm();
                      }}
                      style={{ marginRight: 8 }}
                      size="small"
                    >
                      Reset
                    </Button>
                    <Button onClick={() => confirm()} size="small">
                      Filter
                    </Button>
                  </div>
                </div>
              ),
              filterIcon: () => (
                <Tooltip title="Search Hospital">
                  <SearchOutlined />
                </Tooltip>
              ),
              onFilter: (value: any, record: any) => {
                if (!value || value.length < 2) {
                  return true;
                }
                return record.municipality
                  .toLowerCase()
                  .includes(value.toLowerCase());
              },
            },
            {
              title: "Permission ",
              dataIndex: "permission",
              key: "permission",
              align: "center",
            },
            {
              title: "Actions",
              align: "center",
              key: "id",
              fixed: "right",
              render: (item: any) => (
                <Dropdown overlay={menu(item)}>
                  <a
                    className="ant-dropdown-link"
                    type="primary"
                    onClick={e => e.preventDefault()}
                  >
                    <Button icon={<EditOutlined />}> Action</Button>
                  </a>
                </Dropdown>
              ),
            },
          ]}
          size="middle"
          bordered
          dataSource={
            data &&
            data.map((item, index) => ({
              ...item,
              key: index,
            }))
          }
          scroll={{ x: "max-content" }}
          loading={loading}
          pagination={{
            pageSize: 10,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} of ${total} items`,
          }}
        />
      </div>

      {/* Modal */}
      <Modal
        title="User Update"
        open={visible}
        onCancel={handleHideModal}
        footer={null}
      >
        <Form
          form={form}
          layout="vertical"
          onFinish={handleEditInfo}
          className="w-full"
          autoComplete="off"
        >
          <div className="flex flex-wrap mb-4">
            <div
              className="w-full md:w-1/2 md:pl-2"
              style={{ display: "none" }}
            >
              <Form.Item name="id" label="User ID">
                <Input placeholder="User ID" allowClear prefix={<BiUser />} />
              </Form.Item>
            </div>
            <div className="w-full md:w-1/2 md:pr-2">
              <Form.Item name="lastname" label="Last name">
                <Input placeholder="Last name" allowClear prefix={<BiUser />} />
              </Form.Item>
            </div>
            <div className="w-full md:w-1/2 md:pl-2">
              <Form.Item
                name="firstname"
                label="First name"
                required
                rules={[
                  {
                    required: true,
                    message: "First name is required",
                  },
                ]}
              >
                <Input
                  placeholder="First name"
                  allowClear
                  prefix={<BiUser />}
                />
              </Form.Item>
            </div>
            <div className="w-full">
              <Form.Item name="middlename" label="Middle name">
                <Input
                  placeholder="Middle name"
                  allowClear
                  prefix={<BiUser />}
                />
              </Form.Item>
            </div>
            <div className="w-full md:w-1/2 md:pr-2">
              <Form.Item
                name="permission"
                label="Permission"
                required
                rules={[
                  {
                    required: true,
                    message: "Permission is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="admin" key="admin">
                    ADMIN
                  </Select.Option>
                  <Select.Option value="USER" key="USER">
                    USER
                  </Select.Option>
                  <Select.Option value="CASHIER" key="CASHIER">
                    CASHIER
                  </Select.Option>
                  <Select.Option value="ENTRANCE" key="ENTRANCE">
                    ENTRANCE
                  </Select.Option>
                  <Select.Option value="EXIT" key="EXIT">
                    EXIT
                  </Select.Option>
                </Select>
              </Form.Item>
            </div>
            <div className="w-full md:w-1/2 md:pl-2">
              <Form.Item
                name="municipality"
                label="Municipality"
                required
                rules={[
                  {
                    required: true,
                    message: "municipality is required",
                  },
                ]}
              >
                <Select
                  showSearch
                  optionFilterProp="children"
                  onChange={value => console.log(`Selected: ${value}`)}
                  filterOption={(input: any, option: any) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {data2 &&
                    data2.map((item: any, index: any) => (
                      <Option value={item.hospitalName} key={item.hospitalName}>
                        {item.hospitalName}
                      </Option>
                    ))}
                </Select>
              </Form.Item>
            </div>
            <div className="w-full">
              <Button
                disabled={updateUser.isLoading || updateLoading}
                htmlType="submit"
                type="primary"
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {updateUser.isLoading || updateLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
              </Button>
            </div>
          </div>
        </Form>
      </Modal>

      {/* Modal Change Pass  */}
      <Modal
        title="Change Password"
        open={visiblePass}
        onCancel={handleHidePass}
        footer={null}
      >
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleChangePass}
          className="w-full"
        >
          <div className="flex flex-wrap mb-4">
            <div
              className="w-full md:w-1/2 md:pl-2"
              style={{ display: "none" }}
            >
              <Form.Item name="id" label="User ID">
                <Input placeholder="User ID" allowClear prefix={<BiUser />} />
              </Form.Item>
            </div>
            <div className="w-full">
              <Form.Item name="username" label="Username">
                <Input placeholder="Username" prefix={<BiUser />} readOnly />
              </Form.Item>
            </div>
            <div className="w-full">
              <Form.Item
                name="newPassword"
                label="Password"
                required
                rules={[
                  {
                    required: true,
                    message: "Password is required",
                  },
                ]}
              >
                <Input.Password prefix={<BiLockAlt />} />
              </Form.Item>
            </div>
            <div className="w-full">
              <Button
                disabled={updateUser.isLoading || changePassLoading}
                htmlType="submit"
                type="primary"
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {updateUser.isLoading || changePassLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
              </Button>
            </div>
          </div>
        </Form>
      </Modal>
    </Card>
  );
};

export default UserManagement;
