// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-nocheck
import NextAuth, { type NextAuthOptions } from "next-auth";
// import GoogleProvider from "next-auth/providers/google";
import CredentialsProvider from "next-auth/providers/credentials";
import bcrypt from "bcrypt";

// Prisma adapter for NextAuth, optional and can be removed
import { PrismaAdapter } from "@next-auth/prisma-adapter";
import { prisma } from "../../../server/db/client";
import { env } from "../../../env/server.mjs";

export const authOptions: NextAuthOptions = {
  // Configure one or more authentication providers
  adapter: PrismaAdapter(prisma),
  pages: {
    signIn: "/auth/credentials-signin",
  },
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {},
      async authorize(credentials) {
        const { username, password } = credentials;

        const user = await prisma.system_users.findFirst({
          where: {
            username,
          },
        });

        if (!user) return null;

        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch) return null;

        return user;
      },
    }),
    // GoogleProvider({
    //   clientId: env.GOOGLE_CLIENT_ID,
    //   clientSecret: env.GOOGLE_CLIENT_SECRET,
    // }),
  ],
  callbacks: {
    async jwt(param) {
      if (param.user) {
        param.token.id = param.user.id;
        param.token.username = param.user.username;
        param.token.permission = param.user.permission;
        param.token.municipality = param.user.municipality;
        param.token.firstname = param.user.firstname;
        param.token.lastname = param.user.lastname;
      }
      return param.token;
    },
    async session({ session, token, param }) {
      if (token) {
        session.user.id = token.id;
        session.user.username = token.username
        session.user.permission = token.permission;
        session.user.municipality = token.municipality;
        session.user.firstname = token.firstname;
        session.user.lastname = token.lastname;
        
      }
      return session;
    },
  },
  session: {
    strategy: "jwt",
  },
  secret: env.JWT_SECRET,
};

export default NextAuth(authOptions);
