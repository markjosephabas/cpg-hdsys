import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  Card,
  Alert,
  Space,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import { SearchOutlined, PlusOutlined, FormOutlined } from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality;
  const permission = session.data?.user?.permission;
  const queryKey =
    permission === "admin" ? "taxdec.getBenefiaryAll" : "taxdec.getBenefiary";

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(permission === "admin" ? [queryKey] : [queryKey, hospital]);

  const {
    data: data2,
    isLoading: loading2,
    refetch: refetch2,
  } = trpc.useQuery(["taxdec.getHospital"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);

  const updateBeneficiary = trpc.useMutation("taxdec.updateBeneficiary", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const register_beneficiary = trpc.useMutation("taxdec.register_beneficiary", {
    onSuccess(data) {
      refetch();
      form.resetFields();
      message.success("Added susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleUpdate2 = async (values: any) => {
    try {
      const hospital = session.data?.user?.municipality;

      register_beneficiary.mutate({
        ...values,
        hospital: hospital,
      });

      console.log(register_beneficiary.isSuccess);
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const handleUpdate = async (values: any) => {
    try {
      const hospital = session.data?.user?.municipality;

      updateBeneficiary.mutate({
        ...values,
        hospital: hospital,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  // FOR TABLE
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const handleChange = (pagination: any, filters: any, sorter: any) => {
    setCurrentPage(pagination.current);
    if (pagination.current !== currentPage) {
      setCurrentPage(pagination.current);
    }

    if (pagination.pageSize !== pageSize) {
      setCurrentPage(1); // Reset to the first page when changing page size
      setPageSize(pagination.pageSize);
    }
  };
  const paginationConfig = {
    current: currentPage,
    pageSize: pageSize, // Adjust as needed
    showTotal: (total: any, range: any) =>
      `${range[0]}-${range[1]} of ${total} items`,
  };
  //TABLE

  return (
    <div>
      <Card className="fade-in">
        <div>
          <div className="flex p-3 bg-custom-blue mb-5 rounded ">
            <span className="uppercase text-2xl font-bold text-white">
              BENEFICIARIES
            </span>
            <Button
              className="ml-auto text-custom-blue bg-white border-custom-blue flex items-center  rounded-full"
              icon={<PlusOutlined />}
              onClick={() => {
                setopenUpdateItem2(true);
                setMessage("");
              }}
            >
              ADD{" "}
            </Button>
          </div>

          <Table
            columns={[
              {
                title: "ID",
                dataIndex: "id",
                key: "id",
                align: "center",
                render: (text, record, index) =>
                  (currentPage - 1) * pageSize + index + 1,
              },
              {
                title: "Full Name",
                dataIndex: "firstName",
                key: "firstName",
                filterDropdown: ({
                  setSelectedKeys,
                  selectedKeys,
                  confirm,
                  clearFilters,
                }: CustomFilterDropdownProps) => (
                  <div style={{ padding: 8 }}>
                    <Input
                      placeholder="Search plate number"
                      value={selectedKeys[0]}
                      onChange={e =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                      }
                      onPressEnter={() => confirm()}
                      style={{ marginBottom: 8, display: "block" }}
                    />
                    <div
                      style={{ display: "flex", justifyContent: "flex-end" }}
                    >
                      <Button
                        onClick={() => {
                          clearFilters?.();
                          confirm();
                        }}
                        style={{ marginRight: 8 }}
                        size="small"
                      >
                        Reset
                      </Button>
                      <Button onClick={() => confirm()} size="small">
                        Filter
                      </Button>
                    </div>
                  </div>
                ),
                filterIcon: () => (
                  <Tooltip title="Search by owner name">
                    <SearchOutlined />
                  </Tooltip>
                ),
                onFilter: (value: any, record: any) =>
                  !value ||
                  value.length < 2 ||
                  `${record.firstName} ${record.middleName || ""} ${
                    record.lastName
                  }`
                    .toLowerCase()
                    .includes(value.toLowerCase()),

                render: (text: string, record: any) =>
                  `${record.lastName}, ${record.firstName} ${
                    record.middleName || ""
                  }`.toUpperCase(),
              },
              {
                title: "Address",
                dataIndex: "address",
                key: "address",
                align: "center",
                render: (text: string, record: any) =>
                  record.address.toUpperCase(),
              },
              {
                title: "Class",
                dataIndex: "class",
                key: "class",
                align: "center",
                filters: [
                  {
                    text: "Indigent",
                    value: "Indigent",
                  },
                  {
                    text: "Paying",
                    value: "Paying",
                  },
                  {
                    text: "POS",
                    value: "POS",
                  },
                  {
                    text: "PWD",
                    value: "PWD",
                  },
                ],
                onFilter: (value, record) => record.class.includes(value),
                ellipsis: true,
                render: (text: any, record: any) =>
                  record.status === null ? (
                    ""
                  ) : record.class === "Indigent" ? (
                    <Tag color="green">INDIGENT</Tag>
                  ) : record.class === "Paying" ? (
                    <Tag color="blue">PAYING</Tag>
                  ) : record.class === "POS" ? (
                    <Tag color="purple">POS</Tag>
                  ) : record.class === "PWD" ? (
                    <Tag color="cyan">PWD</Tag>
                  ) : (
                    ""
                  ),
              },
              {
                title: "Hospital",
                dataIndex: "hospital",
                key: "hospital",
                align: "center",
                filters:
                  data2 &&
                  data2.map((item, index) => ({
                    text: item.hospitalName,
                    value: item.hospitalName,
                  })),
                onFilter: (value, record) => record.hospital.includes(value),
                ellipsis: true,
                render: (text: string, record: any) =>
                  record.hospital.toUpperCase(),
              },
              {
                title: "Status",
                dataIndex: "status",
                key: "status",
                align: "center",
                filters: [
                  {
                    text: "Active",
                    value: "Active",
                  },
                  {
                    text: "Inactive",
                    value: "Inactive",
                  },
                  {
                    text: "Expired",
                    value: "Expired",
                  },
                ],
                onFilter: (value, record) => record.status.includes(value),
                ellipsis: true,
                render: (text: any, record: any) =>
                  record.status === null ? (
                    ""
                  ) : record.status === "Active" ? (
                    <Tag color="green">ACTIVE</Tag>
                  ) : record.status === "Inactive" ? (
                    <Tag color="volcano">INACTIVE</Tag>
                  ) : record.status === "Expired" ? (
                    <Tag color="magenta">EXPIRED</Tag>
                  ) : (
                    ""
                  ),
              },

              {
                title: "Actions",
                key: "id",
                align: "center",
                render: (item: any) => {
                  return (
                    <div>
                      {/* <Tooltip title="Cancel">
                        <Button
                          onClick={() => handleEdit(item)}
                          icon={<DeleteOutlined />}
                          shape="circle"
                          danger
                          type="primary"
                        />
                      </Tooltip> */}
                      <Tooltip title="Edit">
                        <Button
                          onClick={() => handleEdit(item)}
                          className="bg-blue-600 text-white ml-1"
                          icon={<FormOutlined />}
                        ></Button>
                      </Tooltip>
                    </div>
                  );
                },
              },
            ]}
            size="middle"
            bordered
            dataSource={
              data &&
              data.map((item: any, index: any) => ({
                ...item,
                key: index,
              }))
            }
            onChange={handleChange}
            loading={loading}
            pagination={paginationConfig}
            scroll={{ x: true }}
            // responsive
          />
        </div>
      </Card>
      {/* UPDATE BENEFICIARY     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> UPDATE BENEFICARY
            </Space>
          </>
        }
        open={openUpdateItem}
        onCancel={() => setopenUpdateItem(false)}
        footer={null}
      >
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleUpdate}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="id"
                label="ID"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="firstName"
                label="First Name"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="middleName"
                label="Middle Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Middle Name is required",
                  },
                ]}
              >
                <Input placeholder="Middle Name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="lastName"
                label="Last Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Last Name is required",
                  },
                ]}
              >
                <Input placeholder="Last Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="address"
                label="Address"
                required
                rules={[
                  {
                    required: true,
                    message: "Address is required",
                  },
                ]}
              >
                <Input placeholder="Address" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="gender"
                label="Gender"
                required
                rules={[
                  {
                    required: true,
                    message: "Gender is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="Male" key="Male">
                    Male
                  </Select.Option>
                  <Select.Option value="Female" key="Female">
                    Female
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="class"
                label="Class"
                required
                rules={[
                  {
                    required: true,
                    message: "Class is required",
                  },
                ]}
              >
                <Select disabled>
                  <Select.Option value="Paying" key="Paying">
                    Paying
                  </Select.Option>
                  <Select.Option value="Indigent" key="Indigent">
                    Indigent
                  </Select.Option>
                  <Select.Option value="POS" key="POS">
                    POS
                  </Select.Option>
                  <Select.Option value="PWD" key="PWD">
                    PWD
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="status"
                label="Status"
                required
                rules={[
                  {
                    required: true,
                    message: "Gender is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="Active" key="Active">
                    Active
                  </Select.Option>
                  <Select.Option value="Inactive" key="Inactive">
                    Inactive
                  </Select.Option>
                  <Select.Option value="Expired" key="Expired">
                    Expired
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            {/* <Col span={24}>
              <Form.Item name="bus_name" label="BUS NAME">
                <Input placeholder="Enter bus name" />
              </Form.Item>
            </Col> */}
            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={register_beneficiary.isLoading}
                htmlType="submit"
                type="primary"
                loading={register_beneficiary.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {register_beneficiary.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>

      {/* ADD BENEFICARY     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> ADD BENEFICARY
            </Space>
          </>
        }
        open={openUpdateItem2}
        onCancel={() => setopenUpdateItem2(false)}
        footer={null}
        width={400}
      >
        <Form
          form={form}
          layout="vertical"
          onFinish={handleUpdate2}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24}>
              <Form.Item
                name="firstName"
                label="First Name"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="middleName"
                label="Middle Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Middle Name is required",
                  },
                ]}
              >
                <Input placeholder="Middle Name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="lastName"
                label="Last Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Last Name is required",
                  },
                ]}
              >
                <Input placeholder="Last Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="address"
                label="Address"
                required
                rules={[
                  {
                    required: true,
                    message: "Address is required",
                  },
                ]}
              >
                <Input placeholder="Address" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="gender"
                label="Gender"
                required
                rules={[
                  {
                    required: true,
                    message: "Gender is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="Male" key="Male">
                    Male
                  </Select.Option>
                  <Select.Option value="Female" key="Female">
                    Female
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="class"
                label="Class"
                required
                rules={[
                  {
                    required: true,
                    message: "Class is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="Paying" key="Paying">
                    Paying
                  </Select.Option>
                  <Select.Option value="Indigent" key="Indigent">
                    Indigent
                  </Select.Option>
                  <Select.Option value="POS" key="POS">
                    POS
                  </Select.Option>
                  <Select.Option value="PWD" key="PWD">
                    PWD
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            {/* <Col span={24}>
              <Form.Item name="bus_name" label="BUS NAME">
                <Input placeholder="Enter bus name" />
              </Form.Item>
            </Col> */}
            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={register_beneficiary.isLoading}
                htmlType="submit"
                type="primary"
                loading={register_beneficiary.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {register_beneficiary.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "SUBMIT"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};

export default PendingTaxDeclaration;
