import { NextPage } from "next";
import { useSession, signOut } from "next-auth/react";
import { trpc } from "../../utils/trpc";
import { useRouter } from "next/router";
import { PrinterOutlined, SaveOutlined } from "@ant-design/icons";
import {
  Button,
  DatePicker,
  Form,
  Card,
  Spin,
  Space,
  message,
  Select,
  Row,
  Col,
} from "antd";
import React, { useState, useEffect } from "react";
import moment, { Moment } from "moment";
import * as XLSX from "xlsx";

const Dashboard: NextPage = () => {
  const session = useSession();
  const route = useRouter();
  const [form] = Form.useForm();
  const formData = form.getFieldsValue();
  const [dateRange, setDateRange] = useState<
    [moment.Moment | null, moment.Moment | null]
  >([null, null]);
  const [dateRange2, setDateRange2] = useState<
    [moment.Moment | null, moment.Moment | null]
  >([null, null]);
  const handleDateRangeChange = (dates: any) => {
    setDateRange(dates);
  };
  const [classPatient, setclassPatient] = useState("");

  const handleCheck = () => {
    dateRange ? setDateRange2(dateRange) : setDateRange2([null, null]);
    setclassPatient(formData.class);
    console.log(classPatient);
    // console.log(dateRange);
  };
  const handleFinishForm = async (values: any) => {
    dateRange ? setDateRange2(dateRange) : setDateRange2([null, null]);
    setclassPatient(values.class);
    // insertRemmitance.mutate({
    //   date_from: startDate2,
    //   date_to: endDate2,
    //   municipality: office,
    // });
    // console.log("values:", values);
  };

  return (
    <div className="text-2xl font-bold mb-10 text-black-200">
      {/* <div className="text-4xl font-bold text-gray-900 ">ADMIN</div> */}
      <div className="text-xl font-medium text-gray-600 mb-8">STATEMENT</div>
      <Space>
        <Form
          form={form}
          onFinish={handleFinishForm}
          layout="vertical"
          className="w-full"
        >
          <Row gutter={12}>
            <Col>
              <Form.Item
                name="Date"
                label="SELECT DATES"
                required
                rules={[
                  {
                    required: true,
                    message: "Date is required",
                  },
                ]}
              >
                <DatePicker.RangePicker
                  value={dateRange}
                  format="YYYY-MM-DD"
                  onChange={handleDateRangeChange}
                />
              </Form.Item>
            </Col>
            <Col>
              <Form.Item
                name="class"
                label="Class"
                required
                rules={[
                  {
                    required: true,
                    message: "Class is required",
                  },
                ]}
                initialValue="Beneficiary (Indigent/POS/PWD)"
              >
                <Select style={{ width: "300px" }}>
                  <Select.Option value="Paying" key="Paying">
                    Paying
                  </Select.Option>
                  <Select.Option
                    value="Beneficiary (Indigent/POS/PWD)"
                    key="Beneficiary (Indigent/POS/PWD)"
                  >
                    Beneficiary (Indigent/POS/PWD)
                  </Select.Option>
                  {/* <Select.Option value="POS" key="POS">
                    POS
                  </Select.Option>
                  <Select.Option value="PWD" key="PWD">
                    PWD
                  </Select.Option> */}
                </Select>
              </Form.Item>
            </Col>
            <Col>
              <Form.Item name="" label="Action">
                <Button
                  htmlType="submit"
                  // onClick={handleCheck}
                  className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
                >
                  SUBMIT
                </Button>
              </Form.Item>
            </Col>
            {/* <Form.Item name="">
                <Button
                  onClick={handleCheck}
                  className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
                >
                  DOWNLOAD
                </Button>
              </Form.Item> */}

            <div className="w-full">
              {dateRange2.includes(null) ? null : (
                <Details data={dateRange2} class={classPatient} />
              )}
            </div>
          </Row>
        </Form>
      </Space>
    </div>
  );
};
const Details = (props: any) => {
  // props.data.includes(null) ? console.log('0'): console.log('1')
  const session = useSession();
  const startDate2 = props.data[0]
    ? moment(props.data[0]).format("YYYY-MM-DD")
    : "";
  const endDate2 = props.data[1]
    ? moment(props.data[1]).format("YYYY-MM-DD")
    : "";
  const route = useRouter();
  const hospital = session.data?.user?.municipality;
  const classPatient = props?.class;
  const {
    data,
    isLoading: loading,
    refetch,
  } = trpc.useQuery([
    "taxdec.getStatement",
    {
      startDate: startDate2,
      endDate: endDate2,
      hospital: hospital ? hospital : "Cebu Province",
      class: classPatient,
    },
  ]);

  // console.log("data:", data);
  //DOWNLOAD
  const printDiv = () => {
    const divToPrint = document?.getElementById("printMe")?.innerHTML;
    const printWindow = window.open("", "_blank", "height=800,width=1200");
    printWindow?.document.write(
      "<html><head><title>Print</title><style>table {font-size:12px;border-collapse:collapse;} p{margin:0} #deleteBtn {display:none}</style></head><body>"
    );
    printWindow?.document.write(divToPrint ? divToPrint : "");
    printWindow?.document.write("</body></html>");
    printWindow?.document.close();
    printWindow?.print();
  };
  const beneficiaries = data ?? [];
  const totalOfAllTotalQuantity = beneficiaries.reduce(
    (total, beneficiary) => total + beneficiary.sessionCount.beneficiary_id,
    0
  );

  const insertStatement = trpc.useMutation("taxdec.insertStatement", {
    onSuccess(data) {
      refetch();

      route.push(`/dashboard/statement_list`);
      message.success("Added susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const saveStatement = () => {
    const dataArray = data ?? [];
    console.log("class", classPatient);
    // Map each item in dataArray to the expected structure
    const formattedDataArray = dataArray.map(item => ({
      hospital: item.hospital ?? "",
      firstName: item.firstName ?? "",
      lastName: item.lastName ?? "",
      middleName: item.middleName ?? "", // Ensure middleName is not null
      unitPrice: 1850,
      totalAmount: 1850 * item.sessionCount.beneficiary_id,
      totalQuantity: item.sessionCount.beneficiary_id,
      // Add other fields as needed
    }));

    insertStatement.mutate({
      data: formattedDataArray,
      startDate: startDate2,
      endDate: endDate2,
      class: classPatient,
    });
  };
  return (
    <div className="py-4">
      <Card bordered={true}>
        <Space>
          <Button
            className="bg-blue-600 text-white"
            onClick={saveStatement}
            icon={<SaveOutlined />}
          >
            SAVE
          </Button>
        </Space>
        <div id="printMe">
          <table
            style={{
              width: "100%",
              marginTop: "5px",
              border: "1px solid black",
            }}
          >
            <thead>
              <tr>
                <td
                  colSpan={5}
                  style={{
                    border: "1px solid black",
                    padding: "8px",
                    textAlign: "center",
                  }}
                >
                  STATEMENT<br></br>
                  FROM {moment(startDate2).format("ll")} TO{" "}
                  {moment(endDate2).format("ll")}
                </td>
              </tr>
              <tr>
                <td
                  style={{
                    border: "1px solid black",
                    padding: "8px",
                    textAlign: "center",
                    width: "5%",
                  }}
                >
                  #
                </td>
                <td
                  style={{
                    border: "1px solid black",
                    padding: "8px",
                    textAlign: "center",
                  }}
                >
                  Name
                </td>
                <td
                  style={{
                    border: "1px solid black",
                    padding: "8px",
                    textAlign: "center",
                  }}
                >
                  Used
                </td>
                <td
                  style={{
                    border: "1px solid black",
                    padding: "8px",
                    textAlign: "center",
                  }}
                >
                  Unit Price
                </td>
                <td
                  style={{
                    border: "1px solid black",
                    padding: "8px",
                    textAlign: "center",
                  }}
                >
                  Amount
                </td>
              </tr>
            </thead>
            <tbody>
              {data?.map((item: any, index) => (
                <tr key={index}>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    {index + 1}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "left",
                      width: "40%",
                    }}
                  >
                    {item.lastName.toUpperCase()},{" "}
                    {item.firstName.toUpperCase()}{" "}
                    {item.middleName ? item.middleName.toUpperCase() : ""}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    {item.sessionCount.beneficiary_id}
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱ 1,850.00
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {(1850 * item.sessionCount.beneficiary_id).toLocaleString(
                      undefined,
                      {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      }
                    )}
                  </td>
                </tr>
              ))}
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td
                  style={{
                    border: "1px solid black",
                    padding: "8px",
                    textAlign: "center",
                  }}
                >
                  TOTAL
                </td>
                <td
                  style={{
                    border: "1px solid black",
                    padding: "8px",
                    textAlign: "center",
                  }}
                >
                  ₱{" "}
                  {(totalOfAllTotalQuantity * 1850).toLocaleString(undefined, {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })}
                </td>
              </tr>
            </tbody>
            <tfoot></tfoot>
          </table>
        </div>
      </Card>
    </div>
  );
};
export default Dashboard;
