import { NextPage } from "next";
import { useSession, signOut } from "next-auth/react";
import { trpc } from "../../utils/trpc";
import { useRouter } from "next/router";
import { Row, Col, Card, Statistic, Typography } from "antd";
import React, { useState, useEffect } from "react";

const Dashboard: NextPage = () => {
  const session = useSession();
  const route = useRouter();

  return (
    <Row>
      <Col span={24}>
        <h1 className="text-2xl md:text-3xl text-slate-800 dark:text-slate-100 font-bold mb-1">
          🌞 Good Day, {session?.data?.user?.municipality.toUpperCase()}. 👋
        </h1>
        <p style={{ fontSize: 16, color: "#666" }}>
          Thank you for using our application. We appreciate your hard work and
          dedication! 🎉
        </p>
      </Col>
      {/* <Col xs={24} sm={24} md={12} lg={8} xl={6}>
        <Card loading={loading}>
          <Statistic
            title="Today's Collection"
            value={data?._sum?.amount ?? 0}
            precision={2}
            prefix="₱"
          />
        </Card>
      </Col> */}
    </Row>
  );
};

export default Dashboard;
