import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  InputNumber,
  Card,
  Alert,
  Space,
  DatePicker,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import moment, { Moment } from "moment";
import { useRouter } from "next/router";
import {
  SearchOutlined,
  ExceptionOutlined,
  DeleteOutlined,
  CalculatorOutlined,
  UndoOutlined,
  CalendarOutlined,
  UserOutlined,
  HomeOutlined,
  TeamOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  // const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  // const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality || "CEBU PROVINCE";
  const permission = session.data?.user?.permission;
  const [balanceChecker, setbalanceChecker] = useState(false);

  // const {
  //   data: data2,
  //   isLoading: loading2,
  //   refetch: refetch2,
  // } = trpc.useQuery(["taxdec.getHospital"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();
  const [form3] = Form.useForm();
  const route = useRouter();
  const [openUpdateItem, setopenUpdateItem] = useState(false);
  // const [openDeduction, setopenDeduction] = useState(false);
  const [openChangeDate, setopenChangeDate] = useState(false);

  const updateItems = trpc.useMutation("taxdec.updateSessionDeduction", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    const total = getSum(item.sessions_itemUsed);
    const balance =
      total -
      item.philhealth_deduction -
      item.aics_deduction -
      item.maep_deduction -
      item.cash -
      item.qfs -
      item.pcso;
    form2.setFieldsValue({
      ...item,
      amount: getSum(item.sessions_itemUsed),
      balance: balance,
    });
    // console.log(form2.getFieldValue("philhealth_deduction"));
    console.log(form2.getFieldsValue());
    setopenUpdateItem(true);
  };

  const handleChangeDate = (item: any) => {
    const created_at = moment(item.created_at);
    const fullName =
      `${item.beneficiary.firstName} ${item.beneficiary.middleName} ${item.beneficiary.lastName}`.toUpperCase();
    const totalAmount = getSum(item.sessions_itemUsed).toLocaleString(
      undefined,
      {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }
    );
    form3.setFieldsValue({
      ...item,
      fullName: fullName,
      totalAmount: totalAmount,
      created_at: created_at,
    });
    console.log(created_at);
    setopenChangeDate(true);
  };

  const insertItems = trpc.useMutation("taxdec.insertItems", {
    onSuccess(data) {
      refetch();
      form.resetFields();
      message.success("Added susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleUpdate = async (values: any) => {
    try {
      updateItems.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };
  const getSum = (values: any) => {
    const sum = values.reduce((acc: any, curr: any) => {
      // Extract unitPrice and quantity from the current object
      const { unitPrice, quantity } = curr;

      // console.log(curr);
      // Convert unitPrice and quantity to numbers and add to the accumulator
      return acc + Number(unitPrice) * Number(quantity);
    }, 0); // Initial accumulator value is 0

    // Check if the sum is a valid number
    if (!isNaN(sum)) {
      // console.log(sum);
      // return sum.toLocaleString(undefined, {
      //   minimumFractionDigits: 2,
      //   maximumFractionDigits: 2,
      // });
      return sum;
    } else {
      console.error("Invalid values for unitPrice or quantity");
      return 0; // Or return any default value as needed
    }
  };
  const deleteTransaction = trpc.useMutation(
    "taxdec.deleteSessionTransaction",
    {
      onSuccess() {
        message.success("Deleted successfully!");
        refetch();
      },
      onError(error) {
        message.error(`${error.message}`);
      },
    }
  );
  const handleClickDelete = (item: any) => {
    deleteTransaction.mutate({
      id: item.id,
    });
  };
  function handleDelete(item: any) {
    Modal.confirm({
      title: `Are you sure you want to delete this transaction with ID number: ${item.id} ?`,
      onOk() {
        handleClickDelete(item);
      },
      onCancel() {
        // The user clicked Cancel
        // Do nothing or handle the cancel event here
      },
      okButtonProps: {
        className:
          "bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline",
      },
    });
  }

  const restoreTransaction = trpc.useMutation(
    "taxdec.restoreSessionTransaction",
    {
      onSuccess() {
        message.success("Restored successfully!");
        refetch();
      },
      onError(error) {
        message.error(`${error.message}`);
      },
    }
  );
  const handleClickRetrieve = (item: any) => {
    restoreTransaction.mutate({
      id: item.id,
    });
  };
  function handleRetrieve(item: any) {
    Modal.confirm({
      title: `Are you sure you want to restore this transaction with ID number: ${item.id} ?`,
      onOk() {
        handleClickRetrieve(item);
      },
      onCancel() {
        // The user clicked Cancel
        // Do nothing or handle the cancel event here
      },
      okButtonProps: {
        className:
          "bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline",
      },
    });
  }

  const [dateRange, setDateRange] = useState<
    [moment.Moment | null, moment.Moment | null]
  >([null, null]);

  const onChangeData = () => {
    const philhealth_deduction = form2.getFieldValue("philhealth_deduction");
    const aics_deduction = form2.getFieldValue("aics_deduction");
    const maep_deduction = form2.getFieldValue("maep_deduction");
    const cash = form2.getFieldValue("cash");
    const qfs = form2.getFieldValue("qfs");
    const pcso = form2.getFieldValue("pcso");
    const amount = form2.getFieldValue("amount");
    const balance =
      amount -
      philhealth_deduction -
      aics_deduction -
      maep_deduction -
      cash -
      pcso -
      qfs;
    form2.setFieldsValue({
      balance: balance.toFixed(2),
    });

    if (balance < 0) {
      setbalanceChecker(true);
    } else {
      setbalanceChecker(false);
    }
  };

  const updateDate = trpc.useMutation("taxdec.updateDate", {
    onSuccess() {
      message.success("Updated successfully!");
      setopenChangeDate(false);
      refetch();
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });
  const handleFinishForm2 = async (values: any) => {
    updateDate.mutate({
      id: values.id,
      created_at: values.created_at,
    });
    // console.log('clicked')
  };

  //START LIVE TABLE

  const [filters, setFilters] = useState({
    // status: null,
    startDate: null as string | null,
    endDate: null as string | null,
    name: "",
    class: "",
    hospital: "",
    // location: "",
    page: 1,
    pageSize: 10,
  });

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getSessionTransactionAdminLive", { ...filters }]);

  const handleTableChange = (pagination: any) => {
    setFilters({
      ...filters,
      page: pagination.current,
      pageSize: pagination.pageSize,
    });
    refetch();
  };
  const changeHospital = (value: any) => {
    const hospital = value.target.value;
    setFilters({
      ...filters,
      hospital: hospital,
      page: 1,
    });
    refetch();
    // console.log(filters);
    // console.log("Selected Hospital:", hospital);
  };
  const changeFullName = (value: any) => {
    const fullName = value.target.value;
    setFilters({
      ...filters,
      name: fullName,
      page: 1,
    });
    refetch();
    // console.log(filters);
    // console.log("Selected Hospital:", hospital);
  };
  const changeClass = (value: any) => {
    const class_name = value;
    setFilters({
      ...filters,
      class: class_name,
      page: 1,
    });
    refetch();
    // console.log(filters);
    // console.log("Selected class:", class_name);
  };
  type Transaction = {
    trans_id: number;
    plate_no: string;
    datetime_start: Date | null;
    datetime_end: Date | null;
    status: string;
    amount: number;
    location: string;
    vehicle_Type: {
      vehicle_decription: string;
      amount: number;
    };
  };

  type Data = {
    transaction: Transaction[];
    total: number;
  };

  const transactionData = data as Data | undefined;
  const dataSource =
    transactionData?.transaction?.map((item, index) => ({
      ...item,
      key: index,
    })) || [];

  const handleDateRangeChange = (
    dates: [Moment | null, Moment | null] | null,
    formatString: [string, string]
  ) => {
    if (dates) {
      setFilters({
        ...filters,
        startDate: dates[0] ? dates[0].format("YYYY-MM-DD") : "",
        endDate: dates[1] ? dates[1].format("YYYY-MM-DD") : null,
        page: 1,
      });
      refetch();
    } else {
      setFilters({
        ...filters,
        startDate: null,
        endDate: null,
        page: 1,
      });
      refetch();
    }
  };
  // const handleDateRangeChange = (dates: any) => {
  //   setFilters({
  //     ...filters,
  //     startDate: dates[0],
  //     endDate: dates[1],
  //   });
  //   refetch();
  //   // setDateRange(dates);
  //   console.log(dates ? moment(dates[0]) : null);
  //   console.log(dates[0]);
  //   // setSelectedKeys(dates);
  // };
  //END LIVE TABLE

  return (
    <div>
      <Row>
        <Col sm={24} md={24} lg={24} xs={24}>
          <Card className="fade-in">
            <div className="flex p-3 bg-custom-blue mb-5 rounded ">
              <span className="uppercase text-2xl font-bold text-white">
                TRANSACTIONS
              </span>
            </div>
            <span className="text-2xl font-bold">FILTER</span>
            <div>
              <Row gutter={12}>
                <Col md={6} xs={24}>
                  <Form layout="vertical">
                    <Form.Item
                      name="searchname"
                      label={
                        <>
                          <UserOutlined className="mr-1" />
                          Name
                        </>
                      }
                    >
                      <Input
                        placeholder="Search name..."
                        onChange={changeFullName}
                        autoComplete="off"
                        allowClear
                      />
                    </Form.Item>
                  </Form>
                </Col>
                {permission === "admin" && (
                  <Col md={6} xs={24}>
                    <Form layout="vertical">
                      <Form.Item
                        name="hospital"
                        label={
                          <>
                            <HomeOutlined className="mr-1" />
                            Hospital
                          </>
                        }
                      >
                        <Input
                          placeholder="Search hospital..."
                          onChange={changeHospital}
                          autoComplete="off"
                          allowClear
                          disabled={permission !== "admin"}
                        />
                      </Form.Item>
                    </Form>
                  </Col>
                )}
                <Col md={6} xs={24}>
                  <Form layout="vertical">
                    <Form.Item
                      name="class"
                      label={
                        <>
                          <TeamOutlined className="mr-1" />
                          Class
                        </>
                      }
                    >
                      <Select onChange={changeClass}>
                        <Select.Option value="">All Class</Select.Option>
                        <Select.Option value="Indigent">Indigent</Select.Option>
                        <Select.Option value="Paying">Paying</Select.Option>
                        <Select.Option value="POS">POS</Select.Option>
                        <Select.Option value="PWD">PWD</Select.Option>
                      </Select>
                    </Form.Item>
                  </Form>
                </Col>
                <Col md={6} xs={24}>
                  <Form layout="vertical">
                    <Form.Item
                      name="hospital"
                      label={
                        <>
                          <CalendarOutlined className="mr-1" />
                          Date Coverage
                        </>
                      }
                    >
                      <DatePicker.RangePicker
                        value={dateRange}
                        onChange={handleDateRangeChange}
                        format="YYYY-MM-DD"
                      />
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
              <Table
                columns={[
                  {
                    title: "#",
                    dataIndex: "id",
                    key: "id",
                    align: "center",
                    render: (text, record, index) =>
                      (filters.page - 1) * filters.pageSize + index + 1,
                  },
                  {
                    title: "Name",
                    dataIndex: "itemName",
                    key: "itemName",
                    render: (text: string, record: any) =>
                      `${record.beneficiary.firstName} ${record.beneficiary.middleName} ${record.beneficiary.lastName}`.toUpperCase(),
                  },
                  {
                    title: "Hospital",
                    dataIndex: "hospital",
                    key: "hospital",
                    align: "center",
                    render: (text: string, record: any) =>
                      record.hospital.toUpperCase(),
                  },
                  {
                    title: "Class",
                    dataIndex: "class",
                    key: "class",
                    align: "center",
                    render: (text: any, record: any) =>
                      record.status === null ? (
                        ""
                      ) : record.beneficiary.class === "Indigent" ? (
                        <Tag color="green">INDIGENT</Tag>
                      ) : record.beneficiary.class === "Paying" ? (
                        <Tag color="blue">PAYING</Tag>
                      ) : record.beneficiary.class === "POS" ? (
                        <Tag color="purple">POS</Tag>
                      ) : record.beneficiary.class === "PWD" ? (
                        <Tag color="cyan">PWD</Tag>
                      ) : (
                        ""
                      ),
                  },

                  {
                    title: "Amount",
                    dataIndex: "amount",
                    key: "amount",
                    align: "center",
                    render: (text: any, record: any) => {
                      return (
                        <b>
                          {getSum(record.sessions_itemUsed).toLocaleString(
                            undefined,
                            {
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2,
                            }
                          )}
                        </b>
                      );
                    },
                  },
                  // {
                  //   title: "balance",
                  //   dataIndex: "balance",
                  //   key: "balance",
                  //   align: "center",
                  //   render: (text: any, record: any) => {
                  //     const balance_amount =
                  //       getSum(record.sessions_itemUsed) -
                  //       record.aics_deduction -
                  //       record.maep_deduction -
                  //       record.philhealth_deduction -
                  //       record.cash -
                  //       record.qfs -
                  //       record.pcso;

                  //     if (balance_amount === 0) {
                  //       return (
                  //         <p style={{ color: "green" }}>
                  //           <b>
                  //             {balance_amount.toLocaleString(undefined, {
                  //               minimumFractionDigits: 2,
                  //               maximumFractionDigits: 2,
                  //             })}
                  //           </b>
                  //         </p>
                  //       );
                  //     } else if (balance_amount < 0) {
                  //       return (
                  //         <p style={{ color: "red" }}>
                  //           <b>
                  //             {balance_amount.toLocaleString(undefined, {
                  //               minimumFractionDigits: 2,
                  //               maximumFractionDigits: 2,
                  //             })}
                  //           </b>
                  //         </p>
                  //       );
                  //     } else {
                  //       return (
                  //         <b>
                  //           {balance_amount.toLocaleString(undefined, {
                  //             minimumFractionDigits: 2,
                  //             maximumFractionDigits: 2,
                  //           })}
                  //         </b>
                  //       );
                  //     }
                  //   },

                  // },
                  {
                    title: "Date Created",
                    dataIndex: "created_at",
                    key: "created_at",
                    width: 200,
                    align: "center",
                    render: (text: any, record: any) =>
                      moment(record.created_at).format("YYYY-MM-DD"),
                    // format the date for display
                  },
                  // {
                  //   title: "Status",
                  //   dataIndex: "is_deleted",
                  //   key: "is_deleted",
                  //   align: "center",
                  //   render: (text: any, record: any) =>
                  //     record.is_deleted === null ? (
                  //       ""
                  //     ) : record.is_deleted === 0 ? (
                  //       <Tag color="green">ACTIVE</Tag>
                  //     ) : record.is_deleted === 1 ? (
                  //       <Tag color="red">DELETED</Tag>
                  //     ) : (
                  //       ""
                  //     ),
                  // },
                  {
                    title: "Actions",
                    key: "id",
                    align: "center",
                    render: (item: any, record: any) => {
                      return (
                        <Space>
                          <Tooltip title="View Details">
                            <Button
                              onClick={() =>
                                route.push(`/dashboard/print/${item.id}`)
                              }
                              className="bg-blue-600 text-white ml-1"
                              icon={<ExceptionOutlined />}
                              shape="circle"
                            ></Button>
                          </Tooltip>
                          {/* <Tooltip title="Edit Details">
                            <Button
                              onClick={() =>
                                route.push(`/dashboard/print/manage/${item.id}`)
                              }
                              className="bg-green-600 text-white ml-1"
                              icon={<EditOutlined />}
                              shape="circle"
                            ></Button>
                          </Tooltip> */}
                          <Tooltip title="Add Deductions">
                            <Button
                              onClick={() => handleEdit(item)}
                              className="bg-violet-600 text-white ml-1"
                              icon={<CalculatorOutlined />}
                              shape="circle"
                            ></Button>
                          </Tooltip>
                          <Tooltip title="Change Date">
                            <Button
                              onClick={() => handleChangeDate(item)}
                              className="bg-cyan-600 text-white ml-1"
                              icon={<CalendarOutlined />}
                              shape="circle"
                            ></Button>
                          </Tooltip>
                          {record.is_deleted === 0 ? (
                            <>
                              <Tooltip title="Delete">
                                <Button
                                  onClick={() => handleDelete(item)}
                                  className="bg-red-600 text-white ml-1"
                                  icon={<DeleteOutlined />}
                                  shape="circle"
                                ></Button>
                              </Tooltip>
                            </>
                          ) : (
                            <>
                              <Tooltip title="Restore">
                                <Button
                                  onClick={() => handleRetrieve(item)}
                                  className="bg-yellow-600 text-white ml-1"
                                  icon={<UndoOutlined />}
                                  shape="circle"
                                ></Button>
                              </Tooltip>
                            </>
                          )}
                        </Space>
                      );
                    },
                  },
                ]}
                size="middle"
                bordered
                dataSource={dataSource}
                loading={loading}
                pagination={{
                  current: filters.page,
                  pageSize: filters.pageSize,
                  total: transactionData?.total,
                  showTotal: (total, range) =>
                    `${range[0]}-${
                      range[1]
                    } of ${total.toLocaleString()} items`,
                }}
                scroll={{ x: true }}
                onChange={handleTableChange}
                // responsive
              />
            </div>
          </Card>
        </Col>
      </Row>
      {/* UPDATE ITEMS     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> UPDATE DEDUCTIONS
            </Space>
          </>
        }
        open={openUpdateItem}
        onCancel={() => setopenUpdateItem(false)}
        footer={null}
        width={400}
      >
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleUpdate}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="id"
                label="ID"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name="amount" label="Total Amount">
                <InputNumber
                  readOnly
                  placeholder="Total Amount"
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="philhealth_deduction"
                label="Philhealth"
                required
                rules={[
                  {
                    required: true,
                    message: "Philhealth is required",
                  },
                ]}
              >
                <InputNumber
                  placeholder="Philhealth"
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="aics_deduction"
                label="AICS"
                required
                initialValue={0}
                rules={[
                  {
                    required: true,
                    message: "AICS is required",
                  },
                ]}
              >
                <InputNumber
                  placeholder="AICS"
                  min={0}
                  onChange={onChangeData}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="maep_deduction"
                label="MAIP"
                initialValue={0}
                required
                rules={[
                  {
                    required: true,
                    message: "MAIP is required",
                  },
                ]}
              >
                <InputNumber
                  placeholder="MAEP"
                  onChange={onChangeData}
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="cash"
                label="Cash"
                required
                rules={[
                  {
                    required: true,
                    message: "Cash is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  placeholder="Cash"
                  min={0}
                  onChange={onChangeData}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="qfs"
                label="QFS"
                required
                rules={[
                  {
                    required: true,
                    message: "QFS is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  placeholder="Qfs"
                  min={0}
                  onChange={onChangeData}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="pcso"
                label="PCSO"
                required
                rules={[
                  {
                    required: true,
                    message: "PCSO is required",
                  },
                ]}
                initialValue={0}
              >
                <InputNumber
                  placeholder="PCSO"
                  min={0}
                  onChange={onChangeData}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name="balance" label="Balance">
                <InputNumber
                  readOnly
                  placeholder="Balance"
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              {balanceChecker ? (
                <Alert
                  message={`Balance cannot be negative. Please ensure the amount is valid.`}
                  type="error"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                <Button
                  disabled={insertItems.isLoading}
                  htmlType="submit"
                  type="primary"
                  loading={insertItems.isLoading}
                  className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
                >
                  {insertItems.isLoading ? (
                    <>
                      <Spin /> <span>Please wait..</span>
                    </>
                  ) : (
                    "UPDATE"
                  )}
                  {/* PAY */}
                </Button>
              )}
            </Col>
          </Row>
        </Form>
      </Modal>

      {/* UPDATE ITEMS     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> CHANGE DATE
            </Space>
          </>
        }
        open={openChangeDate}
        onCancel={() => setopenChangeDate(false)}
        footer={null}
        width={400}
      >
        <Form
          form={form3}
          layout="vertical"
          onFinish={handleFinishForm2}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="id"
                label="ID"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>

            <Col span={24}>
              <Form.Item name="fullName" label="Full Name">
                <Input readOnly placeholder="Total Amount" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="totalAmount" label="Bill Amount">
                <InputNumber
                  placeholder="Bill Amount"
                  readOnly
                  min={0}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="created_at"
                label="Date Created"
                required
                rules={[
                  {
                    required: true,
                    message: "Date is required",
                  },
                ]}
              >
                <DatePicker
                  // value={dateRange}
                  style={{ width: "100%" }}
                  format="YYYY-MM-DD"
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              {balanceChecker ? (
                <Alert
                  message={`Balance cannot be negative. Please ensure the amount is valid.`}
                  type="error"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                <Button
                  disabled={insertItems.isLoading}
                  htmlType="submit"
                  type="primary"
                  loading={insertItems.isLoading}
                  className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
                >
                  {insertItems.isLoading ? (
                    <>
                      <Spin /> <span>Please wait..</span>
                    </>
                  ) : (
                    "UPDATE"
                  )}
                  {/* PAY */}
                </Button>
              )}
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};

export default PendingTaxDeclaration;
