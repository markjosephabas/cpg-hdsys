import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  InputNumber,
  Card,
  Alert,
  Space,
  Divider,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";
import { useRouter } from "next/router";
import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  FileDoneOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";
import { router } from "@trpc/server";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const route = useRouter();
  const [form] = Form.useForm();
  const [form2] = Form.useForm();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const hospital = session.data?.user?.municipality;
  const permission = session.data?.user?.permission;
  const queryKey =
    permission === "admin" ? "taxdec.getItemsAll" : "taxdec.getItemsActive";

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(permission === "admin" ? [queryKey] : [queryKey, hospital]);

  const {
    data: data2,
    isLoading: loading2,
    refetch: refetch2,
  } = trpc.useQuery(["taxdec.getBenefiaryActive", hospital ?? ""]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [openUpdateItem, setopenUpdateItem] = useState(false);

  const updateItems = trpc.useMutation("taxdec.updateItems", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const insertSession = trpc.useMutation("taxdec.insertSession", {
    onSuccess(data) {
      refetch();
      form.resetFields();
      route.push(`/dashboard/session_transaction`);
      message.success("Added susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleUpdate2 = async (values: any) => {
    try {
      const hospital = session.data?.user?.municipality ?? "";
      const itemDetails = Object.keys(values)
        .filter(key => key.startsWith("itemName"))
        .map(key => {
          const matchResult = key.match(/\d+/);
          const index = matchResult ? matchResult[0] : null; // Extract the index from the key
          return {
            itemName: values[`itemName[${index}]`],
            unitMeasure: values[`unitMeasure[${index}]`],
            unitPrice: values[`unitPrice[${index}]`],
            quantity: values[`quantity[${index}]`],
            isMedtronics: values[`isMedtronics[${index}]`],
          };
        });

      insertSession.mutate({
        benefeciary: values.beneficiary,
        hospital: hospital,
        itemDetails: itemDetails,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const setData = (value: any) => {
    const selectedObject2 = value
      ? data2?.find(item => item.id === value)
      : null;

    if (selectedObject2 && selectedObject2.id != null) {
      const newItem = {
        class: selectedObject2.class,
      };

      form.setFieldsValue(newItem);
    } else {
      // Handle the case when selectedObject2 is null or undefined
      console.error("selectedObject2 is null or undefined");
    }

    console.log(value);
  };

  return (
    <div>
      <Row>
        <Col sm={24} md={24} lg={16} xs={24}>
          <Card
            title={
              <>
                <Space>
                  <FileDoneOutlined /> SESSIONS
                </Space>
              </>
            }
          >
            <Form
              form={form}
              layout="vertical"
              onFinish={handleUpdate2}
              className="w-full"
              autoComplete="off"
            >
              <Row gutter={12}>
                <Col md={12} sm={12} lg={12}>
                  <Form.Item
                    label="SELECT BENEFICIARY"
                    required
                    name="beneficiary"
                    rules={[
                      {
                        required: true,
                        message: "Name is required",
                      },
                    ]}
                  >
                    <Select
                      showSearch
                      onChange={setData}
                      optionFilterProp="children"
                      filterOption={(input: any, option: any) => {
                        const optionText =
                          option.children && option.children.props
                            ? option.children.props.children
                            : "";
                        return (
                          optionText
                            .toString()
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        );
                      }}
                    >
                      {data2 &&
                        data2.map(data => (
                          <Select.Option value={data.id} key={data.id}>
                            {/* Your JSX code */}
                            <div>
                              {data.firstName} {data.middleName} {data.lastName}
                            </div>
                          </Select.Option>
                        ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col md={12} sm={12} lg={12}>
                  <Form.Item
                    label="Class"
                    required
                    name="class"
                    rules={[
                      {
                        required: true,
                        message: "Name is required",
                      },
                    ]}
                  >
                    <Input readOnly />
                  </Form.Item>
                </Col>
                <Col md={24} sm={24} lg={12}></Col>
                {loading ? (
                  <>
                    <Spin />
                  </>
                ) : (
                  <>
                    <Divider style={{ border: "black" }}>ITEMS USED</Divider>
                    <table className="sessionTable">
                      <tr style={{ textAlign: "center" }}>
                        <td>
                          <b>#</b>
                        </td>
                        <td>
                          <b>ITEM NAME</b>
                        </td>
                        <td style={{ textAlign: "center", width: "20%" }}>
                          <b>UNIT PRICE</b>
                        </td>
                        <td style={{ width: "20%", textAlign: "center" }}>
                          <b>QTY</b>
                        </td>
                      </tr>
                      {data?.map((item, index) => (
                        <>
                          <tr>
                            <td style={{ textAlign: "center" }}>{index + 1}</td>
                            <td>
                              <Col md={24} sm={24} lg={20}>
                                <Form.Item
                                  key={index}
                                  name={`itemName[${index}]`}
                                  // label="Item Name"
                                  style={{ margin: 0 }}
                                  required
                                  initialValue={item.itemName}
                                  rules={[
                                    {
                                      required: true,
                                      message: "First Name is required",
                                    },
                                  ]}
                                >
                                  <Input
                                    placeholder="First Name"
                                    readOnly
                                    style={{ display: "none" }}
                                  />
                                  <span>{item.itemName.toUpperCase()}</span>
                                </Form.Item>
                              </Col>
                            </td>
                            <td>
                              <span>
                                ₱{" "}
                                {`${item.unitPrice.toLocaleString(undefined, {
                                  minimumFractionDigits: 2,
                                  maximumFractionDigits: 2,
                                })} / ${item.unitMeasure}`}
                              </span>
                            </td>
                            <td style={{ textAlign: "center" }}>
                              <Col
                                lg={4}
                                md={8}
                                sm={24}
                                style={{ display: "none" }}
                              >
                                <Form.Item
                                  key={index}
                                  name={`unitPrice[${index}]`}
                                  label="Unit Price"
                                  initialValue={item.unitPrice}
                                  required
                                  rules={[
                                    {
                                      required: true,
                                      message: "Unit Price is required",
                                    },
                                  ]}
                                >
                                  <InputNumber
                                    placeholder="Unit Price "
                                    readOnly
                                    style={{ display: "none" }}
                                  />
                                  <span>
                                    ₱ {item.unitPrice.toLocaleString()}
                                  </span>
                                </Form.Item>
                              </Col>
                              <Col
                                lg={4}
                                md={8}
                                sm={24}
                                style={{ display: "none" }}
                              >
                                <Form.Item
                                  key={index}
                                  name={`unitMeasure[${index}]`}
                                  label="Unit of Measure"
                                  initialValue={item.unitMeasure}
                                  style={{
                                    whiteSpace: "nowrap",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis",
                                  }}
                                  required
                                  rules={[
                                    {
                                      required: true,
                                      message: "Unit of Measure is required",
                                    },
                                  ]}
                                >
                                  <Input
                                    placeholder="Unit of Measure"
                                    readOnly
                                    style={{ display: "none" }}
                                  />
                                  <span>{item.unitMeasure}</span>
                                </Form.Item>
                              </Col>
                              <div>
                                <Form.Item
                                  key={index}
                                  name={`quantity[${index}]`}
                                  // label="Quantity"
                                  initialValue={1}
                                  style={{ margin: 0 }}
                                  required
                                  rules={[
                                    {
                                      required: true,
                                      message: "Quantity is required",
                                    },
                                  ]}
                                >
                                  <InputNumber
                                    min={0}
                                    style={{ width: "80%" }}
                                  />
                                </Form.Item>
                              </div>
                              <Col
                                lg={4}
                                md={8}
                                sm={24}
                                xs={24}
                                style={{ display: "none" }}
                              >
                                {/* {item.isMedtronics} */}
                                <Form.Item
                                  key={index}
                                  name={`isMedtronics[${index}]`}
                                  label="Unit Price"
                                  initialValue={item.isMedtronics}
                                  required
                                  rules={[
                                    {
                                      required: true,
                                      message: "Unit Price is required",
                                    },
                                  ]}
                                >
                                  <InputNumber
                                    placeholder="Unit Price "
                                    readOnly
                                    style={{ display: "none" }}
                                  />
                                </Form.Item>
                              </Col>
                            </td>
                          </tr>
                        </>
                      ))}
                      <tr>
                        <td colSpan={4} style={{ verticalAlign: "middle" }}>
                          <Button
                            disabled={insertSession.isLoading}
                            htmlType="submit"
                            type="primary"
                            loading={insertSession.isLoading}
                            style={{
                              width: "400px",
                              margin: "auto",
                              display: "block",
                            }}
                            className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline "
                          >
                            {insertSession.isLoading ? (
                              <>
                                <Spin /> <span>Please wait..</span>
                              </>
                            ) : (
                              "SUBMIT"
                            )}
                            {/* PAY */}
                          </Button>
                        </td>
                      </tr>
                    </table>
                  </>
                )}
              </Row>
            </Form>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default PendingTaxDeclaration;
