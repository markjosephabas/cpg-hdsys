import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  InputNumber,
  Card,
  Alert,
  Space,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  FileDoneOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality;
  const permission = session.data?.user?.permission;

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getHospital"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);

  const updateHospital = trpc.useMutation("taxdec.updateHospital", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const insertHospital = trpc.useMutation("taxdec.insertHospital", {
    onSuccess(data) {
      refetch();
      form.resetFields();
      message.success("Added susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleUpdate2 = async (values: any) => {
    try {
      const hospital = session.data?.user?.municipality;

      insertHospital.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const handleUpdate = async (values: any) => {
    try {
      updateHospital.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  return (
    <div>
      <Row>
        <Col
          style={{
            width: "800px",
          }}
        >
          <Card className="fade-in">
            <div>
              <div className="flex p-3 bg-custom-blue mb-5 rounded ">
                <span className="uppercase text-2xl font-bold text-white">
                  HOSPITAL REGISTRATION
                </span>
                <Button
                  className="ml-auto text-custom-blue bg-white border-custom-blue flex items-center  rounded-full"
                  icon={<PlusOutlined />}
                  onClick={() => {
                    setopenUpdateItem2(true);
                    setMessage("");
                  }}
                >
                  CREATE{" "}
                </Button>
              </div>

              <Table
                columns={[
                  {
                    title: "ID",
                    dataIndex: "id",
                    key: "id",
                    align: "center",
                  },

                  {
                    title: "Hospital",
                    dataIndex: "hospital",
                    key: "hospital",
                    align: "center",
                    filterDropdown: ({
                      setSelectedKeys,
                      selectedKeys,
                      confirm,
                      clearFilters,
                    }: CustomFilterDropdownProps) => (
                      <div style={{ padding: 8 }}>
                        <Input
                          placeholder="Search hospital"
                          value={selectedKeys[0]}
                          onChange={e =>
                            setSelectedKeys(
                              e.target.value ? [e.target.value] : []
                            )
                          }
                          onPressEnter={() => confirm()}
                          style={{ marginBottom: 8, display: "block" }}
                        />
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "flex-end",
                          }}
                        >
                          <Button
                            onClick={() => {
                              clearFilters?.();
                              confirm();
                            }}
                            style={{ marginRight: 8 }}
                            size="small"
                          >
                            Reset
                          </Button>
                          <Button onClick={() => confirm()} size="small">
                            Filter
                          </Button>
                        </div>
                      </div>
                    ),
                    filterIcon: () => (
                      <Tooltip title="Search hospital">
                        <SearchOutlined />
                      </Tooltip>
                    ),
                    onFilter: (value: any, record: any) => {
                      if (!value || value.length < 2) {
                        return true;
                      }
                      return record.address
                        .toLowerCase()
                        .includes(value.toLowerCase());
                    },
                    render: (text: string, record: any) =>
                      record.hospitalName.toUpperCase(),
                  },

                  {
                    title: "Actions",
                    key: "id",
                    align: "center",
                    render: (item: any) => {
                      return (
                        <div>
                          {/* <Tooltip title="Cancel">
                        <Button
                          onClick={() => handleEdit(item)}
                          icon={<DeleteOutlined />}
                          shape="circle"
                          danger
                          type="primary"
                        />
                      </Tooltip> */}
                          <Tooltip title="Edit">
                            <Button
                              onClick={() => handleEdit(item)}
                              className="bg-blue-600 text-white ml-1"
                              icon={<FormOutlined />}
                              shape="circle"
                            ></Button>
                          </Tooltip>
                        </div>
                      );
                    },
                  },
                ]}
                size="middle"
                bordered
                dataSource={
                  data &&
                  data.map((item: any, index: any) => ({
                    ...item,
                    key: index,
                  }))
                }
                loading={loading}
                pagination={{
                  showTotal: (total, range) =>
                    `${range[0]}-${range[1]} of ${total} items`,
                }}
                scroll={{ x: true }}
                // responsive
              />
            </div>
          </Card>
        </Col>
      </Row>
      {/* UPDATE ITEMS     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> UPDATE HOSPITAL
            </Space>
          </>
        }
        open={openUpdateItem}
        onCancel={() => setopenUpdateItem(false)}
        footer={null}
        width={400}
      >
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleUpdate}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="id"
                label="ID"
                required
                rules={[
                  {
                    required: true,
                    message: "Hospital Name is required",
                  },
                ]}
              >
                <Input placeholder="Hospital Name" />
              </Form.Item>
            </Col>

            <Col span={24}>
              <Form.Item name="hospitalName" label="HOSPITAL NAME">
                <Input placeholder="Enter hospital name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={updateHospital.isLoading}
                htmlType="submit"
                type="primary"
                loading={updateHospital.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {updateHospital.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>

      {/* ADD ITEMS     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> ADD HOSPITAL
            </Space>
          </>
        }
        open={openUpdateItem2}
        onCancel={() => setopenUpdateItem2(false)}
        footer={null}
        width={400}
      >
        <Form
          form={form}
          layout="vertical"
          onFinish={handleUpdate2}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24}>
              <Form.Item
                name="hospitalName"
                label="Hospital Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Hospital Name is required",
                  },
                ]}
              >
                <Input placeholder="Hospital Name" />
              </Form.Item>
            </Col>

            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={insertHospital.isLoading}
                htmlType="submit"
                type="primary"
                loading={insertHospital.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {insertHospital.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "SUBMIT"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};

export default PendingTaxDeclaration;
