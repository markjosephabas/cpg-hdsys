import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  InputNumber,
  Card,
  Alert,
  Space,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import moment, { Moment } from "moment";
import { useRouter } from "next/router";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  FileDoneOutlined,
  ExceptionOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality || "Cebu Province";
  const permission = session.data?.user?.permission;

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getStatementAdmin"]);

  const {
    data: data2,
    isLoading: loading2,
    refetch: refetch2,
  } = trpc.useQuery(["taxdec.getHospital"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();
  const route = useRouter();
  const [openUpdateItem, setopenUpdateItem] = useState(false);

  const updateItems = trpc.useMutation("taxdec.updateItems", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const insertItems = trpc.useMutation("taxdec.insertItems", {
    onSuccess(data) {
      refetch();
      form.resetFields();
      message.success("Added susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleUpdate2 = async (values: any) => {
    try {
      const hospital = session.data?.user?.municipality;

      insertItems.mutate({
        ...values,
        hospital: hospital,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const handleUpdate = async (values: any) => {
    try {
      updateItems.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const getSum = (values: any) => {
    const sum = values.reduce((acc: any, curr: any) => {
      // Extract unitPrice and quantity from the current object
      const { unitPrice, quantity } = curr;

      console.log(curr);
      // Convert unitPrice and quantity to numbers and add to the accumulator
      return acc + Number(unitPrice) * Number(quantity);
    }, 0); // Initial accumulator value is 0

    // Check if the sum is a valid number
    if (!isNaN(sum)) {
      console.log(sum);
      return sum.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      });
    } else {
      console.error("Invalid values for unitPrice or quantity");
      return 0; // Or return any default value as needed
    }
  };
  const deleteStatement = trpc.useMutation("taxdec.deleteStatement", {
    onSuccess() {
      message.success("Deleted successfully!");
      refetch();
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });
  const handleClickDelete = (item: any) => {
    deleteStatement.mutate({
      statement_code: item.statement_code,
    });
  };
  function handleDelete(item: any) {
    Modal.confirm({
      title: `Are you sure you want to delete this transaction with statement number: ${item.statement_code} ?`,
      onOk() {
        handleClickDelete(item);
      },
      onCancel() {
        // The user clicked Cancel
        // Do nothing or handle the cancel event here
      },
      okButtonProps: {
        className:
          "bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline",
      },
    });
  }

  return (
    <div>
      <Row>
        <Col sm={24} md={24} lg={24} xs={24}>
          <Card className="fade-in">
            <div>
              <div className="flex p-3 bg-custom-blue mb-5 rounded ">
                <span className="uppercase text-2xl font-bold text-white">
                  STATEMENTS
                </span>
              </div>

              <Table
                columns={[
                  {
                    title: "STATEMENT CODE",
                    dataIndex: "statement_code",
                    key: "statement_code",
                    align: "center",
                  },
                  {
                    title: "Hospital",
                    dataIndex: "hospital",
                    key: "hospital",
                    align: "center",
                    filters:
                      data2 &&
                      data2.map((item, index) => ({
                        text: item.hospitalName,
                        value: item.hospitalName,
                      })),
                    onFilter: (value, record) => {
                      if (
                        typeof record.hospital === "string" &&
                        typeof value === "string"
                      ) {
                        return record.hospital
                          .toLowerCase()
                          .includes(value.toLowerCase());
                      }
                      return false;
                    },
                    ellipsis: true,
                    render: (text: string, record: any) =>
                      typeof record.hospital === "string"
                        ? record.hospital.toUpperCase()
                        : record.hospital,
                  },

                  {
                    title: "Sessions",
                    dataIndex: "session",
                    key: "session",
                    align: "center",
                    render: (text: string, record: any) =>
                      record._sum.totalAmount / 1850,
                  },
                  {
                    title: "AMOUNT",
                    dataIndex: "amount",
                    key: "amount",
                    align: "center",
                    render: (text: string, record: any) =>
                      record._sum.totalAmount.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      }),
                  },
                  {
                    title: "Class",
                    dataIndex: "class",
                    key: "class",
                    align: "center",
                    filters: [
                      {
                        text: "Indigent",
                        value: "Indigent",
                      },
                      {
                        text: "Paying",
                        value: "Paying",
                      },
                      {
                        text: "POS",
                        value: "POS",
                      },
                      {
                        text: "PWD",
                        value: "PWD",
                      },
                      {
                        text: "Beneficiary (Indigent/POS/PWD)",
                        value: "Beneficiary (Indigent/POS/PWD)",
                      },
                    ],
                    onFilter: (value, record) =>
                      record.class ? record.class.includes(value) : false,
                    ellipsis: true,
                    render: (text: any, record: any) =>
                      record.class === null ? (
                        ""
                      ) : record.class === "Indigent" ? (
                        <Tag color="green">INDIGENT</Tag>
                      ) : record.class === "Beneficiary (Indigent/POS/PWD)" ? (
                        <Tag color="green">Beneficiary (Indigent/POS/PWD)</Tag>
                      ) : record.class === "Paying" ? (
                        <Tag color="blue">PAYING</Tag>
                      ) : record.class === "POS" ? (
                        <Tag color="purple">POS</Tag>
                      ) : record.class === "PWD" ? (
                        <Tag color="cyan">PWD</Tag>
                      ) : (
                        ""
                      ),
                  },
                  {
                    title: "DATE OF STATEMENT",
                    dataIndex: "Date",
                    key: "Date",
                    align: "center",
                    render: (text: string, record: any) =>
                      moment(record.DateFrom).format("MMM DD") +
                      " - " +
                      moment(record.DateTo).format("DD YYYY"),
                  },
                  {
                    title: "DATE CREATED",
                    dataIndex: "statement_code",
                    key: "statement_code",
                    align: "center",
                    render: (text: string, record: any) =>
                      moment(record.created_at).format("MMM DD YYYY"),
                  },
                  {
                    title: "Actions",
                    key: "id",
                    align: "center",
                    render: (item: any) => {
                      return (
                        <Space>
                          <Tooltip title="View Statement Details">
                            <Button
                              onClick={() =>
                                route.push(
                                  `/dashboard/print/statement/${item.statement_code}`
                                )
                              }
                              className="bg-green-600 text-white ml-1"
                              icon={<ExceptionOutlined />}
                              shape="circle"
                            ></Button>
                          </Tooltip>
                          <Tooltip title="View Summary Details">
                            <Button
                              onClick={() =>
                                route.push(
                                  `/dashboard/print/summary/${item.statement_code}`
                                  // `/dashboard/print/summaryv2/${item.statement_code}`
                                )
                              }
                              className="bg-pink-600 text-white ml-1"
                              icon={<ExceptionOutlined />}
                              shape="circle"
                            ></Button>
                          </Tooltip>
                          <Tooltip title="View Summary">
                            <Button
                              onClick={() =>
                                route.push(
                                  // `/dashboard/print/summary/${item.statement_code}`
                                  `/dashboard/print/summaryv2/${item.statement_code}`
                                )
                              }
                              className="bg-blue-600 text-white ml-1"
                              icon={<ExceptionOutlined />}
                              shape="circle"
                            ></Button>
                          </Tooltip>
                          <Tooltip title="Delete">
                            <Button
                              onClick={() => handleDelete(item)}
                              className="bg-red-600 text-white ml-1"
                              icon={<DeleteOutlined />}
                              shape="circle"
                            ></Button>
                          </Tooltip>
                        </Space>
                      );
                    },
                  },
                ]}
                size="middle"
                bordered
                dataSource={
                  data &&
                  data.map((item: any, index: any) => ({
                    ...item,
                    key: index,
                  }))
                }
                loading={loading}
                pagination={{
                  showTotal: (total, range) =>
                    `${range[0]}-${range[1]} of ${total} items`,
                }}
                scroll={{ x: true }}
              />
            </div>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default PendingTaxDeclaration;
