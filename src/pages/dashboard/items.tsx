import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  InputNumber,
  Card,
  Alert,
  Space,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  FileDoneOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality;
  const permission = session.data?.user?.permission;
  const queryKey =
    permission === "admin" ? "taxdec.getItemsAll" : "taxdec.getItems";

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(permission === "admin" ? [queryKey] : [queryKey, hospital]);

  const {
    data: data2,
    isLoading: loading2,
    refetch: refetch2,
  } = trpc.useQuery(["taxdec.getHospital"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);

  const updateItems = trpc.useMutation("taxdec.updateItems", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const insertItems = trpc.useMutation("taxdec.insertItems", {
    onSuccess(data) {
      refetch();
      form.resetFields();
      message.success("Added susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleUpdate2 = async (values: any) => {
    try {
      const hospital = session.data?.user?.municipality;

      insertItems.mutate({
        ...values,
        hospital: hospital,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const handleUpdate = async (values: any) => {
    try {
      updateItems.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  return (
    <div>
      <Row>
        <Col sm={24} md={24} lg={18} xs={24}>
          <Card className="fade-in">
            <div className="flex p-3 bg-custom-blue mb-5 rounded ">
              <span className="uppercase text-2xl font-bold text-white">
                ITEMS
              </span>
              <Button
                className="ml-auto text-custom-blue bg-white border-custom-blue flex items-center  rounded-full"
                icon={<PlusOutlined />}
                onClick={() => {
                  setopenUpdateItem2(true);
                  setMessage("");
                }}
              >
                CREATE{" "}
              </Button>
            </div>
            <div>
              {/* <div className="text-2xl font-bold mb-10 text-black-200">
            <div className="text-4xl font-bold text-gray-900 ">PENDING</div>
            <div className="text-xl font-medium text-gray-600 mb-8">
              Transactions
            </div>
          </div> */}

              <Table
                columns={[
                  {
                    title: "ID",
                    dataIndex: "id",
                    key: "id",
                    align: "center",
                  },
                  {
                    title: "Item Name",
                    dataIndex: "itemName",
                    key: "itemName",
                    filterDropdown: ({
                      setSelectedKeys,
                      selectedKeys,
                      confirm,
                      clearFilters,
                    }: CustomFilterDropdownProps) => (
                      <div style={{ padding: 8 }}>
                        <Input
                          placeholder="Search plate number"
                          value={selectedKeys[0]}
                          onChange={e =>
                            setSelectedKeys(
                              e.target.value ? [e.target.value] : []
                            )
                          }
                          onPressEnter={() => confirm()}
                          style={{ marginBottom: 8, display: "block" }}
                        />
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "flex-end",
                          }}
                        >
                          <Button
                            onClick={() => {
                              clearFilters?.();
                              confirm();
                            }}
                            style={{ marginRight: 8 }}
                            size="small"
                          >
                            Reset
                          </Button>
                          <Button onClick={() => confirm()} size="small">
                            Filter
                          </Button>
                        </div>
                      </div>
                    ),
                    filterIcon: () => (
                      <Tooltip title="Search by owner name">
                        <SearchOutlined />
                      </Tooltip>
                    ),
                    onFilter: (value: any, record: any) => {
                      if (!value || value.length < 2) {
                        return true;
                      }
                      return record.itemName
                        .toLowerCase()
                        .includes(value.toLowerCase());
                    },
                    render: (text: string, record: any) =>
                      `${record.itemName}`.toUpperCase(),
                  },
                  {
                    title: "Unit Price",
                    dataIndex: "unitPrice",
                    key: "unitPrice",
                    align: "center",
                    render: (text: string, record: any) =>
                      record.unitPrice.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      }),
                  },

                  {
                    title: "Hospital",
                    dataIndex: "hospital",
                    key: "hospital",
                    align: "center",
                    filters:
                      data2 &&
                      data2.map((item, index) => ({
                        text: item.hospitalName,
                        value: item.hospitalName,
                      })),
                    onFilter: (value, record) =>
                      record.hospital.includes(value),
                    ellipsis: true,
                    render: (text: string, record: any) =>
                      record.hospital.toUpperCase(),
                  },
                  {
                    title: "Status",
                    dataIndex: "status",
                    key: "status",
                    align: "center",
                    filters: [
                      {
                        text: "Active",
                        value: "Active",
                      },
                      {
                        text: "Inactive",
                        value: "Inactive",
                      },
                    ],
                    onFilter: (value, record) => record.status.includes(value),
                    ellipsis: true,
                    render: (text: any, record: any) =>
                      record.status === null ? (
                        ""
                      ) : record.status === "Active" ? (
                        <Tag color="green">ACTIVE</Tag>
                      ) : record.status === "Inactive" ? (
                        <Tag color="volcano">INACTIVE</Tag>
                      ) : (
                        ""
                      ),
                  },

                  {
                    title: "Actions",
                    key: "id",
                    align: "center",
                    render: (item: any) => {
                      return (
                        <div>
                          {/* <Tooltip title="Cancel">
                        <Button
                          onClick={() => handleEdit(item)}
                          icon={<DeleteOutlined />}
                          shape="circle"
                          danger
                          type="primary"
                        />
                      </Tooltip> */}
                          <Tooltip title="Edit">
                            <Button
                              onClick={() => handleEdit(item)}
                              className="bg-blue-600 text-white ml-1"
                              icon={<FormOutlined />}
                            ></Button>
                          </Tooltip>
                        </div>
                      );
                    },
                  },
                ]}
                size="middle"
                bordered
                dataSource={
                  data &&
                  data.map((item: any, index: any) => ({
                    ...item,
                    key: index,
                  }))
                }
                loading={loading}
                pagination={{
                  showTotal: (total, range) =>
                    `${range[0]}-${range[1]} of ${total} items`,
                }}
                scroll={{ x: true }}
                // responsive
              />
            </div>
          </Card>
        </Col>
      </Row>
      {/* UPDATE ITEMS     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> UPDATE ITEMS
            </Space>
          </>
        }
        open={openUpdateItem}
        onCancel={() => setopenUpdateItem(false)}
        footer={null}
        width={400}
      >
        <Form
          form={form2}
          layout="vertical"
          onFinish={handleUpdate}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24} style={{ display: "none" }}>
              <Form.Item
                name="id"
                label="ID"
                required
                rules={[
                  {
                    required: true,
                    message: "First Name is required",
                  },
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="itemName"
                label="Item Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Item Name is required",
                  },
                ]}
              >
                <Input placeholder="Item Name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="unitMeasure"
                label="Unit of Measure"
                required
                rules={[
                  {
                    required: true,
                    message: "Unit of Measure is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="PC" key="PC">
                    PC
                  </Select.Option>
                  <Select.Option value="PAIR" key="PAIR">
                    PAIR
                  </Select.Option>
                  <Select.Option value="PU" key="PU">
                    PU
                  </Select.Option>
                  <Select.Option value="PACK" key="PACK">
                    PACK
                  </Select.Option>
                  <Select.Option value="SERVE" key="SERVE">
                    SERVE
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="unitPrice"
                label="Unit Price"
                initialValue={1}
                required
                rules={[
                  {
                    required: true,
                    message: "Unit Price is required",
                  },
                ]}
              >
                <InputNumber
                  placeholder="Unit Price"
                  min={1}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="status"
                label="Status"
                required
                rules={[
                  {
                    required: true,
                    message: "Gender is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="Active" key="Active">
                    Active
                  </Select.Option>
                  <Select.Option value="Inactive" key="Inactive">
                    Inactive
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            {/* <Col span={24}>
              <Form.Item name="bus_name" label="BUS NAME">
                <Input placeholder="Enter bus name" />
              </Form.Item>
            </Col> */}
            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={insertItems.isLoading}
                htmlType="submit"
                type="primary"
                loading={insertItems.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {insertItems.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "UPDATE"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>

      {/* ADD ITEMS     */}
      <Modal
        title={
          <>
            <Space>
              <BiUser /> ADD ITEMS
            </Space>
          </>
        }
        open={openUpdateItem2}
        onCancel={() => setopenUpdateItem2(false)}
        footer={null}
        width={400}
      >
        <Form
          form={form}
          layout="vertical"
          onFinish={handleUpdate2}
          className="w-full"
          autoComplete="off"
        >
          <Row gutter={12}>
            <Col span={24}>
              <Form.Item
                name="itemName"
                label="Item Name"
                required
                rules={[
                  {
                    required: true,
                    message: "Item Name is required",
                  },
                ]}
              >
                <Input placeholder="Item Name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="unitMeasure"
                label="Unit of Measure"
                required
                rules={[
                  {
                    required: true,
                    message: "Unit of Measure is required",
                  },
                ]}
              >
                <Select>
                  <Select.Option value="PC" key="PC">
                    PC
                  </Select.Option>
                  <Select.Option value="PAIR" key="PAIR">
                    PAIR
                  </Select.Option>
                  <Select.Option value="PU" key="PU">
                    PU
                  </Select.Option>
                  <Select.Option value="PACK" key="PACK">
                    PACK
                  </Select.Option>
                  <Select.Option value="SERVE" key="SERVE">
                    SERVE
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="unitPrice"
                label="Unit Price"
                initialValue={1}
                required
                rules={[
                  {
                    required: true,
                    message: "Unit Price is required",
                  },
                ]}
              >
                <InputNumber
                  placeholder="Unit Price"
                  min={1}
                  style={{ width: "100%" }}
                  formatter={value =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                />
              </Form.Item>
            </Col>

            {/* <Col span={24}>
              <Form.Item name="bus_name" label="BUS NAME">
                <Input placeholder="Enter bus name" />
              </Form.Item>
            </Col> */}
            <Col span={24}>
              {txtmessage ? (
                <Alert
                  message={txtmessage}
                  type="success"
                  closable
                  showIcon
                  style={{ marginBottom: "10px" }}
                />
              ) : (
                ""
              )}

              <Button
                disabled={insertItems.isLoading}
                htmlType="submit"
                type="primary"
                loading={insertItems.isLoading}
                className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline w-full"
              >
                {insertItems.isLoading ? (
                  <>
                    <Spin /> <span>Please wait..</span>
                  </>
                ) : (
                  "SUBMIT"
                )}
                {/* PAY */}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};

export default PendingTaxDeclaration;
