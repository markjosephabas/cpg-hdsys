import { Button, Card, Row, Col, Input, Form, InputNumber } from "antd";
import { NextPage } from "next";
import { TagsOutlined } from "@ant-design/icons";
import { trpc } from "../../../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import moment from "moment";
import { FilterDropdownProps } from "antd/lib/table/interface";

type Props = {
  req: any;
  res: any;
  query: any;
};
export const getServerSideProps = async function ({ req, res, query }: Props) {
  const id = query.id;

  return {
    props: {
      id: id ?? null,
    },
  };
};

const UserManagement: NextPage = ({ id }: any) => {
  const [router_id, setID] = useState<number>(parseInt(id));
  const [form] = Form.useForm();
  const session = useSession();

  const {
    data,
    isLoading: loading,
    refetch,
  } = trpc.useQuery(["taxdec.getSessionTransactionByID", router_id]);
  // console.log(data);
  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  // END INSERT

  const printDiv = () => {
    const divToPrint = document?.getElementById("printMe");

    if (!divToPrint) {
      return;
    }

    const printWindow = window.open("", "_blank", "height=800,width=1200");

    printWindow?.document.write(
      "<html><head><title>Print</title><style>table {font-size:12px;border-collapse:collapse;} p{margin:0} #deleteBtn {display:none}</style></head><body>"
    );

    // Clone the content to preserve the original content
    const clonedContent = divToPrint.cloneNode(true);
    printWindow?.document.body.appendChild(clonedContent);

    printWindow?.document.write("</body></html>");
    printWindow?.document.close();
    setTimeout(() => {
      printWindow?.print();
    }, 500);
  };

  const overallTotal = data?.reduce((acc, session) => {
    const itemTotal = session.sessions_itemUsed.reduce((itemAcc, item) => {
      const total = item.unitPrice * item.quantity;
      return itemAcc + total;
    }, 0);

    return acc + itemTotal;
  }, 0);

  const divContainerStyle: React.CSSProperties = {
    display: "flex",
    justifyContent: "space-between", // Distribute space between the two divs
    width: "100%", // Use the full width of the container
  };

  const divStyleLeft: React.CSSProperties = {
    borderTop: "2px solid black",
    width: "200px",
    textAlign: "center",
    marginTop: "50px",
  };

  const divStyleRight: React.CSSProperties = {
    borderTop: "2px solid black",
    width: "200px",
    textAlign: "center",
    marginTop: "50px",
    float: "right",
  };

  const handleUpdate2 = async (values: any) => {
    console.log(values);
    // try {
    //   const hospital = session.data?.user?.municipality ?? "";
    //   const itemDetails = Object.keys(values)
    //     .filter(key => key.startsWith("itemName"))
    //     .map(key => {
    //       const matchResult = key.match(/\d+/);
    //       const index = matchResult ? matchResult[0] : null; // Extract the index from the key
    //       return {
    //         itemName: values[`itemName[${index}]`],
    //         unitMeasure: values[`unitMeasure[${index}]`],
    //         unitPrice: values[`unitPrice[${index}]`],
    //         quantity: values[`quantity[${index}]`],
    //         isMedtronics: values[`isMedtronics[${index}]`],
    //       };
    //     });

    //   insertSession.mutate({
    //     benefeciary: values.beneficiary,
    //     hospital: hospital,
    //     itemDetails: itemDetails,
    //   });
    // } catch (error) {
    //   // If there's an error during the mutation, handle it
    //   console.error(error);
    //   // You can add additional error handling logic here if needed
    // }
  };

  const updateItem = async (values: any) => {
    console.log(values);
  };
  return (
    <Row gutter={[16, 16]}>
      <Col span={18}>
        <Card bordered={true}>
          <div id="printMe">
            <Form
              form={form}
              layout="vertical"
              //   onFinish={handleUpdate2}
              // onFinish={handleUpdate2}
              className="w-full"
              autoComplete="off"
            >
              <table className="sessionTable">
                <thead>
                  <tr>
                    <td
                      colSpan={5}
                      style={{
                        textAlign: "center",
                        fontWeight: "bold",
                      }}
                    >
                      <h1>UPDATE SESSION DETAILS</h1>
                      <p
                        style={{
                          textAlign: "left",
                          fontStyle: "normal",
                          fontWeight: "normal",
                        }}
                      >
                        <span>
                          {" "}
                          NAME &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; :{" "}
                          {data && data[0]?.beneficiary.middleName
                            ? data[0]?.beneficiary.lastName.toUpperCase() +
                              ", " +
                              data[0]?.beneficiary.firstName.toUpperCase() +
                              " " +
                              data[0]?.beneficiary.middleName?.toUpperCase()
                            : ""}
                        </span>
                        <span style={{ float: "right" }}>
                          Class &nbsp; &nbsp;:{" "}
                          {data && data[0]?.beneficiary.class.toUpperCase()}
                        </span>
                      </p>
                      <p
                        style={{
                          textAlign: "left",
                          fontStyle: "normal",
                          fontWeight: "normal",
                        }}
                      >
                        <span>
                          ADDRESS &nbsp; &nbsp;:{" "}
                          {data && data[0]?.beneficiary.address.toUpperCase()}
                        </span>
                      </p>
                    </td>
                  </tr>
                  <tr style={{ textAlign: "center" }}>
                    <td>
                      <b>#</b>
                    </td>
                    <td>
                      <b>ITEM NAME</b>
                    </td>
                    <td style={{ textAlign: "center", width: "20%" }}>
                      <b>UNIT PRICE</b>
                    </td>
                    <td style={{ width: "20%", textAlign: "center" }}>
                      <b>QTY</b>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  {data &&
                    data[0]?.sessions_itemUsed.map((item: any, index) => (
                      <>
                        <tr>
                          <td style={{ textAlign: "center" }}>{index + 1}</td>
                          <td>
                            <Col md={24} sm={24} lg={20}>
                              <Form.Item
                                key={index}
                                name={`itemName[${index}]`}
                                // label="Item Name"
                                style={{ margin: 0 }}
                                required
                                initialValue={item.itemName}
                                rules={[
                                  {
                                    required: true,
                                    message: "First Name is required",
                                  },
                                ]}
                              >
                                <Input
                                  placeholder="First Name"
                                  readOnly
                                  style={{ display: "none" }}
                                />
                                <span>{item.itemName.toUpperCase()}</span>
                              </Form.Item>
                            </Col>
                          </td>
                          <td>
                            <span>
                              ₱{" "}
                              {`${item.unitPrice.toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2,
                              })} / ${item.unitMeasure}`}
                            </span>
                          </td>
                          <td style={{ textAlign: "center" }}>
                            <Col
                              lg={4}
                              md={8}
                              sm={24}
                              style={{ display: "none" }}
                            >
                              <Form.Item
                                key={index}
                                name={`unitPrice[${index}]`}
                                label="Unit Price"
                                initialValue={item.unitPrice}
                                required
                                rules={[
                                  {
                                    required: true,
                                    message: "Unit Price is required",
                                  },
                                ]}
                              >
                                <InputNumber
                                  placeholder="Unit Price "
                                  readOnly
                                  style={{ display: "none" }}
                                />
                                <span>₱ {item.unitPrice.toLocaleString()}</span>
                              </Form.Item>
                            </Col>
                            <Col
                              lg={4}
                              md={8}
                              sm={24}
                              style={{ display: "none" }}
                            >
                              <Form.Item
                                key={index}
                                name={`unitMeasure[${index}]`}
                                label="Unit of Measure"
                                initialValue={item.unitMeasure}
                                style={{
                                  whiteSpace: "nowrap",
                                  overflow: "hidden",
                                  textOverflow: "ellipsis",
                                }}
                                required
                                rules={[
                                  {
                                    required: true,
                                    message: "Unit of Measure is required",
                                  },
                                ]}
                              >
                                <Input
                                  placeholder="Unit of Measure"
                                  readOnly
                                  style={{ display: "none" }}
                                />
                                <span>{item.unitMeasure}</span>
                              </Form.Item>
                            </Col>
                            <div>
                              <Form.Item
                                key={index}
                                name={`quantity[${index}]`}
                                // label="Quantity"
                                initialValue={1}
                                style={{ margin: 0 }}
                                required
                                rules={[
                                  {
                                    required: true,
                                    message: "Quantity is required",
                                  },
                                ]}
                              >
                                <InputNumber min={0} style={{ width: "80%" }} />
                              </Form.Item>
                            </div>
                            <Col
                              lg={4}
                              md={8}
                              sm={24}
                              xs={24}
                              style={{ display: "none" }}
                            >
                              {/* {item.isMedtronics} */}
                              <Form.Item
                                key={index}
                                name={`isMedtronics[${index}]`}
                                label="Unit Price"
                                initialValue={item.isMedtronics}
                                required
                                rules={[
                                  {
                                    required: true,
                                    message: "Unit Price is required",
                                  },
                                ]}
                              >
                                <InputNumber
                                  placeholder="Unit Price "
                                  readOnly
                                  style={{ display: "none" }}
                                />
                              </Form.Item>
                            </Col>
                          </td>
                        </tr>
                      </>
                    ))}
                </tbody>
              </table>
            </Form>
          </div>
        </Card>
      </Col>
    </Row>
  );
};

export default UserManagement;
