import { Button, Card, Row, Col } from "antd";
import { NextPage } from "next";
import { TagsOutlined } from "@ant-design/icons";
import { trpc } from "../../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import moment from "moment";
import { FilterDropdownProps } from "antd/lib/table/interface";

type Props = {
  req: any;
  res: any;
  query: any;
};
export const getServerSideProps = async function ({ req, res, query }: Props) {
  const id = query.id;

  return {
    props: {
      id: id ?? null,
    },
  };
};

const UserManagement: NextPage = ({ id }: any) => {
  const [router_id, setID] = useState<number>(parseInt(id));

  const session = useSession();

  const {
    data,
    isLoading: loading,
    refetch,
  } = trpc.useQuery(["taxdec.getSessionTransactionByID", router_id]);
  console.log(data);
  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  // END INSERT

  const printDiv = () => {
    const divToPrint = document?.getElementById("printMe");

    if (!divToPrint) {
      return;
    }

    const printWindow = window.open("", "_blank", "height=800,width=1200");

    printWindow?.document.write(
      "<html><head><title>Print</title><style>table {font-size:12px;border-collapse:collapse;} p{margin:0} #deleteBtn {display:none}</style></head><body>"
    );

    // Clone the content to preserve the original content
    const clonedContent = divToPrint.cloneNode(true);
    printWindow?.document.body.appendChild(clonedContent);

    printWindow?.document.write("</body></html>");
    printWindow?.document.close();
    setTimeout(() => {
      printWindow?.print();
    }, 500);
  };

  const overallTotal = data?.reduce((acc, session) => {
    const itemTotal = session.sessions_itemUsed.reduce((itemAcc, item) => {
      const total = item.unitPrice * item.quantity;
      return itemAcc + total;
    }, 0);

    return acc + itemTotal;
  }, 0);

  const divContainerStyle: React.CSSProperties = {
    display: "flex",
    justifyContent: "space-between", // Distribute space between the two divs
    width: "100%", // Use the full width of the container
  };

  const divStyleLeft: React.CSSProperties = {
    borderTop: "2px solid black",
    width: "200px",
    textAlign: "center",
    marginTop: "50px",
  };

  const divStyleRight: React.CSSProperties = {
    borderTop: "2px solid black",
    width: "200px",
    textAlign: "center",
    marginTop: "50px",
    float: "right",
  };
  return (
    <Row gutter={[16, 16]}>
      <Col span={18}>
        <Card
          // title={
          //   <>
          //     <TagsOutlined /> &nbsp; SUPPLIES LEDGER CARD
          //   </>
          // }
          bordered={true}
        >
          {/* <div className="w-full mb-5" style={{backgroundColor:"#2c2b31", padding:"5px", textAlign:"center"}}>
                  <h1 style={{color:"white"}}>PURCHASE ORDER DETAILS</h1>
              </div> */}
          <Button className="bg-blue-600 text-white" onClick={printDiv}>
            Print
          </Button>
          <div id="printMe">
            <table
              style={{
                width: "100%",
                marginTop: "5px",
              }}
            >
              <thead>
                <tr>
                  <td
                    colSpan={5}
                    style={{
                      textAlign: "center",
                      fontWeight: "bold",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <div style={{ marginLeft: "-30px", marginTop: "-30px" }}>
                        {/* Your image */}
                        <img
                          src="/capitol.png"
                          alt="My Image"
                          width="100"
                          id="logo"
                        />
                      </div>

                      <div style={{ marginRight: "50px" }}>
                        {/* Your text */}
                        <p>REPUBLIC OF THE PHILIPPINES</p>
                        <p>PROVINCE OF CEBU</p>
                        <p>{session.data?.user?.municipality.toUpperCase()}</p>
                        {/* <p style={{ fontSize: "5px" }}>Telephone.</p>
                    <p style={{ fontSize: "5px" }}>Email Address.</p> */}
                        <p style={{ fontStyle: "normal" }}>
                          HEMODIALYSIS EXPENSES
                        </p>
                        <p style={{ fontStyle: "normal" }}>
                          <i>
                            <small>(HEMODIALYSIS SYSTEM)</small>
                          </i>
                        </p>
                        <br></br>
                        <br></br>
                      </div>
                    </div>

                    <p
                      style={{
                        textAlign: "left",
                        fontStyle: "normal",
                        fontWeight: "normal",
                      }}
                    >
                      <span>
                        {" "}
                        NAME &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; :{" "}
                        {data && data[0]?.beneficiary.middleName
                          ? data[0]?.beneficiary.lastName.toUpperCase() +
                            ", " +
                            data[0]?.beneficiary.firstName.toUpperCase() +
                            " " +
                            data[0]?.beneficiary.middleName?.toUpperCase()
                          : ""}
                      </span>
                      <span style={{ float: "right" }}>
                        Class &nbsp; &nbsp;:{" "}
                        {data && data[0]?.beneficiary.class.toUpperCase()}
                      </span>
                    </p>
                    <p
                      style={{
                        textAlign: "left",
                        fontStyle: "normal",
                        fontWeight: "normal",
                      }}
                    >
                      <span>
                        ADDRESS &nbsp; &nbsp;:{" "}
                        {data && data[0]?.beneficiary.address.toUpperCase()}
                      </span>
                    </p>
                    <p
                      style={{
                        textAlign: "left",
                        fontStyle: "normal",
                        fontWeight: "normal",
                      }}
                    >
                      {`(1) NUMBER OF PATIENT\'S SERVED `}
                    </p>

                    <p
                      style={{
                        textAlign: "left",
                        fontStyle: "normal",
                        fontWeight: "normal",
                      }}
                    >
                      (2) PHIC CASE RATE
                    </p>
                    <p
                      style={{
                        textAlign: "left",
                        fontStyle: "normal",
                        fontWeight: "normal",
                      }}
                    >
                      (3) Breakdown summary of expenses per siossion: Internal
                      Jugular Vein Central Venous Access and Permanent Catherer
                      Patient
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    style={{
                      border: "1px solid black",

                      textAlign: "center",
                    }}
                  >
                    <b>#</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      textAlign: "center",
                    }}
                  >
                    <b>ITEM NAME</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    <b>USED</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",

                      width: "15%",
                      textAlign: "center",
                    }}
                  >
                    <b>UNIT PRICE</b>
                  </td>
                  <td
                    style={{
                      border: "1px solid black",

                      width: "10%",
                      textAlign: "right",
                    }}
                  >
                    <b>AMOUNT</b>
                  </td>
                </tr>
              </thead>
              <tbody>
                {data &&
                  data[0]?.sessions_itemUsed.map((item: any, index) => (
                    <tr key={index}>
                      <td
                        style={{
                          border: "1px solid black",
                          padding: "8px",
                          textAlign: "center",
                        }}
                      >
                        {index + 1}
                      </td>
                      <td
                        style={{
                          border: "1px solid black",
                          padding: "8px",
                          textAlign: "left",
                        }}
                      >
                        {item.itemName}
                      </td>
                      <td
                        style={{
                          border: "1px solid black",
                          padding: "8px",
                          textAlign: "center",
                        }}
                      >
                        {item.quantity}
                      </td>
                      <td
                        style={{
                          border: "1px solid black",
                          padding: "8px",
                          textAlign: "center",
                        }}
                      >
                        {item.unitPrice} / {item.unitMeasure}
                      </td>
                      <td
                        style={{
                          border: "1px solid black",
                          padding: "8px",
                          textAlign: "right",
                        }}
                      >
                        <b>
                          {(item.unitPrice * item.quantity).toLocaleString(
                            undefined,
                            {
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2,
                            }
                          )}
                        </b>
                      </td>
                    </tr>
                  ))}
                <tr>
                  <td
                    style={{
                      textAlign: "center",
                    }}
                  ></td>
                  <td
                    style={{
                      textAlign: "center",
                    }}
                  ></td>
                  <td
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <b>TOTAL</b>
                  </td>
                  <td
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <i style={{ fontSize: "8px" }}>
                      <b>per session(actual expenses)</b>
                    </i>
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "right",
                    }}
                  >
                    <b>
                      ₱{" "}
                      {overallTotal?.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </b>
                  </td>
                </tr>
                <tr>
                  <td colSpan={5}>
                    <br></br>
                    <div style={divContainerStyle}>
                      <div style={divStyleLeft}>
                        <p style={{ marginTop: "5px" }}>Prepared by</p>
                      </div>

                      <div style={divStyleRight}>
                        <p style={{ marginTop: "5px" }}>Noted by</p>
                      </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </Card>
      </Col>
    </Row>
  );
};

export default UserManagement;
