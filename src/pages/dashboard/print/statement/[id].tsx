import { Button, Card, Row, Col } from "antd";
import { NextPage } from "next";
import { TagsOutlined } from "@ant-design/icons";
import { trpc } from "../../../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import moment from "moment";
import { FilterDropdownProps } from "antd/lib/table/interface";

type Props = {
  req: any;
  res: any;
  query: any;
};
export const getServerSideProps = async function ({ req, res, query }: Props) {
  const id = query.id;

  return {
    props: {
      id: id ?? null,
    },
  };
};

const UserManagement: NextPage = ({ id }: any) => {
  const [router_id, setID] = useState<string>(id);

  const session = useSession();

  const {
    data,
    isLoading: loading,
    refetch,
  } = trpc.useQuery(["taxdec.getStatementByCode", router_id]);

  console.log(data);
  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  // END INSERT

  const printDiv = () => {
    const divToPrint = document?.getElementById("printMe");

    if (!divToPrint) {
      return;
    }

    const printWindow = window.open("", "_blank", "height=800,width=1200");

    printWindow?.document.write(
      "<html><head><title>Print</title><style>table {font-size:12px;border-collapse:collapse;} p{margin:0} #deleteBtn {display:none}</style></head><body>"
    );

    // Clone the content to preserve the original content
    const clonedContent = divToPrint.cloneNode(true);
    printWindow?.document.body.appendChild(clonedContent);

    printWindow?.document.write("</body></html>");
    printWindow?.document.close();
    setTimeout(() => {
      printWindow?.print();
    }, 500);
  };

  function formatNumberExcel(number: any) {
    const decimal = number.toString().split(".")[1];

    console.log(number);
  }

  const totalOfAllTotalQuantity = data?.reduce(
    (total, currentItem) => total + (currentItem.totalAmount ?? 0),
    0
  );

  const totalSession = data?.reduce(
    (total, currentItem) => total + (currentItem.usedMedtronics ?? 0),
    0
  );

  const divContainerStyle: React.CSSProperties = {
    display: "flex",
    justifyContent: "space-between", // Distribute space between the two divs
    width: "100%", // Use the full width of the container
  };

  const divStyleLeft: React.CSSProperties = {
    borderTop: "2px solid black",
    width: "200px",
    textAlign: "center",
    marginTop: "50px",
  };

  const divStyleRight: React.CSSProperties = {
    borderTop: "2px solid black",
    width: "200px",
    textAlign: "center",
    marginTop: "50px",
    float: "right",
  };
  return (
    <Row gutter={[16, 16]}>
      <Col span={18}>
        <Card
          // title={
          //   <>
          //     <TagsOutlined /> &nbsp; SUPPLIES LEDGER CARD
          //   </>
          // }
          bordered={true}
        >
          {/* <div className="w-full mb-5" style={{backgroundColor:"#2c2b31", padding:"5px", textAlign:"center"}}>
                  <h1 style={{color:"white"}}>PURCHASE ORDER DETAILS</h1>
              </div> */}
          <Button className="bg-blue-600 text-white" onClick={printDiv}>
            Print
          </Button>
          <div id="printMe">
            <table
              style={{
                width: "100%",
                marginTop: "5px",
              }}
            >
              <thead>
                <tr>
                  <td
                    colSpan={5}
                    style={{
                      padding: "8px",
                      textAlign: "center",
                      paddingTop: "40px",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <div style={{ marginLeft: "-30px", marginTop: "-30px" }}>
                        {/* Your image */}
                        <img
                          src="/capitol.png"
                          alt="My Image"
                          width="100"
                          id="logo"
                        />
                      </div>

                      <div style={{ marginRight: "50px" }}>
                        {/* Your text */}
                        <p>REPUBLIC OF THE PHILIPPINES</p>
                        <p>PROVINCE OF CEBU</p>
                        {/* <p>{session.data?.user?.municipality.toUpperCase()}</p> */}
                        {/* <p style={{ fontSize: "5px" }}>Telephone.</p>
                    <p style={{ fontSize: "5px" }}>Email Address.</p> */}
                        <p style={{ fontWeight: "bold", fontSize: "14px" }}>
                          BILLING STATEMENT
                        </p>
                        <p style={{ fontStyle: "normal" }}>
                          <i>
                            <small>(HEMODIALYSIS SYSTEM)</small>
                          </i>
                        </p>
                        <br></br>
                        <br></br>
                      </div>
                    </div>
                    <p
                      style={{
                        textAlign: "left",
                        fontStyle: "normal",
                        fontWeight: "normal",
                      }}
                    >
                      <span>
                        {" "}
                        HOSPITAL &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; :{" "}
                        {data && data[0]?.hospital.toUpperCase()}
                      </span>
                      <span style={{ float: "right" }}>
                        Class &nbsp; &nbsp;:{" "}
                        {data?.[0]?.class?.toUpperCase() || ""}
                      </span>
                    </p>
                    <p
                      style={{
                        textAlign: "left",
                        fontStyle: "normal",
                        fontWeight: "normal",
                      }}
                    >
                      <span>
                        {" "}
                        STATEMENT DATE &nbsp;&nbsp;&nbsp;:{" "}
                        {data &&
                          moment(data[0]?.DateFrom).format("MMM DD")} -{" "}
                        {data && moment(data[0]?.DateTo).format("DD YYYY")}
                      </span>
                    </p>
                    <p
                      style={{
                        textAlign: "left",
                        fontStyle: "normal",
                        fontWeight: "normal",
                      }}
                    >
                      <span>
                        {" "}
                        STATEMENT CODE &nbsp;&nbsp;&nbsp;:{" "}
                        {data && data[0]?.statement_code.toUpperCase()}
                      </span>
                    </p>
                    {/* FROM {moment(startDate2).format("ll")} TO{" "}
                    {moment(endDate2).format("ll")} */}
                  </td>
                </tr>
                <tr>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                      width: "5%",
                    }}
                  >
                    #
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Name
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Used
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Unit Price
                  </td>
                  <td
                    style={{
                      border: "1px solid black",
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    Amount
                  </td>
                </tr>
              </thead>
              <tbody>
                {data?.map((item: any, index) => (
                  <tr key={index}>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                      }}
                    >
                      {index + 1}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "left",
                        width: "40%",
                      }}
                    >
                      {item.fullName.toUpperCase()}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                      }}
                    >
                      {item.usedMedtronics}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                      }}
                    >
                      ₱{" "}
                      {item.UnitPrice.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </td>
                    <td
                      style={{
                        border: "1px solid black",
                        padding: "8px",
                        textAlign: "center",
                      }}
                    >
                      ₱{" "}
                      {(item.usedMedtronics * item.UnitPrice).toLocaleString(
                        undefined,
                        {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        }
                      )}
                    </td>
                  </tr>
                ))}
                <tr>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  ></td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  ></td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    {totalSession}
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    TOTAL
                  </td>
                  <td
                    style={{
                      padding: "8px",
                      textAlign: "center",
                    }}
                  >
                    ₱{" "}
                    {(totalOfAllTotalQuantity
                      ? totalOfAllTotalQuantity
                      : 0
                    ).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colSpan={5} style={{ height: "96px" }}>
                    &nbsp;
                  </td>
                </tr>
              </tfoot>
              <tr style={{ border: "0" }}>
                <td colSpan={5}>
                  <br></br>
                  <div style={divContainerStyle}>
                    <div style={divStyleLeft}>
                      <p style={{ marginTop: "5px" }}>Clerk</p>
                    </div>

                    <div style={divStyleRight}>
                      <p style={{ marginTop: "5px" }}>HD Nurse</p>
                    </div>
                  </div>

                  <div style={divContainerStyle}>
                    <div style={divStyleLeft}>
                      <p style={{ marginTop: "5px" }}>Social Worker</p>
                    </div>
                    <div style={divStyleRight}>
                      <p style={{ marginTop: "5px" }}>Chief of Hospital</p>
                    </div>
                    <div style={divStyleLeft}>
                      <p style={{ marginTop: "5px" }}>Billing</p>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </Card>
      </Col>
    </Row>
  );
};

export default UserManagement;
