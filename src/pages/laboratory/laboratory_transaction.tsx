import {
  Button,
  Input,
  message,
  Form,
  Spin,
  Row,
  Col,
  Select,
  InputNumber,
  Card,
  Alert,
  Space,
  DatePicker,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
// import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  FileDoneOutlined,
  UserOutlined,
} from "@ant-design/icons";

// import TextArea from "antd/lib/input/TextArea";
import moment, { Moment } from "moment";

interface InsertPatientStatus {
  type: "success" | "error";
  text: string;
}

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality;
  const permission = session.data?.user?.permission;

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getHospital"]);

  const {
    data: data2,
    isLoading: loading2,
    refetch: refetch2,
  } = trpc.useQuery(["taxdec.getSensusPatient"]);

  const {
    data: data3,
    isLoading: loading3,
    refetch: refetch3,
  } = trpc.useQuery(["taxdec.getLaboratoryItems"]);

  // interface CustomFilterDropdownProps extends FilterDropdownProps {
  //   setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  // }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);
  const [alertVisible, setAlertVisible] = useState(true);

  // const handleEdit = (item: any) => {
  //   form2.setFieldsValue({
  //     ...item,
  //   });
  //   setopenUpdateItem(true);
  // };

  const [insertPatientStatus, setInsertPatientStatus] =
    useState<InsertPatientStatus | null>(null);

  const insertlabtransaction = trpc.useMutation("taxdec.insertlabtransaction", {
    onSuccess(data) {
      // setInsertPatientStatus({
      //   type: "success",
      //   text: "Record inserted successfully.",
      // });

      refetch();
      form.resetFields();
      form.setFieldsValue({
        list: [], // Replace this with the initial values or empty values for your 'form.list' fields
      });
      message.success("Record inserted successfully.");
      // setAlertVisible(true);
    },
    onError(error) {
      setInsertPatientStatus({ type: "error", text: error.message });
      message.error(`${error.message}`);
      // setAlertVisible(true);
    },
  });

  const handleAlertClose = () => {
    setAlertVisible(false);
  };

  // const handleUpdate2 = async (values: any) => {
  //   const selectedItemsWithAmounts = targetKeys.map(key => ({
  //     id: key,
  //     amount: itemAmounts[key],
  //   }));

  //   console.log(values);
  //   // try {
  //   //   insertlabtransaction.mutate({
  //   //     ...values,
  //   //   });
  //   // } catch (error) {
  //   //   // If there's an error during the mutation, handle it
  //   //   console.error(error);
  //   //   // You can add additional error handling logic here if needed
  //   // }
  // };

  const handSubmit = async (values: any) => {
    const formValues = form.getFieldsValue();

    // Check if 'list' property exists in formValues and form2Values and they are arrays
    if (!Array.isArray(formValues.list)) {
      console.error("Invalid data format. 'list' property should be an array.");
      return;
    }

    try {
      insertlabtransaction.mutate({
        laboratory_patients_id: formValues.laboratory_patients_id,
        patients_name: formValues.patients_name,
        classification: formValues.classification,
        bill_date: formValues.bill_date,
        lab_type: "LABORATORY",
        list: formValues.list,
        area: formValues.area,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
    // const combinedData = {
    //   list: formValues.list.map((item: any) => ({
    //     ...item,
    //     ...formValues, // Add properties from formValues to each item in the list
    //   })),
    // };

    console.log("formValues", formValues);

    // const updatePromises = combinedData.list.map((listItem: any) => {
    //   return updateStockout.mutateAsync({
    //     id: listItem.item_id, // Replace 'id' with the correct property from your 'listItem' object
    //     quantity: listItem.quantity, // Replace 'quantity2' with the correct property from your 'listItem' object
    //     unit_price: listItem.unit_price, // Replace 'unit_price' with the correct property from your 'listItem' object
    //     date_withdrew: listItem.date_withdrew, // Replace 'date_withdrew' with the correct property from your 'listItem' object
    //     transaction: "STOCKOUT",
    //     reference: listItem.reference, // Replace 'reference' with the correct property from your 'listItem' object
    //     received_by: listItem.received_by, // Replace 'received_by' with the correct property from your 'listItem' object
    //     ris: listItem.ris, // Replace 'ris' with the correct property from your 'listItem' object
    //   });
    // });

    // try {
    //   // Wait for all the promises to resolve using Promise.all()
    //   await Promise.all(updatePromises);
    //   message.success("Stock out successfully!");

    //   // form2.resetFields();
    //   form2.setFieldsValue({
    //     list: [], // Replace this with the initial values or empty values for your 'form.list' fields
    //   });
    //   form.resetFields();
    //   // setopenUpdateItem(false);
    // } catch (error) {
    //   // Type assertion to cast 'error' to the 'Error' type
    //   const errorObj = error as Error;

    //   if (errorObj.message) {
    //     message.error(errorObj.message);
    //   } else {
    //     message.error("An error occurred during the update.");
    //   }
    // }

    //   console.log(combinedData);
  };

  const setData = (value: any) => {
    const selectedObject2 = value
      ? data2?.find(item => item.id === value)
      : null;

    if (selectedObject2 && selectedObject2.id != null) {
      const middleName = selectedObject2.middleName
        ? selectedObject2.middleName
        : "";

      const newItem = {
        classification: selectedObject2.classification,
        laboratory_patients_id: selectedObject2.id,
        patients_name:
          selectedObject2.lastName +
          ", " +
          selectedObject2.firstName +
          " " +
          middleName,
      };

      form.setFieldsValue(newItem);
    } else {
      // Handle the case when selectedObject2 is null or undefined
      console.error("selectedObject2 is null or undefined");
    }

    console.log(value);
  };

  const setData3 = (value: any) => {
    const selectedObject2 = value
      ? data3?.find(item => item.id === value)
      : null;

    if (selectedObject2 && selectedObject2.id != null) {
      const newItem = {
        equipment_used: selectedObject2.name,
        bill_amount: selectedObject2.amount,
      };

      form.setFieldsValue(newItem);
    } else {
      // Handle the case when selectedObject2 is null or undefined
      console.error("selectedObject2 is null or undefined");
    }

    console.log(value);
  };

  //TRANSFER
  const [targetKeys, setTargetKeys] = useState<string[]>([]);
  const [targetKeysWithAmounts, setTargetKeysWithAmounts] = useState<string[]>(
    []
  );
  const [searchValue, setSearchValue] = useState("");
  const [itemAmounts, setItemAmounts] = useState<{ [key: string]: number }>({}); // State to store amount for each item

  const handleChange = (
    selectedKeys: React.Key[],
    direction: "left" | "right",
    moveKeys: React.Key[]
  ) => {
    if (direction === "right") {
      setTargetKeys(prevKeys => [...prevKeys, ...(selectedKeys as string[])]);
      setTargetKeysWithAmounts(prevKeys => [
        ...prevKeys,
        ...(selectedKeys as string[]),
      ]);
    } else {
      setTargetKeys(prevKeys =>
        prevKeys.filter(key => !moveKeys.includes(key))
      );
      setTargetKeysWithAmounts(prevKeys =>
        prevKeys.filter(key => !moveKeys.includes(key))
      );
    }
  };

  const transferData =
    data3?.map(item => ({
      key: item.id, // Assuming id is unique
      title: item.name,
      amount: item.amount,
    })) || [];

  const handleSearch = (direction: any, value: any) => {
    setSearchValue(value);
  };

  const filterOption = (inputValue: any, option: any) => {
    return option.title.toLowerCase().includes(inputValue.toLowerCase());
  };

  const [list, setList] = useState([]);
  const [selectedValue, setSelectedValue] = useState<any[]>([]);
  // State to store the total amount
  const [totalAmount, setTotalAmount] = useState(0);

  const handleSelectChangeAdd = (value: any) => {
    const selectedObject2 = value
      ? data3?.find(item => item.id === value)
      : null;

    const newItem = {
      name: selectedObject2 ? selectedObject2.name : "",
      amount: selectedObject2 ? selectedObject2.amount : 0,
      item_id: selectedObject2 ? selectedObject2.id : "",
    };
    setSelectedValue([newItem]);
  };

  const handleAdd = (data3: any) => {
    const values = form.getFieldsValue();
    const { list } = values;
    // const { list: currentList, ...restValues } = values;

    const selectedItemId = data3?.find(
      (item: any) => item.id === selectedItemId
    );
    // const selectedItem = data2?.find((item) => item.id === currentList[0]?.id)

    // console.log("selectedValue: ", selectedValue[0]);
    // console.log(list);
    const newItem = {
      name: selectedValue ? selectedValue[0].name : "",
      amount: selectedValue ? selectedValue[0].amount : 0,
      item_id: selectedValue ? selectedValue[0].item_id : "",
    };

    // console.log("newItem: ", newItem);
    if (list) {
      form.setFieldsValue({
        list: [...list, newItem],
        // list : [ newItem],
      });
    } else {
      form.setFieldsValue({
        // list: [...list, newItem],
        list: [newItem],
      });
    }

    const listValues = form.getFieldValue("list");

    const totalAmount = listValues.reduce(
      (total: any, item: any) => total + (item.amount || 0),
      0
    );

    setTotalAmount(totalAmount);
  };

  const removeUpdate = () => {
    const listValues = form.getFieldValue("list");

    const totalAmount = listValues.reduce(
      (total: any, item: any) => total + (item.amount || 0),
      0
    );

    setTotalAmount(totalAmount);
  };
  // Calculate the total amount

  // Function to check if the list is empty
  const isListEmpty = () => {
    const listValue = form.getFieldValue("list");
    return !listValue || listValue.length === 0;
  };

  return (
    <div>
      <Form
        form={form}
        layout="vertical"
        onFinish={handSubmit}
        className="w-full"
        autoComplete="off"
      >
        <Row gutter={[12, 12]}>
          <Col md={8} sm={12}>
            <Card>
              {/* <div
              style={{
                maxWidth: "400px",
                margin: "0",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
              }}
            > */}
              <div className="flex p-2 bg-custom-blue mb-5 rounded w-full justify-center">
                <span className="uppercase text-2xl font-bold text-white">
                  LAB TRANSACTION
                </span>
              </div>

              <Row gutter={12}>
                <Col span={24}>
                  <Form.Item
                    label="Select Patient"
                    required
                    name="patients_name2"
                    rules={[
                      {
                        required: true,
                        message: "Name is required",
                      },
                    ]}
                  >
                    <Select
                      showSearch
                      onChange={setData}
                      optionFilterProp="children"
                      filterOption={(input: any, option: any) => {
                        const optionText =
                          option.children && option.children.props
                            ? option.children.props.children
                            : "";
                        return (
                          optionText
                            .toString()
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        );
                      }}
                    >
                      {data2 &&
                        data2.map(data => (
                          <Select.Option value={data.id} key={data.id}>
                            {/* Your JSX code */}
                            <div>
                              {data.firstName} {data.middleName} {data.lastName}
                            </div>
                          </Select.Option>
                        ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={24} style={{ display: "none" }}>
                  <Form.Item
                    name="patients_name"
                    label="Name"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Classification is required",
                      },
                    ]}
                  >
                    <Input disabled />
                  </Form.Item>
                </Col>
                <Col span={12} style={{ display: "none" }}>
                  <Form.Item
                    name="laboratory_patients_id"
                    label="ID"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Classification is required",
                      },
                    ]}
                  >
                    <Input disabled />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name="classification"
                    label="Classification"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Classification is required",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value="Indigent" key="Indigent">
                        Indigent
                      </Select.Option>
                      <Select.Option value="Paying" key="Paying">
                        Paying
                      </Select.Option>
                      <Select.Option
                        value="Non Philhealth"
                        key="Non Philhealth"
                      >
                        Non Philhealth
                      </Select.Option>
                      <Select.Option value="Philhealth" key="Philhealth">
                        Philhealth
                      </Select.Option>
                    </Select>
                    {/* <Input disabled /> */}
                  </Form.Item>
                </Col>

                <Col span={12}>
                  <Form.Item
                    name="bill_date"
                    label="Date "
                    required
                    rules={[
                      {
                        required: true,
                        message: "Date is required",
                      },
                    ]}
                    initialValue={moment()}
                  >
                    <DatePicker format="YYYY-MM-DD" className="w-full" />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    name="area"
                    label="Area"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Area is required",
                      },
                    ]}
                    initialValue={"DISCHARGE"}
                  >
                    <Select>
                      <Select.Option value="DISCHARGE" key="DISCHARGE">
                        DISCHARGE
                      </Select.Option>
                      <Select.Option value="OPD" key="OPD">
                        OPD
                      </Select.Option>
                      <Select.Option value="EMERGENCY" key="EMERGENCY">
                        EMERGENCY
                      </Select.Option>
                      <Select.Option value="ADMITTED" key="ADMITTED">
                        ADMITTED
                      </Select.Option>
                      <Select.Option value="WALK-IN" key="WALK-IN">
                        WALK-IN
                      </Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item label="Select Procedure ">
                    <Select
                      showSearch
                      optionFilterProp="children"
                      onChange={handleSelectChangeAdd}
                      style={{
                        width: "70%",
                      }}
                      filterOption={(input: any, option: any) => {
                        const optionText =
                          option.children && option.children.props
                            ? option.children.props.children
                            : "";
                        return (
                          optionText
                            .toString()
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        );
                      }}
                    >
                      {data3 &&
                        data3.map(data => (
                          <Select.Option value={data.id} key={data.id}>
                            {/* Your JSX code */}
                            <div>{data.name}</div>
                          </Select.Option>
                        ))}
                    </Select>
                    <Button
                      type="dashed"
                      onClick={() => handleAdd(data3)}
                      className="ml-2"
                    >
                      Add
                    </Button>
                  </Form.Item>
                </Col>
              </Row>

              {/* </div> */}
            </Card>
          </Col>
          <Col md={16} sm={24}>
            <Card>
              <div className="flex p-2 bg-custom-blue mb-5 rounded w-full justify-center">
                <span className="uppercase text-2xl font-bold text-white ">
                  PROCEDURE USED
                </span>
              </div>

              <Row gutter={12}>
                <Col span={24} style={{ height: "400px", overflow: "scroll" }}>
                  <table
                    style={{
                      width: "100%",
                      border: "1px solid black",
                    }}
                  >
                    <tr>
                      <th className="border-solid border-2 border-custom-blue text-center w-fit bg-custom-blue text-white">
                        #
                      </th>
                      <th className="border-solid border-2 border-custom-blue text-center w-fit bg-custom-blue text-white">
                        Description/Particulars
                      </th>
                      <th className="border-solid border-2 border-custom-blue text-center w-fit bg-custom-blue text-white">
                        Quantity
                      </th>
                      <th className="border-solid border-2 border-custom-blue text-center w-fit bg-custom-blue text-white">
                        Unit Price
                      </th>
                      <th className="border-solid border-2 border-custom-blue text-center w-fit bg-custom-blue text-white">
                        Amount
                      </th>
                      <th className="border-solid border-2 border-custom-blue text-center w-fit bg-custom-blue text-white">
                        Action
                      </th>
                    </tr>

                    <Form.List name="list">
                      {(fields, { add, remove }) => {
                        fields.reverse();
                        return (
                          <>
                            {fields.map((field, index) => {
                              // Get the value of the list field from the form
                              const listValues = form.getFieldValue("list");
                              // Find the corresponding item in the list using the index
                              const currentItem = listValues[index];

                              return (
                                <tr key={field.key}>
                                  <td
                                    className="border-solid border-2 border-custom-blue text-center p-4"
                                    style={{ width: "20px" }}
                                  >
                                    {index + 1}
                                    <Form.Item
                                      name={[field.name, "item_id"]}
                                      label="Name"
                                      rules={[
                                        {
                                          required: true,
                                          message: "Name is required",
                                        },
                                      ]}
                                      hidden
                                    >
                                      <InputNumber
                                        className="w-full"
                                        // formatter={formatter}
                                        // parser={parser}
                                      />
                                    </Form.Item>
                                  </td>
                                  <td
                                    className="border-solid border-2 border-custom-blue px-4"
                                    style={{ width: "400px" }}
                                  >
                                    <Form.Item
                                      name={[field.name, "name"]}
                                      label="Name"
                                      rules={[
                                        {
                                          required: true,
                                          message: "Name is required",
                                        },
                                      ]}
                                      hidden
                                    >
                                      <Input
                                        placeholder="Enter name"
                                        readOnly
                                        style={{ backgroundColor: "#f0f0f0" }}
                                      />
                                    </Form.Item>
                                    {currentItem && currentItem.name}
                                  </td>
                                  <td
                                    className="border-solid border-2 border-custom-blue text-center"
                                    style={{ width: "20px" }}
                                  >
                                    1
                                  </td>
                                  <td
                                    className="border-solid border-2 border-custom-blue text-center"
                                    style={{ width: "100px" }}
                                  >
                                    <Form.Item
                                      name={[field.name, "amount"]}
                                      label="Amount"
                                      style={{ display: "none" }}
                                      initialValue={0}
                                      rules={[
                                        {
                                          required: true,
                                          message: "Quantity is required",
                                        },
                                      ]}
                                    >
                                      <InputNumber
                                        className="w-full"
                                        // formatter={formatter}
                                        // parser={parser}
                                      />
                                    </Form.Item>
                                    {currentItem &&
                                      currentItem.amount.toLocaleString(
                                        undefined,
                                        {
                                          minimumFractionDigits: 2,
                                          maximumFractionDigits: 2,
                                        }
                                      )}
                                  </td>
                                  <td
                                    className="border-solid border-2 border-custom-blue text-center"
                                    style={{ width: "100px" }}
                                  >
                                    {currentItem &&
                                      currentItem.amount.toLocaleString(
                                        undefined,
                                        {
                                          minimumFractionDigits: 2,
                                          maximumFractionDigits: 2,
                                        }
                                      )}
                                  </td>
                                  <td
                                    className="border-solid border-2 border-custom-blue text-center"
                                    style={{ width: "100px" }}
                                  >
                                    <Button
                                      style={{
                                        backgroundColor: "#ff4d4f",
                                        color: "white",
                                      }}
                                      type="dashed"
                                      onClick={() => {
                                        remove(field.name);
                                        removeUpdate();
                                      }}
                                    >
                                      Remove
                                    </Button>
                                  </td>
                                </tr>
                              );
                            })}
                          </>
                        );
                      }}
                    </Form.List>
                  </table>
                </Col>
                <Col
                  span={24}
                  style={{ marginTop: "20px" }}
                  className="flex justify-between"
                >
                  <Button
                    disabled={isListEmpty()}
                    htmlType="submit"
                    type="primary"
                    style={{ width: "200px" }}
                    // loading={insertPatient.isLoading}
                    className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline p-8 flex justify-center items-center text-2xl font-bold"
                  >
                    {insertlabtransaction.isLoading ? (
                      <>
                        <Spin /> <span>Please wait..</span>
                      </>
                    ) : (
                      `SAVE `
                    )}
                    {/* SAVE */}
                  </Button>
                  <div
                    className="bg-custom-blue rounded  font-bold text-2xl text-white"
                    style={{
                      width: "200px",
                      alignItems: "center",
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    ₱{" "}
                    {totalAmount.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </div>
                </Col>
              </Row>

              {/* </div> */}
            </Card>
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default PendingTaxDeclaration;
