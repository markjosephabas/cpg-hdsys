import {
  Button,
  Table,
  Tooltip,
  Input,
  message,
  Modal,
  Form,
  Spin,
  Row,
  Col,
  Tag,
  Select,
  InputNumber,
  Card,
  Alert,
  Space,
  Divider,
} from "antd";
import { NextPage } from "next";
// import DashboardLayout from "../../dashboard-layout";

import { trpc } from "../../utils/trpc";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { FilterDropdownProps } from "antd/lib/table/interface";
import {
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  FileDoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { BiUser } from "react-icons/bi";

interface InsertPatientStatus {
  type: "success" | "error";
  text: string;
}

const PendingTaxDeclaration: NextPage = () => {
  const session = useSession();
  const [openUpdateItem2, setopenUpdateItem2] = useState(false);
  const [txtmessage, setMessage] = useState("");
  const hospital = session.data?.user?.municipality;
  const permission = session.data?.user?.permission;

  const {
    data: data,
    isLoading: loading,
    refetch: refetch,
  } = trpc.useQuery(["taxdec.getHospital"]);

  interface CustomFilterDropdownProps extends FilterDropdownProps {
    setSelectedKeys: (selectedKeys: (string | number)[]) => void;
  }

  const [form] = Form.useForm();
  const [form2] = Form.useForm();

  const [openUpdateItem, setopenUpdateItem] = useState(false);
  const [alertVisible, setAlertVisible] = useState(true);

  const updateHospital = trpc.useMutation("taxdec.updateHospital", {
    onSuccess(data) {
      refetch();
      form2.resetFields();
      setopenUpdateItem(false);
      message.success("Updated susccessfully");
    },
    onError(error) {
      message.error(`${error.message}`);
    },
  });

  const handleEdit = (item: any) => {
    form2.setFieldsValue({
      ...item,
    });
    setopenUpdateItem(true);
  };

  const [insertPatientStatus, setInsertPatientStatus] =
    useState<InsertPatientStatus | null>(null);

  const insertLabItems = trpc.useMutation("taxdec.insertLabItems", {
    onSuccess(data) {
      setInsertPatientStatus({
        type: "success",
        text: "Item inserted successfully.",
      });
      refetch();
      form.resetFields();
      setAlertVisible(true);
    },
    onError(error) {
      setInsertPatientStatus({ type: "error", text: error.message });
      message.error(`${error.message}`);
      setAlertVisible(true);
    },
  });

  const handleAlertClose = () => {
    setInsertPatientStatus(null);
  };

  useEffect(() => {
    if (insertPatientStatus && insertPatientStatus.text !== "") {
      // Automatically close the alert after 3 seconds
      const timer = setTimeout(() => {
        handleAlertClose(); // Close the alert
      }, 3000);

      // Clear the timer if the component unmounts or insertPatientStatus changes
      return () => clearTimeout(timer);
    }
  }, [insertPatientStatus]);

  const handleUpdate2 = async (values: any) => {
    try {
      insertLabItems.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  const handleUpdate = async (values: any) => {
    try {
      updateHospital.mutate({
        ...values,
      });
    } catch (error) {
      // If there's an error during the mutation, handle it
      console.error(error);
      // You can add additional error handling logic here if needed
    }
  };

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Row>
        <Col>
          <Card
            style={{
              width: "400px",
            }}
            title={
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  gap: 5,
                }}
              >
                <UserOutlined size={20} />
                <p>LAB ITEMS REGISTRATION</p>
              </div>
            }
          >
            <Form
              form={form}
              layout="vertical"
              onFinish={handleUpdate2}
              className="w-full"
              autoComplete="off"
            >
              <Row gutter={12}>
                <Col span={24}>
                  <Form.Item
                    name="name"
                    label="Item name"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Last Name is required",
                      },
                    ]}
                  >
                    <Input placeholder="Last Name" />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    name="type"
                    label="Type"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Type is required",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option
                        value="CLINICAL MICROSCOPY"
                        key="CLINICAL MICROSCOPY"
                      >
                        CLINICAL MICROSCOPY
                      </Select.Option>
                      <Select.Option value="HEMATOLOGY" key="HEMATOLOGY">
                        HEMATOLOGY
                      </Select.Option>
                      <Select.Option value="BLOOD BANK" key="BLOOD BANK">
                        BLOOD BANK
                      </Select.Option>
                      <Select.Option value="IMMUNOLOGY" key="IMMUNOLOGY">
                        IMMUNOLOGY
                      </Select.Option>
                      <Select.Option
                        value="CLINICAL CHEMISTRY"
                        key="CLINICAL CHEMISTRY"
                      >
                        CLINICAL CHEMISTRY
                      </Select.Option>
                      <Select.Option value="MICRO BIOLOGY" key="MICRO BIOLOGY">
                        MICRO BIOLOGY
                      </Select.Option>
                      <Select.Option
                        value="HISTOPATHOLOGY"
                        key="HISTOPATHOLOGY"
                      >
                        HISTOPATHOLOGY
                      </Select.Option>
                      <Select.Option value="LABORATORY" key="LABORATORY">
                        LABORATORY
                      </Select.Option>
                      <Select.Option value="RADIOLOGY" key="RADIOLOGY">
                        RADIOLOGY
                      </Select.Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    name="amount"
                    label="Cost"
                    required
                    rules={[
                      {
                        required: true,
                        message: "Cost is required",
                      },
                    ]}
                  >
                    <InputNumber
                      min={0}
                      style={{ width: "100%" }}
                      formatter={value =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                    />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  {insertPatientStatus && alertVisible && (
                    <Alert
                      message={insertPatientStatus.text}
                      type={
                        insertPatientStatus.type === "success"
                          ? "success"
                          : "error"
                      }
                      showIcon
                      closable
                      onClose={handleAlertClose}
                    />
                  )}
                </Col>
                <Col
                  span={24}
                  style={{ marginTop: "20px", textAlign: "center" }}
                >
                  <Button
                    // disabled={insertPatient.isLoading}
                    htmlType="submit"
                    type="primary"
                    // loading={insertPatient.isLoading}
                    className="bg-blue-500 hover:bg-blue-700 text-white rounded focus:outline-none focus:shadow-outline"
                  >
                    {insertLabItems.isLoading ? (
                      <>
                        <Spin /> <span>Please wait..</span>
                      </>
                    ) : (
                      "SAVE"
                    )}
                    {/* SAVE */}
                  </Button>
                </Col>
              </Row>
            </Form>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default PendingTaxDeclaration;
