import { transaction } from "./../../../utils/atoms/transaction";
import { createProtectedRouter } from "../protected-router";
import { Prisma } from "@prisma/client";
import z from "zod";
// import z, { number } from "zod";
// import { hash } from "bcryptjs";
// import { PrismaClient } from "@prisma/client";
// import { transformDocument } from "@prisma/client/runtime";

export const taxdec = createProtectedRouter()
  .mutation("register", {
    input: z.object({
      username: z.string(),
      password: z.string(),
      lastname: z.string().nullish(),
      firstname: z.string().nullish(),
      middlename: z.string().nullish(),
      municipality: z.string(),
      permission: z.string(),
      newPass: z.string(),
    }),
    async resolve({ ctx, input }) {
      const existingUser = await ctx.prisma.system_users.findUnique({
        where: {
          username: input.username,
        },
      });

      if (existingUser) {
        throw new Error(
          `Sorry, the username "${input.username}" is already taken. Please choose a different username and try again.`
        );
      }

      return await ctx.prisma.system_users.create({
        data: {
          username: input.username,
          password: input.newPass,
          lastname: input.lastname,
          firstname: input.firstname,
          middlename: input.middlename,
          municipality: input.municipality,
          permission: input.permission,
        },
      });
    },
  })
  .query("getUser", {
    async resolve({ ctx }) {
      return await ctx.prisma.system_users.findMany();
    },
  })
  .mutation("updateuser", {
    input: z.object({
      id: z.number(),
      lastname: z.string().nullish(),
      firstname: z.string().nullish(),
      middlename: z.string().nullish(),
      municipality: z.string(),
      permission: z.string(),
    }),
    async resolve({ input, ctx }) {
      console.log("Submitted", input);
      return await ctx.prisma.system_users.update({
        where: {
          id: input.id,
        },
        data: {
          lastname: input.lastname,
          firstname: input.firstname,
          middlename: input.middlename,
          municipality: input.municipality,
          permission: input.permission,
        },
      });
    },
  })
  .mutation("changePassword", {
    input: z.object({
      id: z.number(),
      newPass: z.string(),
    }),
    async resolve({ input, ctx }) {
      console.log("Submitted", input);
      return await ctx.prisma.system_users.update({
        where: {
          id: input.id,
        },
        data: {
          password: input.newPass,
        },
      });
    },
  })
  .mutation("deleteUser", {
    input: z.object({
      id: z.number(),
    }),
    async resolve({ input, ctx }) {
      console.log("Submitted", input);
      return await ctx.prisma.system_users.delete({
        where: {
          id: input.id,
        },
      });
    },
  })
  .mutation("register_beneficiary", {
    input: z.object({
      firstName: z.string(),
      lastName: z.string(),
      middleName: z.string().nullish(),
      gender: z.string(),
      address: z.string(),
      class: z.string(),
      hospital: z.string(),
    }),
    async resolve({ ctx, input }) {
      return await ctx.prisma.beneficiary.create({
        data: {
          firstName: input.firstName,
          lastName: input.lastName,
          middleName: input.middleName,
          gender: input.gender,
          address: input.address,
          class: input.class,
          hospital: input.hospital,
          status: "Active",
        },
      });
    },
  })
  .mutation("updateBeneficiary", {
    input: z.object({
      id: z.number(),
      firstName: z.string(),
      lastName: z.string(),
      middleName: z.string().nullish(),
      gender: z.string(),
      address: z.string(),
      class: z.string(),
      hospital: z.string(),
      status: z.string(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.beneficiary.update({
        where: {
          id: input.id,
        },
        data: {
          firstName: input.firstName,
          lastName: input.lastName,
          middleName: input.middleName,
          gender: input.gender,
          address: input.address,
          class: input.class,
          hospital: input.hospital,
          status: input.status,
        },
      });
    },
  })

  .query("getBenefiary", {
    input: z.string(),
    async resolve({ ctx, input }) {
      return await ctx.prisma.beneficiary.findMany({
        where: {
          hospital: input,
        },
        orderBy: {
          id: "asc",
        },
      });
    },
  })
  .query("getBenefiaryActive", {
    input: z.string(),
    async resolve({ ctx, input }) {
      return await ctx.prisma.beneficiary.findMany({
        where: {
          hospital: input,
          status: "Active",
        },
        orderBy: {
          id: "asc",
        },
      });
    },
  })
  .query("getBenefiaryAll", {
    async resolve({ ctx, input }) {
      return await ctx.prisma.beneficiary.findMany({
        orderBy: {
          id: "asc",
        },
      });
    },
  })
  .mutation("insertItems", {
    input: z.object({
      itemName: z.string(),
      unitMeasure: z.string(),
      unitPrice: z.number(),
      hospital: z.string(),
    }),
    async resolve({ ctx, input }) {
      return await ctx.prisma.items.create({
        data: {
          itemName: input.itemName,
          unitMeasure: input.unitMeasure,
          unitPrice: input.unitPrice,
          hospital: input.hospital,
        },
      });
    },
  })
  .mutation("insertHospital", {
    input: z.object({
      hospitalName: z.string(),
    }),
    async resolve({ ctx, input }) {
      return await ctx.prisma.hospital.create({
        data: {
          hospitalName: input.hospitalName,
        },
      });
    },
  })
  .mutation("updateItems", {
    input: z.object({
      id: z.number(),
      itemName: z.string(),
      unitMeasure: z.string(),
      unitPrice: z.number(),
      status: z.string(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.items.update({
        where: {
          id: input.id,
        },
        data: {
          itemName: input.itemName,
          unitMeasure: input.unitMeasure,
          unitPrice: input.unitPrice,
          status: input.status,
        },
      });
    },
  })
  .mutation("updateSessionDeduction", {
    input: z.object({
      id: z.number(),
      philhealth_deduction: z.number(),
      aics_deduction: z.number(),
      maep_deduction: z.number(),
      cash: z.number(),
      qfs: z.number(),
      pcso: z.number(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.sessions.update({
        where: {
          id: input.id,
        },
        data: {
          philhealth_deduction: input.philhealth_deduction,
          aics_deduction: input.aics_deduction,
          maep_deduction: input.maep_deduction,
          cash: input.cash,
          qfs: input.qfs,
          pcso: input.pcso,
        },
      });
    },
  })
  .mutation("updateHospital", {
    input: z.object({
      id: z.number(),
      hospitalName: z.string(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.hospital.update({
        where: {
          id: input.id,
        },
        data: {
          hospitalName: input.hospitalName,
        },
      });
    },
  })
  .mutation("insertSession", {
    input: z.object({
      benefeciary: z.number(),
      itemDetails: z.array(
        z.object({
          itemName: z.string(),
          unitMeasure: z.string(),
          unitPrice: z.number(),
          quantity: z.number(),
          isMedtronics: z.number(),
        })
      ),
      hospital: z.string(),
    }),
    async resolve({ ctx, input }) {
      //  return await ctx.prisma.sessions.create({
      //   data: {
      //     beneficiary_id: input.benefeciary,
      //     hospital: input.hospital,
      //   },
      // })
      const insertData = await ctx.prisma.sessions.create({
        data: {
          beneficiary_id: input.benefeciary,
          hospital: input.hospital,
        },
      });

      // return insertData.id
      for (const data of input.itemDetails) {
        await ctx.prisma.sessions_itemUsed.create({
          data: {
            sessions_id: insertData.id,
            itemName: data.itemName,
            unitMeasure: data.unitMeasure,
            unitPrice: data.unitPrice,
            quantity: data.quantity,
            isMedtronics: data.isMedtronics,
          },
        });
      }
    },
  })
  .mutation("insertStatement", {
    input: z.object({
      startDate: z.string(),
      endDate: z.string(),
      class: z.string(),
      data: z.array(
        z.object({
          firstName: z.string(),
          lastName: z.string(),
          middleName: z.string().nullish(),
          totalAmount: z.number(),
          totalQuantity: z.number(),
          unitPrice: z.number(),
          hospital: z.string(),
        })
      ),
    }),
    async resolve({ ctx, input }) {
      const lastStatement = await ctx.prisma.statement.findFirst({
        orderBy: { id: "desc" },
      });

      // Extract the numeric part from the statement_code and increment by 1
      const lastCodeNumber = lastStatement
        ? parseInt((lastStatement.statement_code?.match(/\d+/) || [])[0]!, 10)
        : 0;
      const newCodeNumber = lastCodeNumber + 1;
      // Generate the new statement code with leading zeros
      const newStatementCode = `STATEMENT-${newCodeNumber
        .toString()
        .padStart(5, "0")}`;
      const currentDate = new Date();

      for (const data of input.data) {
        const fullName = `${data.lastName ?? ""}, ${data.firstName ?? ""} ${
          data.middleName ?? ""
        }`;

        await ctx.prisma.statement.create({
          data: {
            statement_code: newStatementCode,
            fullName: fullName,
            hospital: data.hospital,
            usedMedtronics: data.totalQuantity,
            UnitPrice: data.unitPrice,
            totalAmount: data.totalAmount,
            class: input.class,
            DateFrom: new Date(input.startDate),
            DateTo: new Date(input.endDate),
            created_at: currentDate,
          },
        });
      }
    },
  })
  .query("getItemsActive", {
    input: z.string(),
    async resolve({ ctx, input }) {
      return await ctx.prisma.items.findMany({
        where: {
          AND: [
            {
              OR: [{ hospital: input }, { hospital: "Cebu Province" }],
            },
            { status: "Active" },
          ],
        },
        orderBy: {
          itemName: "asc",
        },
      });
    },
  })

  .query("getSessionTransaction", {
    input: z.string(),
    async resolve({ ctx, input }) {
      return await ctx.prisma.sessions.findMany({
        where: {
          hospital: input,
        },
        orderBy: {
          id: "desc",
        },
        select: {
          id: true,
          beneficiary_id: true,
          hospital: true,
          is_deleted: true,
          created_at: true,
          philhealth_deduction: true,
          aics_deduction: true,
          maep_deduction: true,
          cash: true,
          qfs: true,
          pcso: true,
          beneficiary: {
            select: {
              firstName: true,
              lastName: true,
              middleName: true,
              address: true,
              gender: true,
              class: true,
              status: true,
            },
          },
          sessions_itemUsed: {
            select: {
              unitPrice: true,
              quantity: true,
              itemName: true,
              unitMeasure: true,
            },
          },
        },
      });
    },
  })
  .query("getStatementByHospital", {
    input: z.string(),
    async resolve({ ctx, input }) {
      return await ctx.prisma.statement.groupBy({
        by: [
          "hospital",
          "DateFrom",
          "DateTo",
          "statement_code",
          "created_at",
          "class",
        ],
        where: {
          hospital: input,
        },
        orderBy: {
          statement_code: "desc",
        },
        _sum: {
          totalAmount: true,
        },
      });
    },
  })
  .query("getStatementAdmin", {
    async resolve({ ctx, input }) {
      return await ctx.prisma.statement.groupBy({
        by: [
          "hospital",
          "DateFrom",
          "DateTo",
          "statement_code",
          "created_at",
          "class",
        ],

        orderBy: {
          statement_code: "desc",
        },
        _sum: {
          totalAmount: true,
        },
      });
    },
  })
  .query("getStatementByCode", {
    input: z.string(),
    async resolve({ ctx, input }) {
      return await ctx.prisma.statement.findMany({
        where: {
          statement_code: input,
        },
      });
    },
  })
  .query("getStatementSummary", {
    input: z.string(),
    async resolve({ ctx, input }) {
      const lastStatement = await ctx.prisma.statement.findFirst({
        where: {
          statement_code: input,
        },
      });

      // Check if lastStatement is defined
      if (lastStatement) {
        const dateFrom = lastStatement.DateFrom;
        const dateTo = lastStatement.DateTo; // Corrected from DateFrom to DateTo

        // Check if dateFrom and dateTo are defined before creating Date objects
        if (dateFrom && dateTo) {
          const startDate = new Date(dateFrom);
          const endDate = new Date(dateTo);
          startDate.setHours(0, 0, 0, 0); // Set to midnight
          endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

          const groupedSessions = await ctx.prisma.sessions.groupBy({
            by: ["beneficiary_id"],
            where: {
              is_deleted: 0,
              hospital: lastStatement.hospital,
              created_at: {
                gte: startDate,
                lte: endDate,
              },
              beneficiary: {
                class:
                  lastStatement.class === "Beneficiary (Indigent/POS/PWD)"
                    ? {
                        in: ["Indigent", "POS", "PWD"],
                      }
                    : lastStatement.class === "Indigent"
                    ? "Indigent"
                    : lastStatement.class === "POS"
                    ? "POS"
                    : lastStatement.class === "PWD"
                    ? "PWD"
                    : "Paying",
              },
            },
            _sum: {
              cash: true,
              philhealth_deduction: true,
              qfs: true,
              aics_deduction: true,
              maep_deduction: true,
              pcso: true,
            },
            _count: {
              beneficiary_id: true,
            },
            orderBy: {
              beneficiary_id: "asc",
            },
          });

          // Fetch beneficiaries' last names separately
          const beneficiaryIds = groupedSessions.map(
            group => group.beneficiary_id
          );
          const beneficiaries = await ctx.prisma.beneficiary.findMany({
            where: {
              id: {
                in: beneficiaryIds,
              },
            },
            select: {
              id: true,
              lastName: true,
              firstName: true,
              middleName: true,
            },
          });

          // Combine the grouped sessions with beneficiaries' last names
          const groupedSessionsWithNames = groupedSessions.map(group => ({
            beneficiary_id: group.beneficiary_id,
            lastName:
              beneficiaries.find(b => b.id === group.beneficiary_id)
                ?.lastName || null,
            firstName:
              beneficiaries.find(b => b.id === group.beneficiary_id)
                ?.firstName || null,
            middleName:
              beneficiaries.find(b => b.id === group.beneficiary_id)
                ?.middleName || null,
            sessionCount: group._count || 0,
            sum: {
              cash: group._sum.cash || 0,
              philhealth_deduction: group._sum.philhealth_deduction || 0,
              qfs: group._sum.qfs || 0,
              aics_deduction: group._sum.aics_deduction || 0,
              maep_deduction: group._sum.maep_deduction || 0,
              pcso: group._sum.pcso || 0,
            },
          }));

          // Sort the results by lastName
          groupedSessionsWithNames.sort((a, b) => {
            if (a.lastName && b.lastName) {
              return a.lastName.localeCompare(b.lastName);
            } else if (!a.lastName && b.lastName) {
              return 1;
            } else if (a.lastName && !b.lastName) {
              return -1;
            } else {
              return 0;
            }
          });
          // console.log(groupedSessionsWithNames);
          return groupedSessionsWithNames;
          // return await ctx.prisma.sessions.groupBy({
          //   by: ['beneficiary_id'],
          //   where: {
          //     is_deleted: 0,
          //     hospital: lastStatement.hospital,
          //     created_at: {
          //       gte: startDate,
          //       lte: endDate,
          //     },
          //     beneficiary:{
          //       class: lastStatement.class === "Beneficiary (Indigent/POS/PWD)" ? {
          //         in: ["Indigent", "POS", "PWD"]
          //       } : lastStatement.class === "Indigent" ? "Indigent" : lastStatement.class === "POS" ? "POS" : lastStatement.class === "PWD" ? "PWD" : "Paying"
          //     }
          //   },
          //   _sum:{
          //     cash:true,
          //     philhealth_deduction:true,
          //     qfs:true,
          //     aics_deduction:true,
          //     maep_deduction:true,
          //     pcso:true,
          //   },
          //   _count:{
          //     beneficiary_id:true,
          //   },

          //   orderBy:{
          //     beneficiary_id:'asc'
          //   }

          // });
          // Use startDate and endDate as needed
        } else {
          console.error("dateFrom or dateTo is undefined");
        }
      } else {
        console.error("No lastStatement found");
      }
    },
  })
  .query("getStatementSummaryByDates", {
    input: z.object({
      startDate: z.string(),
      endDate: z.string(),
      hospital: z.string(),
      class: z.string(),
    }),
    async resolve({ ctx, input }) {
      const startDate = new Date(input.startDate);
      const endDate = new Date(input.endDate);
      startDate.setHours(0, 0, 0, 0); // Set to midnight
      endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

      const groupedSessions = await ctx.prisma.sessions.groupBy({
        by: ["beneficiary_id"],
        where: {
          is_deleted: 0,
          hospital: input.hospital,
          created_at: {
            gte: startDate,
            lte: endDate,
          },
          beneficiary: {
            class:
              input.class === "Beneficiary (Indigent/POS/PWD)"
                ? {
                    in: ["Indigent", "POS", "PWD"],
                  }
                : input.class === "Indigent"
                ? "Indigent"
                : input.class === "POS"
                ? "POS"
                : input.class === "PWD"
                ? "PWD"
                : "Paying",
          },
        },
        _sum: {
          cash: true,
          philhealth_deduction: true,
          qfs: true,
          aics_deduction: true,
          maep_deduction: true,
          pcso: true,
        },
        _count: {
          beneficiary_id: true,
        },
        orderBy: {
          beneficiary_id: "asc",
        },
      });

      // Fetch beneficiaries' last names separately
      const beneficiaryIds = groupedSessions.map(group => group.beneficiary_id);
      const beneficiaries = await ctx.prisma.beneficiary.findMany({
        where: {
          id: {
            in: beneficiaryIds,
          },
        },
        select: {
          id: true,
          lastName: true,
          firstName: true,
          middleName: true,
        },
      });

      // Combine the grouped sessions with beneficiaries' last names
      const groupedSessionsWithNames = groupedSessions.map(group => ({
        beneficiary_id: group.beneficiary_id,
        lastName:
          beneficiaries.find(b => b.id === group.beneficiary_id)?.lastName ||
          null,
        firstName:
          beneficiaries.find(b => b.id === group.beneficiary_id)?.firstName ||
          null,
        middleName:
          beneficiaries.find(b => b.id === group.beneficiary_id)?.middleName ||
          null,
        sessionCount: group._count || 0,
        sum: {
          cash: group._sum.cash || 0,
          philhealth_deduction: group._sum.philhealth_deduction || 0,
          qfs: group._sum.qfs || 0,
          aics_deduction: group._sum.aics_deduction || 0,
          maep_deduction: group._sum.maep_deduction || 0,
          pcso: group._sum.pcso || 0,
        },
      }));

      // Sort the results by lastName
      groupedSessionsWithNames.sort((a, b) => {
        if (a.lastName && b.lastName) {
          return a.lastName.localeCompare(b.lastName);
        } else if (!a.lastName && b.lastName) {
          return 1;
        } else if (a.lastName && !b.lastName) {
          return -1;
        } else {
          return 0;
        }
      });
      // console.log(groupedSessionsWithNames);
      return groupedSessionsWithNames;
    },
  })
  .query("getStatementSummaryDetails", {
    input: z.string(),
    async resolve({ ctx, input }) {
      const lastStatement = await ctx.prisma.statement.findFirst({
        where: {
          statement_code: input,
        },
      });

      // Check if lastStatement is defined
      if (lastStatement) {
        const dateFrom = lastStatement.DateFrom;
        const dateTo = lastStatement.DateTo; // Corrected from DateFrom to DateTo

        // Check if dateFrom and dateTo are defined before creating Date objects
        if (dateFrom && dateTo) {
          const startDate = new Date(dateFrom);
          const endDate = new Date(dateTo);
          startDate.setHours(0, 0, 0, 0); // Set to midnight
          endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

          return await ctx.prisma.sessions.findMany({
            where: {
              is_deleted: 0,
              hospital: lastStatement.hospital,
              created_at: {
                gte: startDate,
                lte: endDate,
              },
              beneficiary: {
                class:
                  lastStatement.class === "Beneficiary (Indigent/POS/PWD)"
                    ? {
                        in: ["Indigent", "POS", "PWD"],
                      }
                    : lastStatement.class === "Indigent"
                    ? "Indigent"
                    : lastStatement.class === "POS"
                    ? "POS"
                    : lastStatement.class === "PWD"
                    ? "PWD"
                    : "Paying",
              },
            },
            select: {
              beneficiary_id: true,
              hospital: true,
              created_at: true,
              cash: true,
              aics_deduction: true,
              maep_deduction: true,
              philhealth_deduction: true,
              qfs: true,
              sessions_itemUsed: {
                select: {
                  unitPrice: true,
                  quantity: true,
                },
              },
              beneficiary: {
                select: {
                  lastName: true,
                  firstName: true,
                  middleName: true,
                },
              },
            },
            orderBy: {
              beneficiary: {
                lastName: "asc", // Assuming 'asc' is the sorting order for lastName
              },
              // Assuming 'asc' is the sorting order for created_at
            },
          });
          // Use startDate and endDate as needed
        } else {
          console.error("dateFrom or dateTo is undefined");
        }
      } else {
        console.error("No lastStatement found");
      }
    },
  })
  .mutation("deleteSessionTransaction", {
    input: z.object({
      id: z.number(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.sessions.update({
        where: {
          id: input.id,
        },
        data: {
          is_deleted: 1,
        },
      });
    },
  })
  .mutation("restoreSessionTransaction", {
    input: z.object({
      id: z.number(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.sessions.update({
        where: {
          id: input.id,
        },
        data: {
          is_deleted: 0,
        },
      });
    },
  })
  .query("getSessionTransactionAdminLive", {
    input: z.object({
      hospital: z.string(),
      class: z.string(),
      name: z.string(),
      startDate: z.string().nullish(),
      endDate: z.string().nullish(),
      page: z.optional(z.number().min(1)),
      pageSize: z.optional(z.number().min(1).max(100)),
    }),
    async resolve({ ctx, input }) {
      const page = input.page || 1;
      const pageSize = input.pageSize || 10;
      // Ensure startDate and endDate are valid strings or null
      const startDate = input.startDate ? new Date(input.startDate) : null;
      const endDate = input.endDate ? new Date(input.endDate) : null;

      if (startDate) {
        startDate.setHours(0, 0, 0, 0); // Set to midnight
      }
      if (endDate) {
        endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999
      }

      // created_at: {
      //   gte: startDate,
      //   lte: endDate,
      // },
      const where = {
        is_deleted: 0,
        ...(input.hospital || ctx.session.user.permission === "admin"
          ? {
              hospital: {
                contains: input.hospital,
                mode: Prisma.QueryMode.insensitive,
              },
            }
          : {
              hospital: {
                contains: ctx.session.user.municipality,
                mode: Prisma.QueryMode.insensitive,
              },
            }),
        ...(input.class && {
          beneficiary: {
            class: input.class,
          },
        }),
        ...(input.startDate && {
          bill_date: {
            gte: startDate!,
            lte: endDate!,
          },
        }),
        ...(input.name && {
          OR: [
            {
              beneficiary: {
                firstName: {
                  contains: input.name,
                  mode: Prisma.QueryMode.insensitive,
                },
              },
            },
            {
              beneficiary: {
                lastName: {
                  contains: input.name,
                  mode: Prisma.QueryMode.insensitive,
                },
              },
            },
          ],
        }),
      };

      const transaction = await ctx.prisma.sessions.findMany({
        where,
        select: {
          id: true,
          beneficiary_id: true,
          hospital: true,
          is_deleted: true,
          created_at: true,
          beneficiary: {
            select: {
              firstName: true,
              lastName: true,
              middleName: true,
              address: true,
              gender: true,
              class: true,
              status: true,
            },
          },
          sessions_itemUsed: {
            select: {
              unitPrice: true,
              quantity: true,
              itemName: true,
              unitMeasure: true,
            },
          },
        },
        orderBy: {
          id: "desc",
        },
        skip: (page - 1) * pageSize,
        take: pageSize,
      });
      const total = await ctx.prisma.sessions.count({
        where,
      });
      return { transaction, total };
    },
  })
  .query("getSessionTransactionAdmin", {
    async resolve({ ctx, input }) {
      return await ctx.prisma.sessions.findMany({
        where: {
          is_deleted: 0,
        },
        orderBy: {
          id: "desc",
        },
        select: {
          id: true,
          beneficiary_id: true,
          hospital: true,
          is_deleted: true,
          created_at: true,
          beneficiary: {
            select: {
              firstName: true,
              lastName: true,
              middleName: true,
              address: true,
              gender: true,
              class: true,
              status: true,
            },
          },
          sessions_itemUsed: {
            select: {
              unitPrice: true,
              quantity: true,
              itemName: true,
              unitMeasure: true,
            },
          },
        },
      });
    },
  })
  .query("getSessionTransactionByID", {
    input: z.number(),
    async resolve({ ctx, input }) {
      return await ctx.prisma.sessions.findMany({
        where: {
          id: input,
        },
        orderBy: {
          id: "desc",
        },
        select: {
          id: true,
          beneficiary_id: true,
          hospital: true,
          is_deleted: true,
          created_at: true,
          beneficiary: {
            select: {
              firstName: true,
              lastName: true,
              middleName: true,
              address: true,
              gender: true,
              class: true,
              status: true,
            },
          },
          sessions_itemUsed: {
            where: {
              quantity: {
                gt: 0,
              },
            },
            select: {
              id: true,
              unitPrice: true,
              quantity: true,
              itemName: true,
              unitMeasure: true,
            },
          },
        },
      });
    },
  })
  .query("getItems", {
    input: z.string(),
    async resolve({ ctx, input }) {
      return await ctx.prisma.items.findMany({
        where: {
          OR: [{ hospital: input }, { hospital: "Cebu Province" }],
        },
        orderBy: {
          itemName: "asc",
        },
      });
    },
  })
  .query("getStatement", {
    input: z.object({
      hospital: z.string(),
      startDate: z.string(),
      endDate: z.string(),
      class: z.string(),
    }),
    async resolve({ ctx, input }) {
      const startDate = new Date(input.startDate);
      const endDate = new Date(input.endDate);
      startDate.setHours(0, 0, 0, 0); // Set to midnight
      endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

      const groupedSessions = await ctx.prisma.sessions.groupBy({
        by: ["beneficiary_id"],
        where: {
          is_deleted: 0,
          hospital: input.hospital,
          created_at: {
            gte: startDate,
            lte: endDate,
          },
          beneficiary: {
            class:
              input.class === "Beneficiary (Indigent/POS/PWD)"
                ? {
                    in: ["Indigent", "POS", "PWD"],
                  }
                : input.class === "Indigent"
                ? "Indigent"
                : input.class === "POS"
                ? "POS"
                : input.class === "PWD"
                ? "PWD"
                : "Paying",
          },
        },
        _sum: {
          cash: true,
          philhealth_deduction: true,
          qfs: true,
          aics_deduction: true,
          maep_deduction: true,
          pcso: true,
        },
        _count: {
          beneficiary_id: true,
        },
        orderBy: {
          beneficiary_id: "asc",
        },
      });

      // Fetch beneficiaries' last names separately
      const beneficiaryIds = groupedSessions.map(group => group.beneficiary_id);
      const beneficiaries = await ctx.prisma.beneficiary.findMany({
        where: {
          id: {
            in: beneficiaryIds,
          },
        },
        select: {
          id: true,
          lastName: true,
          firstName: true,
          middleName: true,
          hospital: true,
        },
      });

      // Combine the grouped sessions with beneficiaries' last names
      const groupedSessionsWithNames = groupedSessions.map(group => ({
        beneficiary_id: group.beneficiary_id,
        lastName:
          beneficiaries.find(b => b.id === group.beneficiary_id)?.lastName ||
          null,
        firstName:
          beneficiaries.find(b => b.id === group.beneficiary_id)?.firstName ||
          null,
        middleName:
          beneficiaries.find(b => b.id === group.beneficiary_id)?.middleName ||
          null,
        sessionCount: group._count || 0,
        sum: {
          cash: group._sum.cash || 0,
          philhealth_deduction: group._sum.philhealth_deduction || 0,
          qfs: group._sum.qfs || 0,
          aics_deduction: group._sum.aics_deduction || 0,
          maep_deduction: group._sum.maep_deduction || 0,
          pcso: group._sum.pcso || 0,
        },
        hospital:
          beneficiaries.find(b => b.id === group.beneficiary_id)?.hospital ||
          null,
      }));

      // Sort the results by lastName
      groupedSessionsWithNames.sort((a, b) => {
        if (a.lastName && b.lastName) {
          return a.lastName.localeCompare(b.lastName);
        } else if (!a.lastName && b.lastName) {
          return 1;
        } else if (a.lastName && !b.lastName) {
          return -1;
        } else {
          return 0;
        }
      });
      // console.log(groupedSessionsWithNames);
      return groupedSessionsWithNames;
    },
  })
  // .query("getStatement", {
  //   input: z.object({
  //     hospital: z.string(),
  //     startDate: z.string(),
  //     endDate: z.string(),
  //     class: z.string(),
  //   }),
  //   async resolve({ ctx, input }) {
  //     const startDate = new Date(input.startDate);
  //     // const newstartDate = startDate.toISOString();
  //     const endDate = new Date(input.endDate);
  //     startDate.setHours(0, 0, 0, 0); // Set to midnight
  //     endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

  //     type WhereCondition = {
  //       hospital: string;
  //       AND?: {
  //         OR: {
  //           class?: string;
  //           status?: string;
  //         }[];
  //       }[];
  //     };

  //     const whereCondition: WhereCondition = {
  //       hospital: input.hospital,
  //       AND: [
  //         {
  //           OR: [{ class: "Indigent" }, { class: "POS" }, { class: "PWD" }],
  //         },
  //         {
  //           OR: [{ status: "Active" }, { status: "Expired" }],
  //         },
  //       ],
  //     };

  //     if (
  //       input.class !== "Beneficiary (Indigent/POS/PWD)" &&
  //       whereCondition.AND
  //     ) {
  //       whereCondition.AND[0]!.OR = [{ class: input.class }];
  //     }

  //     const beneficiaries = await ctx.prisma.beneficiary.findMany({
  //       where: whereCondition,
  //       orderBy: {
  //         lastName: "asc",
  //       },
  //       select: {
  //         id: true,
  //         firstName: true,
  //         lastName: true,
  //         middleName: true,
  //         hospital: true,
  //         sessions: {
  //           where: {
  //             AND: [
  //               {
  //                 created_at: {
  //                   gte: startDate,
  //                   lte: endDate,
  //                 },
  //               },
  //               {
  //                 is_deleted: 0,
  //               },
  //             ],
  //           },
  //           select: {
  //             sessions_itemUsed: {
  //               where: {
  //                 isMedtronics: 1,
  //               },
  //               select: {
  //                 quantity: true,
  //                 unitPrice: true,
  //               },
  //             },
  //           },
  //         },
  //       },
  //     });

  //     const result = beneficiaries.map(beneficiary => {
  //       const totalQuantity = beneficiary.sessions.reduce(
  //         (sum, session) =>
  //           sum +
  //           session.sessions_itemUsed.reduce(
  //             (itemSum, item) => itemSum + (item.quantity || 0),
  //             0
  //           ),
  //         0
  //       );

  //       const unitPrice = beneficiary.sessions.reduce(
  //         (sum, session) =>
  //           session.sessions_itemUsed.reduce(
  //             (itemSum, item) => item.unitPrice || 0,
  //             0
  //           ),
  //         0
  //       );

  //       const totalAmount = beneficiary.sessions.reduce(
  //         (sum, session) =>
  //           sum +
  //           session.sessions_itemUsed.reduce(
  //             (itemSum, item) =>
  //               itemSum + (item.unitPrice * item.quantity || 0),
  //             0
  //           ),
  //         0
  //       );

  //       return {
  //         id: beneficiary.id,
  //         firstName: beneficiary.firstName,
  //         lastName: beneficiary.lastName,
  //         middleName: beneficiary.middleName,
  //         hospital: beneficiary.hospital,
  //         totalQuantity,
  //         unitPrice,
  //         totalAmount,
  //       };
  //     });

  //     return result;

  //   },
  // })
  .query("getHospital", {
    async resolve({ ctx, input }) {
      return await ctx.prisma.hospital.findMany({
        orderBy: {
          hospitalName: "asc",
        },
      });
    },
  })
  .query("getItemsAll", {
    async resolve({ ctx, input }) {
      return await ctx.prisma.items.findMany({
        orderBy: {
          id: "asc",
        },
      });
    },
  })

  .mutation("deleteStatement", {
    input: z.object({
      statement_code: z.string(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.statement.deleteMany({
        where: {
          statement_code: input.statement_code,
        },
      });
    },
  })

  .mutation("updateDate", {
    input: z.object({
      id: z.number(),
      created_at: z.string(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.sessions.updateMany({
        where: {
          id: input.id,
        },
        data: {
          created_at: input.created_at,
        },
      });
    },
  })

  //sensus
  .mutation("insertPatient", {
    input: z.object({
      firstName: z.string(),
      lastName: z.string(),
      middleName: z.string().nullish(),
      classification: z.string(),
    }),
    async resolve({ ctx, input }) {
      try {
        const newPatient = await ctx.prisma.sensus_patients.create({
          data: {
            firstName: input.firstName,
            lastName: input.lastName,
            middleName: input.middleName,
            classification: input.classification,
            hospital: ctx.session.user.municipality,
            status: "Active",
            created_by:
              ctx.session.user.lastname + ", " + ctx.session.user.firstname,
          },
        });

        // Patient insertion successful, return the new patient and success message
        return {
          patient: newPatient,
          message: "Patient inserted successfully.",
        };
      } catch (error) {
        // Patient insertion failed, throw an error message
        throw new Error("Failed to insert patient.");
      }
    },
  })
  .query("getSensusPatient", {
    async resolve({ ctx, input }) {
      const hospital = ctx.session.user.municipality;
      const where = {
        is_deleted: 0,
        ...(ctx.session.user.permission !== "admin" && {
          hospital: {
            contains: hospital,
            mode: Prisma.QueryMode.insensitive,
          },
        }),
      };

      return await ctx.prisma.sensus_patients.findMany({
        where,
        orderBy: {
          id: "desc",
        },
      });
    },
  })
  .query("getSensusPatientLive", {
    input: z.object({
      hospital: z.string(),
      class: z.string(),
      area: z.string(),
      name: z.string(),
      startDate: z.string().nullish(),
      endDate: z.string().nullish(),
      page: z.optional(z.number().min(1)),
      pageSize: z.optional(z.number().min(1).max(100)),
    }),
    async resolve({ ctx, input }) {
      const page = input.page || 1;
      const pageSize = input.pageSize || 10;

      type Filters = {
        is_deleted: number;
        hospital?: string; // Optional hospital property
      };

      const filters: Filters = {
        is_deleted: 0,
      };

      const where = {
        is_deleted: 0,
        ...(input.hospital || ctx.session.user.permission === "admin"
          ? {
              hospital: {
                contains: input.hospital,
                mode: Prisma.QueryMode.insensitive,
              },
            }
          : {
              hospital: {
                contains: ctx.session.user.municipality,
                mode: Prisma.QueryMode.insensitive,
              },
            }),
        ...(input.class && {
          classification: input.class,
        }),
        ...(input.area && {
          area: input.area,
        }),

        ...(input.name && {
          OR: [
            {
              firstName: {
                contains: input.name,
                mode: Prisma.QueryMode.insensitive,
              },
            },
            {
              lastName: {
                contains: input.name,
                mode: Prisma.QueryMode.insensitive,
              },
            },
          ],
        }),
      };

      const transaction = await ctx.prisma.sensus_patients.findMany({
        where,
        orderBy: {
          id: "desc",
        },
        skip: (page - 1) * pageSize,
        take: pageSize,
      });
      const total = await ctx.prisma.sensus_patients.count({
        where,
      });
      return { transaction, total };
    },
  })
  .mutation("updatePatient", {
    input: z.object({
      id: z.number(),
      firstName: z.string(),
      lastName: z.string(),
      middleName: z.string().nullish(),
      classification: z.string(),
      status: z.string(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.sensus_patients.update({
        where: {
          id: input.id,
        },
        data: {
          firstName: input.firstName,
          lastName: input.lastName,
          middleName: input.middleName,
          classification: input.classification,
          status: input.status,
        },
      });
    },
  })

  //LAB TRANSACTIONS
  .mutation("updateLabPatient", {
    input: z.object({
      id: z.number(),
      used: z.number(),
      bill_amount: z.number(),
      remarks: z.string().nullish(),
      equipment_used: z.string(),
      bill_date: z.string(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.laboratory_transactions.update({
        where: {
          id: input.id,
        },
        data: {
          remarks: input.remarks,
          // equipment_used: input.equipment_used,
          used: input.used,
          bill_amount: input.bill_amount,
          bill_date: input.bill_date,
        },
      });
    },
  })
  .mutation("insertlabtransaction", {
    input: z.object({
      laboratory_patients_id: z.number(),
      patients_name: z.string(),
      classification: z.string(),
      bill_date: z.string(),
      lab_type: z.string(),
      area: z.string(),
      list: z.array(
        z.object({
          item_id: z.number(),
          amount: z.number(),
          name: z.string(),
        })
      ),
    }),
    async resolve({ ctx, input }) {
      try {
        const newTransaction = await ctx.prisma.laboratory_transactions.create({
          data: {
            laboratory_patients_id: input.laboratory_patients_id,
            patients_name: input.patients_name,
            classification: input.classification,
            lab_type: input.lab_type,
            area: input.area,
            bill_date: new Date(input.bill_date),
            hospital: ctx.session.user.municipality,
            created_by:
              ctx.session.user.lastname + ", " + ctx.session.user.firstname,
          },
        });

        // Create the related used items entries
        const usedItemsPromises = input.list.map(item => {
          return ctx.prisma.laboratory_used.create({
            data: {
              lab_itemsId: item.item_id,
              laboratory_transactionsId: newTransaction.id,
              amount: item.amount,
              item_name: item.name,
              bill_date: new Date(input.bill_date),
            },
          });
        });

        await Promise.all(usedItemsPromises);

        return newTransaction;
        // Patient insertion successful, return the new patient and success message
        // return {
        //   patient: newPatient,
        //   message: "Record inserted successfully.",
        // };
      } catch (error) {
        // Patient insertion failed, throw an error message
        throw new Error("Failed to insert record.");
      }
    },
  })

  .query("getLaboratoryTranscation", {
    input: z.object({
      startDate: z.string(),
      endDate: z.string(),
    }),
    async resolve({ ctx, input }) {
      const startDate = new Date(input.startDate);
      const endDate = new Date(input.endDate);
      startDate.setHours(0, 0, 0, 0); // Set to midnight
      endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

      type Filters = {
        created_at: {
          gte: Date;
          lte: Date;
        };
        is_deleted: number;
        lab_type: string;
        hospital?: string; // Optional hospital property
      };

      const filters: Filters = {
        created_at: {
          gte: startDate,
          lte: endDate,
        },
        is_deleted: 0,
        lab_type: "LABORATORY",
      };

      // Apply hospital filter only if the user is not an admin
      if (ctx.session.user.permission !== "admin") {
        filters.hospital = ctx.session.user.municipality;
      }

      return await ctx.prisma.laboratory_transactions.findMany({
        where: filters,
        select: {
          patients_name: true,
          classification: true,
          bill_date: true,
          created_at: true,
          laboratory_used: true,
          hospital: true,
          id: true,
          area: true,
        },
        orderBy: {
          id: "desc",
        },
      });
    },
  })
  .query("getLaboratoryTranscationLive", {
    input: z.object({
      hospital: z.string(),
      class: z.string(),
      area: z.string(),
      name: z.string(),
      startDate: z.string().nullish(),
      endDate: z.string().nullish(),
      page: z.optional(z.number().min(1)),
      pageSize: z.optional(z.number().min(1).max(100)),
    }),
    async resolve({ ctx, input }) {
      const page = input.page || 1;
      const pageSize = input.pageSize || 10;
      const startDate = input.startDate ? new Date(input.startDate) : null;
      const endDate = input.endDate ? new Date(input.endDate) : null;

      if (startDate) {
        startDate.setHours(0, 0, 0, 0); // Set to midnight
      }
      if (endDate) {
        endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999
      }

      const where = {
        is_deleted: 0,
        lab_type: "LABORATORY",
        ...(input.hospital || ctx.session.user.permission === "admin"
          ? {
              hospital: {
                contains: input.hospital,
                mode: Prisma.QueryMode.insensitive,
              },
            }
          : {
              hospital: {
                contains: ctx.session.user.municipality,
                mode: Prisma.QueryMode.insensitive,
              },
            }),
        ...(input.class && {
          classification: input.class,
        }),
        ...(input.area && {
          area: input.area,
        }),
        ...(input.startDate && {
          bill_date: {
            gte: startDate!,
            lte: endDate!,
          },
        }),
        ...(input.name && {
          patients_name: {
            contains: input.name,
            mode: Prisma.QueryMode.insensitive,
          },
        }),
      };

      const transaction = await ctx.prisma.laboratory_transactions.findMany({
        where,
        select: {
          patients_name: true,
          classification: true,
          bill_date: true,
          created_at: true,
          laboratory_used: true,
          hospital: true,
          id: true,
          area: true,
        },
        orderBy: {
          id: "desc",
        },
        skip: (page - 1) * pageSize,
        take: pageSize,
      });
      const total = await ctx.prisma.laboratory_transactions.count({
        where,
      });
      return { transaction, total };
    },
  })
  .query("getRadTranscationLive", {
    input: z.object({
      hospital: z.string(),
      class: z.string(),
      area: z.string(),
      name: z.string(),
      startDate: z.string().nullish(),
      endDate: z.string().nullish(),
      page: z.optional(z.number().min(1)),
      pageSize: z.optional(z.number().min(1).max(100)),
    }),
    async resolve({ ctx, input }) {
      const page = input.page || 1;
      const pageSize = input.pageSize || 10;
      const startDate = input.startDate ? new Date(input.startDate) : null;
      const endDate = input.endDate ? new Date(input.endDate) : null;

      if (startDate) {
        startDate.setHours(0, 0, 0, 0); // Set to midnight
      }
      if (endDate) {
        endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999
      }

      const where = {
        is_deleted: 0,
        lab_type: "RADIOLOGY",
        ...(input.hospital || ctx.session.user.permission === "admin"
          ? {
              hospital: {
                contains: input.hospital,
                mode: Prisma.QueryMode.insensitive,
              },
            }
          : {
              hospital: {
                contains: ctx.session.user.municipality,
                mode: Prisma.QueryMode.insensitive,
              },
            }),
        ...(input.class && {
          classification: input.class,
        }),
        ...(input.area && {
          area: input.area,
        }),
        ...(input.startDate && {
          bill_date: {
            gte: startDate!,
            lte: endDate!,
          },
        }),
        ...(input.name && {
          patients_name: {
            contains: input.name,
            mode: Prisma.QueryMode.insensitive,
          },
        }),
      };

      const transaction = await ctx.prisma.laboratory_transactions.findMany({
        where,
        select: {
          patients_name: true,
          classification: true,
          bill_date: true,
          created_at: true,
          laboratory_used: true,
          hospital: true,
          id: true,
          area: true,
        },
        orderBy: {
          id: "desc",
        },
        skip: (page - 1) * pageSize,
        take: pageSize,
      });
      const total = await ctx.prisma.laboratory_transactions.count({
        where,
      });
      return { transaction, total };
    },
  })
  .query("getRadTranscation", {
    input: z.object({
      startDate: z.string(),
      endDate: z.string(),
    }),
    async resolve({ ctx, input }) {
      const startDate = new Date(input.startDate);
      const endDate = new Date(input.endDate);
      startDate.setHours(0, 0, 0, 0); // Set to midnight
      endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

      type Filters = {
        created_at: {
          gte: Date;
          lte: Date;
        };
        is_deleted: number;
        lab_type: string;
        hospital?: string; // Optional hospital property
      };

      const filters: Filters = {
        created_at: {
          gte: startDate,
          lte: endDate,
        },
        is_deleted: 0,
        lab_type: "RADIOLOGY",
      };

      // Apply hospital filter only if the user is not an admin
      if (ctx.session.user.permission !== "admin") {
        filters.hospital = ctx.session.user.municipality;
      }

      return await ctx.prisma.laboratory_transactions.findMany({
        where: filters,
        select: {
          patients_name: true,
          classification: true,
          bill_date: true,
          created_at: true,
          laboratory_used: true,
          hospital: true,
          id: true,
          area: true,
        },
        orderBy: {
          id: "desc",
        },
      });
    },
  })
  .mutation("deletePatient", {
    input: z.object({
      id: z.number(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.sensus_patients.update({
        where: {
          id: input.id,
        },
        data: {
          is_deleted: 1,
          deleted_by:
            ctx.session.user.lastname + ", " + ctx.session.user.firstname,
        },
      });
    },
  })

  .mutation("deleteLabTransaction", {
    input: z.object({
      id: z.number(),
    }),
    async resolve({ input, ctx }) {
      await ctx.prisma.laboratory_used.updateMany({
        where: {
          laboratory_transactionsId: input.id,
        },
        data: {
          is_deleted: 1,
          deleted_by:
            ctx.session.user.lastname + ", " + ctx.session.user.firstname,
        },
      });

      return await ctx.prisma.laboratory_transactions.update({
        where: {
          id: input.id,
        },
        data: {
          is_deleted: 1,
          deleted_by:
            ctx.session.user.lastname + ", " + ctx.session.user.firstname,
        },
      });
    },
  })

  .mutation("insertLabItems", {
    input: z.object({
      name: z.string(),
      type: z.string(),
      amount: z.number(),
    }),
    async resolve({ ctx, input }) {
      try {
        const newPatient = await ctx.prisma.lab_items.create({
          data: {
            name: input.name,
            type: input.type,
            amount: input.amount,
            hospital: ctx.session.user.municipality,
          },
        });

        // Patient insertion successful, return the new patient and success message
        return {
          patient: newPatient,
          message: "Patient inserted successfully.",
        };
      } catch (error) {
        // Patient insertion failed, throw an error message
        throw new Error("Failed to insert patient.");
      }
    },
  })
  .query("getLabItems", {
    async resolve({ ctx, input }) {
      const hospital = ctx.session.user.municipality;
      const where = {
        is_deleted: 0,
        type: "LABORATORY",

        ...(ctx.session.user.permission !== "admin" && {
          hospital: {
            contains: hospital,
            mode: Prisma.QueryMode.insensitive,
          },
        }),
      };

      return await ctx.prisma.lab_items.findMany({
        where,
        orderBy: {
          id: "desc",
        },
      });
    },
  })
  .query("getRadItems", {
    async resolve({ ctx, input }) {
      const hospital = ctx.session.user.municipality;
      const where = {
        is_deleted: 0,
        type: "RADIOLOGY",

        ...(ctx.session.user.permission !== "admin" && {
          hospital: {
            contains: hospital,
            mode: Prisma.QueryMode.insensitive,
          },
        }),
      };

      return await ctx.prisma.lab_items.findMany({
        where: where,
        orderBy: {
          id: "desc",
        },
      });
    },
  })
  .query("getLaboratoryItems", {
    async resolve({ ctx, input }) {
      const hospital = ctx.session.user.municipality;
      const where = {
        is_deleted: 0,
        type: "LABORATORY",

        ...(ctx.session.user.permission !== "admin" && {
          hospital: {
            contains: hospital,
            mode: Prisma.QueryMode.insensitive,
          },
        }),
      };
      return await ctx.prisma.lab_items.findMany({
        where,
        orderBy: {
          id: "desc",
        },
      });
    },
  })
  .mutation("updateLabItems", {
    input: z.object({
      id: z.number(),
      name: z.string(),
      type: z.string(),
      amount: z.number(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.lab_items.update({
        where: {
          id: input.id,
        },
        data: {
          name: input.name,
          type: input.type,
          amount: input.amount,
        },
      });
    },
  })
  .mutation("deleteLabItems", {
    input: z.object({
      id: z.number(),
    }),
    async resolve({ input, ctx }) {
      console.log("Submitted", input);
      return await ctx.prisma.lab_items.update({
        where: {
          id: input.id,
        },
        data: {
          is_deleted: 1,
          deleted_by:
            ctx.session.user.lastname + ", " + ctx.session.user.firstname,
        },
      });
    },
  })
  //sensus
  .mutation("insertsensustransaction", {
    input: z.object({
      laboratory_patients_id: z.number(),
      bill_amount: z.number(),
      patients_name: z.string(),
      classification: z.string(),
      bill_date: z.string(),
      area: z.string(),
      discount_senior: z.number(),
      discount_pwd: z.number(),
      philhealth_deduction: z.number(),
      maip_deduction: z.number(),
      aics_deduction: z.number(),
      himsugbo: z.number(),
      malaskit: z.number(),
      pcso: z.number(),
      cash: z.number(),
      qfs: z.number(),
      promisri_note: z.number(),
    }),
    async resolve({ ctx, input }) {
      try {
        const newPatient = await ctx.prisma.sensus_transactions.create({
          data: {
            sensus_patients_id: input.laboratory_patients_id,
            patients_name: input.patients_name,
            classification: input.classification,
            hospital: ctx.session.user.municipality,
            bill_amount: input.bill_amount,
            bill_date: input.bill_date,
            area: input.area,
            discount_senior: input.discount_senior,
            discount_pwd: input.discount_pwd,
            philhealth_deduction: input.philhealth_deduction,
            maip_deduction: input.maip_deduction,
            aics_deduction: input.aics_deduction,
            himsugbo: input.himsugbo,
            malaskit: input.malaskit,
            pcso: input.pcso,
            cash: input.cash,
            qfs: input.qfs,
            promisri_note: input.promisri_note,
            created_by:
              ctx.session.user.lastname + ", " + ctx.session.user.firstname,
          },
        });

        // Patient insertion successful, return the new patient and success message
        return {
          patient: newPatient,
          message: "Record inserted successfully.",
        };
      } catch (error) {
        // Patient insertion failed, throw an error message
        throw new Error("Failed to insert record.");
      }
    },
  })
  .query("getSensusTranscation", {
    input: z.object({
      startDate: z.string(),
      endDate: z.string(),
    }),
    async resolve({ ctx, input }) {
      const startDate = new Date(input.startDate);
      const endDate = new Date(input.endDate);
      startDate.setHours(0, 0, 0, 0); // Set to midnight
      endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

      type Filters = {
        created_at: {
          gte: Date;
          lte: Date;
        };
        is_deleted: number;
        hospital?: string; // Optional hospital property
      };

      const filters: Filters = {
        created_at: {
          gte: startDate,
          lte: endDate,
        },
        is_deleted: 0,
      };

      // Apply hospital filter only if the user is not an admin
      if (ctx.session.user.permission !== "admin") {
        filters.hospital = ctx.session.user.municipality;
      }

      return await ctx.prisma.sensus_transactions.findMany({
        where: filters,
        orderBy: {
          id: "desc",
        },
      });
    },
  })

  .query("getSensusTranscationLive", {
    input: z.object({
      hospital: z.string(),
      class: z.string(),
      area: z.string(),
      name: z.string(),
      startDate: z.string().nullish(),
      endDate: z.string().nullish(),
      page: z.optional(z.number().min(1)),
      pageSize: z.optional(z.number().min(1).max(100)),
    }),
    async resolve({ ctx, input }) {
      const page = input.page || 1;
      const pageSize = input.pageSize || 10;
      // Ensure startDate and endDate are valid strings or null
      const startDate = input.startDate ? new Date(input.startDate) : null;
      const endDate = input.endDate ? new Date(input.endDate) : null;

      if (startDate) {
        startDate.setHours(0, 0, 0, 0); // Set to midnight
      }
      if (endDate) {
        endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999
      }

      // const where = {
      //   // created_at: {
      //   //   gte: startDate,
      //   //   lte: endDate,
      //   // },
      //   is_deleted: 0,
      // };

      const where = {
        is_deleted: 0,
        ...(input.hospital || ctx.session.user.permission === "admin"
          ? {
              hospital: {
                contains: input.hospital,
                mode: Prisma.QueryMode.insensitive,
              },
            }
          : {
              hospital: {
                contains: ctx.session.user.municipality,
                mode: Prisma.QueryMode.insensitive,
              },
            }),
        ...(input.class && {
          classification: input.class,
        }),
        ...(input.area && {
          area: input.area,
        }),
        ...(input.startDate && {
          bill_date: {
            gte: startDate!,
            lte: endDate!,
          },
        }),
        ...(input.name && {
          patients_name: {
            contains: input.name,
            mode: Prisma.QueryMode.insensitive,
          },
        }),
      };

      // Apply hospital filter only if the user is not an admin
      // if (ctx.session.user.permission !== "admin") {
      //   filters.hospital = ctx.session.user.municipality;
      // }

      const transaction = await ctx.prisma.sensus_transactions.findMany({
        where,
        orderBy: {
          id: "desc",
        },
        skip: (page - 1) * pageSize,
        take: pageSize,
      });
      const total = await ctx.prisma.sensus_transactions.count({
        where,
      });
      return { transaction, total };
    },
  })
  .mutation("updateSensusTransaction", {
    input: z.object({
      id: z.number(),
      bill_amount: z.number(),
      bill_date: z.string(),
      classification: z.string(),
      area: z.string(),
      discount_senior: z.number(),
      discount_pwd: z.number(),
      philhealth_deduction: z.number(),
      maip_deduction: z.number(),
      aics_deduction: z.number(),
      himsugbo: z.number(),
      malaskit: z.number(),
      pcso: z.number(),
      cash: z.number(),
      qfs: z.number(),
      promisri_note: z.number(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.sensus_transactions.update({
        where: {
          id: input.id,
        },
        data: {
          classification: input.classification,
          bill_amount: input.bill_amount,
          bill_date: input.bill_date,
          area: input.area,
          discount_senior: input.discount_senior,
          discount_pwd: input.discount_pwd,
          philhealth_deduction: input.philhealth_deduction,
          maip_deduction: input.maip_deduction,
          aics_deduction: input.aics_deduction,
          himsugbo: input.himsugbo,
          malaskit: input.malaskit,
          pcso: input.pcso,
          cash: input.cash,
          qfs: input.qfs,
          promisri_note: input.promisri_note,
        },
      });
    },
  })

  .mutation("deleteSensusTransaction", {
    input: z.object({
      id: z.number(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.sensus_transactions.update({
        where: {
          id: input.id,
        },
        data: {
          is_deleted: 1,
          deleted_by:
            ctx.session.user.lastname + ", " + ctx.session.user.firstname,
        },
      });
    },
  })
  .query("getSensusReport", {
    input: z.object({
      startDate: z.string(),
      endDate: z.string(),
      area: z.string(),
      classification: z.string(),
    }),
    async resolve({ ctx, input }) {
      const startDate = new Date(input.startDate);
      const endDate = new Date(input.endDate);
      startDate.setHours(0, 0, 0, 0); // Set to midnight
      endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

      const hospital = ctx.session.user.municipality;
      const where = {
        is_deleted: 0,
        bill_date: {
          gte: startDate,
          lte: endDate,
        },
        area: input.area,
        classification: input.classification,
        ...(ctx.session.user.permission !== "admin" && {
          hospital: {
            contains: hospital,
            mode: Prisma.QueryMode.insensitive,
          },
        }),
      };

      return await ctx.prisma.sensus_transactions.findMany({
        where,
        orderBy: {
          bill_date: "asc",
        },
      });
    },
  })
  .query("getSensusReportAdmin", {
    input: z.object({
      startDate: z.string(),
      endDate: z.string(),
      area: z.string(),
      classification: z.string(),
      hospital: z.string(),
    }),
    async resolve({ ctx, input }) {
      const startDate = new Date(input.startDate);
      const endDate = new Date(input.endDate);
      startDate.setHours(0, 0, 0, 0); // Set to midnight
      endDate.setHours(23, 59, 59, 999); // Set to 23:59:59.999

      return await ctx.prisma.sensus_transactions.findMany({
        where: {
          bill_date: {
            gte: startDate,
            lte: endDate,
          },
          hospital: {
            contains: input.hospital,
            mode: Prisma.QueryMode.insensitive,
          },
          is_deleted: 0,
          area: input.area,
          classification: input.classification,
        },
        orderBy: {
          bill_date: "asc",
        },
      });
    },
  })
  .mutation("insertphicmanagement", {
    input: z.object({
      year: z.string(),
    }),
    async resolve({ ctx, input }) {
      const existingYear = await ctx.prisma.philhealth_management.findFirst({
        where: {
          year: input.year,
          is_deleted: 0,
          hospital: ctx.session.user.municipality,
        },
      });

      if (existingYear) {
        throw new Error(
          `Sorry, year "${input.year}" is already taken. Please choose a different year and try again.`
        );
      }

      try {
        return await ctx.prisma.philhealth_management.create({
          data: {
            year: input.year,
            hospital: ctx.session.user.municipality,
          },
        });
      } catch (error) {
        // Patient insertion failed, throw an error message
        throw new Error("Failed to insert year.");
      }
    },
  })
  .query("getphicmanagement", {
    async resolve({ ctx, input }) {
      const hospital = ctx.session.user.municipality;
      const where = {
        is_deleted: 0,
        ...(ctx.session.user.permission !== "admin" && {
          hospital: {
            contains: hospital,
            mode: Prisma.QueryMode.insensitive,
          },
        }),
      };

      return await ctx.prisma.philhealth_management.findMany({
        where,
        orderBy: {
          id: "desc",
        },
      });
    },
  })
  .mutation("updatephicmanagement", {
    input: z.object({
      id: z.number(),
      data: z.record(z.any()), // allows any additional fields in data
    }),
    async resolve({ input, ctx }) {
      const { id, data } = input;
      const currentDate = new Date();
      return await ctx.prisma.philhealth_management.update({
        where: {
          id,
        },
        data: {
          ...data,
          updated_at: currentDate,
          // january_total_denied: data.january_total_denied,
        },
      });
    },
  })
  .mutation("deletePhic", {
    input: z.object({
      id: z.number(),
    }),
    async resolve({ input, ctx }) {
      return await ctx.prisma.philhealth_management.update({
        where: {
          id: input.id,
        },
        data: {
          is_deleted: 1,
          deleted_by:
            ctx.session.user.lastname + ", " + ctx.session.user.firstname,
        },
      });
    },
  })
  .query("getphicmanagementexport", {
    input: z.object({
      id: z.number(),
    }),
    async resolve({ ctx, input }) {
      return await ctx.prisma.philhealth_management.findMany({
        where: {
          id: input.id,
        },
        orderBy: {
          id: "desc",
        },
      });
    },
  });
