// src/server/router/index.ts
import { createRouter } from "./context";
import superjson from "superjson";
import { registration } from "./protected/registration";
import { taxdec } from "./protected/taxdec";

export const appRouter = createRouter()
  .transformer(superjson)
  .merge("registration.", registration)
  .merge("taxdec.",taxdec);


// export type definition of API
export type AppRouter = typeof appRouter;
