import z from 'zod'

export const getCpgUsersSchema = z.object({
  search: z.string().default("maricel"),
  limit: z.number().default(10),
  offset: z.number().default(0),
  sortField: z.string().default("first_name"),
  sortOrder: z.string().default("asc")
})

export type getCpgUsersInput = z.TypeOf<typeof getCpgUsersSchema>
