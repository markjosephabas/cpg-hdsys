// LayoutSelector.tsx

import React from "react";
import { useSession } from "next-auth/react";
import DashboardLayout from "./dashboard-layout";

interface LayoutSelectorProps {
  component: React.ComponentType;
  pageProps: any; // Adjust the type based on your pageProps
}

const LayoutSelector: React.FC<LayoutSelectorProps> = ({
  component: Component,
  pageProps,
}) => {
  const session = useSession(); // Access the session context
  // console.log(session);
  if (session.status === "unauthenticated") {
    return <Component {...pageProps} />;
  } else {
    // return <Component {...pageProps} />;
    return (
      <DashboardLayout>
        <Component {...pageProps} />
      </DashboardLayout>
    );
  }

  // if (
  //   (session && session?.data?.user?.permission === "admin") ||
  //   session?.data?.user?.permission === "CASHIER"
  // ) {
  //   return (
  //     <DashboardLayout>
  //       <Component {...pageProps} />
  //     </DashboardLayout>
  //   );
  // } else {
  //   return <Component {...pageProps} />;
  // }
};

export default LayoutSelector;
