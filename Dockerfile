FROM node:lts as builder
WORKDIR /my-project
COPY . .
COPY package.json ./
RUN npm install
RUN npm run build

FROM node:lts as runner
WORKDIR /my-project
ENV NODE_ENV production
ENV DATABASE_URL postgres://postgres_user:postgres_pass@postgres:5432/taxdec
# Next Auth
ENV NEXTAUTH_SECRET qZRp020nxPsKxRQGq0QOT6JKnehaOw88
ENV NEXTAUTH_URL http://localhost:3000
ENV NEXT_PUBLIC_AUTH_URL http://localhost:3000
# Next Auth Google Provider
ENV GOOGLE_CLIENT_ID n
ENV GOOGLE_CLIENT_SECRET n
ENV JWT_SECRET qZRp020nxPsKxRQGq0QOT6JKnehaOw88
# If you are using a custom next.config.js file, uncomment this line.
# COPY --from=builder /my-project/next.config.js ./
COPY --from=builder /my-project/public ./public
COPY --from=builder /my-project/.next ./.next
COPY --from=builder /my-project/node_modules ./node_modules
COPY --from=builder /my-project/package.json ./package.json

EXPOSE 3000
CMD ["yarn", "start"]
