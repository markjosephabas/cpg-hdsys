/*
  Warnings:

  - You are about to drop the column `series_number` on the `series_number` table. All the data in the column will be lost.
  - Added the required column `municipality` to the `remittances` table without a default value. This is not possible if the table is not empty.
  - Added the required column `total_assessed_value` to the `remittances` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "remittances" ADD COLUMN     "municipality" TEXT NOT NULL,
ADD COLUMN     "total_assessed_value" DOUBLE PRECISION NOT NULL;

-- AlterTable
ALTER TABLE "series_number" DROP COLUMN "series_number",
ADD COLUMN     "seriesNumber" INTEGER NOT NULL DEFAULT 0;

-- CreateTable
CREATE TABLE "system_users" (
    "id" SERIAL NOT NULL,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "permission" TEXT NOT NULL,
    "municipality" TEXT NOT NULL,
    "lastname" TEXT,
    "firstname" TEXT,
    "middlename" TEXT,
    "created_at" TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "system_users_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "system_users_username_key" ON "system_users"("username");
