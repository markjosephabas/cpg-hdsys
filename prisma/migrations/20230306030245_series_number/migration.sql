/*
  Warnings:

  - You are about to drop the `merchant_sales` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `merchants` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `merchants_billing` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `prescription_list` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `user_transaction` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `users` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[username]` on the table `system_users` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `municipality` to the `system_users` table without a default value. This is not possible if the table is not empty.

*/

-- CreateTable
CREATE TABLE "tax_declarations" (
    "id" SERIAL NOT NULL,
    "td_no" TEXT,
    "property_no" TEXT NOT NULL,
    "arp_no" TEXT,
    "owner_name" TEXT NOT NULL,
    "owner_tin" TEXT,
    "owner_address" TEXT,
    "owner_contact_no" TEXT,
    "admin_name" TEXT,
    "admin_tin" TEXT,
    "admin_address" TEXT,
    "admin_contact_no" TEXT,
    "tax_exempt" TEXT,
    "qtr" TEXT,
    "year" TEXT,
    "approved_by" TEXT,
    "position" TEXT,
    "cancel_td_no" TEXT,
    "cancel_owner" TEXT,
    "prev_av" DOUBLE PRECISION,
    "Memoranda" TEXT,
    "municipality" TEXT,
    "date_approved" DATE,
    "location" TEXT,
    "oct_no" TEXT,
    "survey_no" TEXT,
    "cct_no" TEXT,
    "lot_no" TEXT,
    "dated" DATE,
    "blk_no" TEXT,
    "north" TEXT,
    "south" TEXT,
    "east" TEXT,
    "west" TEXT,
    "kind_property" TEXT,
    "no_storeys" TEXT,
    "specify" TEXT,
    "brief_desc" TEXT,
    "classification" TEXT,
    "area" TEXT,
    "market_value" DOUBLE PRECISION,
    "actual_use" TEXT,
    "assess_level" TEXT,
    "assessed_value" DOUBLE PRECISION,
    "status" TEXT NOT NULL DEFAULT 'Pending',
    "remittance_id" INTEGER,
    "created_at" DATE DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "tax_declarations_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "remittances" (
    "id" SERIAL NOT NULL,
    "series_number" TEXT,
    "Date_from" TIMESTAMP(3),
    "Date_To" TIMESTAMP(3),
    "created_at" DATE DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "remittances_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "series_number" (
    "id" SERIAL NOT NULL,
    "series_number" INTEGER NOT NULL DEFAULT 0,
    "municipality" TEXT NOT NULL,

    CONSTRAINT "series_number_pkey" PRIMARY KEY ("id")
);

--INITIAL VALUE FOR SERIES NUMBER--
INSERT INTO series_number (municipality) VALUES ('Alcantara');
INSERT INTO series_number (municipality) VALUES ('Alcoy');
INSERT INTO series_number (municipality) VALUES ('Alegria');
INSERT INTO series_number (municipality) VALUES ('Aloguinsan');
INSERT INTO series_number (municipality) VALUES ('Argao');
INSERT INTO series_number (municipality) VALUES ('Asturias');
INSERT INTO series_number (municipality) VALUES ('Badian');
INSERT INTO series_number (municipality) VALUES ('Barili');
INSERT INTO series_number (municipality) VALUES ('Boljoon');
INSERT INTO series_number (municipality) VALUES ('Borbon');
INSERT INTO series_number (municipality) VALUES ('Carmen');
INSERT INTO series_number (municipality) VALUES ('Catmon');
INSERT INTO series_number (municipality) VALUES ('Compostela');
INSERT INTO series_number (municipality) VALUES ('Consolacion');
INSERT INTO series_number (municipality) VALUES ('Cordova');
INSERT INTO series_number (municipality) VALUES ('Daanbantayan');
INSERT INTO series_number (municipality) VALUES ('Dalaguete');
INSERT INTO series_number (municipality) VALUES ('Dumanjug');
INSERT INTO series_number (municipality) VALUES ('Ginatilan');
INSERT INTO series_number (municipality) VALUES ('Liloan');
INSERT INTO series_number (municipality) VALUES ('Madridejos');
INSERT INTO series_number (municipality) VALUES ('Malabuyoc');
INSERT INTO series_number (municipality) VALUES ('Medellin');
INSERT INTO series_number (municipality) VALUES ('Minglanilla');
INSERT INTO series_number (municipality) VALUES ('Moalboal');
INSERT INTO series_number (municipality) VALUES ('Oslob');
INSERT INTO series_number (municipality) VALUES ('Pilar');
INSERT INTO series_number (municipality) VALUES ('Pinamungajan');
INSERT INTO series_number (municipality) VALUES ('Poro');
INSERT INTO series_number (municipality) VALUES ('Ronda');
INSERT INTO series_number (municipality) VALUES ('Samboan');
INSERT INTO series_number (municipality) VALUES ('San Francisco');
INSERT INTO series_number (municipality) VALUES ('San Remigio');
INSERT INTO series_number (municipality) VALUES ('Santa Fe');
INSERT INTO series_number (municipality) VALUES ('Santa Fernando');
INSERT INTO series_number (municipality) VALUES ('Sibonga');
INSERT INTO series_number (municipality) VALUES ('Sogod');
INSERT INTO series_number (municipality) VALUES ('Tabogon');
INSERT INTO series_number (municipality) VALUES ('Tabuelan');
INSERT INTO series_number (municipality) VALUES ('Tuburan');
INSERT INTO series_number (municipality) VALUES ('Tudela');
INSERT INTO series_number (municipality) VALUES ('Toledo City');
INSERT INTO series_number (municipality) VALUES ('Cebu Province');
INSERT INTO series_number (municipality) VALUES ('Balamban');
INSERT INTO series_number (municipality) VALUES ('Bantayan');


