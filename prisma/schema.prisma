generator client {
  provider        = "prisma-client-js"
  previewFeatures = ["referentialIntegrity"]
}

datasource db {
  provider             = "postgresql"
  url                  = env("DATABASE_URL")
  referentialIntegrity = "prisma"
}

model system_users {
  id           Int       @id @default(autoincrement())
  username     String    @unique
  password     String
  permission   String
  municipality String
  lastname     String?
  firstname    String?
  middlename   String?
  created_at   DateTime? @default(now()) @db.Timestamp(6)
  updated_at   DateTime? @default(now()) @db.Timestamp(6)
}

model beneficiary {
  id         Int        @id @default(autoincrement())
  firstName  String
  lastName   String
  middleName String?
  address    String
  hospital   String
  gender     String
  class      String
  status     String
  created_at DateTime?  @default(now()) @db.Timestamp(6)
  updated_at DateTime?  @default(now()) @db.Timestamp(6)
  sessions   sessions[]
}

model items {
  id           Int       @id @default(autoincrement())
  itemName     String
  unitMeasure  String
  unitPrice    Float
  hospital     String
  isMedtronics Int       @default(0)
  status       String    @default("Active")
  created_at   DateTime? @default(now()) @db.Timestamp(6)
  updated_at   DateTime? @default(now()) @db.Timestamp(6)

  @@index([itemName], name: "idx_items_itemName")
  @@index([hospital], name: "idx_items_hospital")
}

model sessions {
  id                   Int                 @id @default(autoincrement())
  beneficiary          beneficiary         @relation(fields: [beneficiary_id], references: [id])
  beneficiary_id       Int
  hospital             String
  philhealth_deduction Float               @default(2600.00)
  aics_deduction       Float               @default(0)
  maep_deduction       Float               @default(0)
  cash                 Float               @default(0)
  qfs                  Float               @default(0)
  pcso                 Float               @default(0)
  is_deleted           Int                 @default(0)
  created_at           DateTime?           @default(now()) @db.Timestamp(6)
  updated_at           DateTime?           @default(now()) @db.Timestamp(6)
  sessions_itemUsed    sessions_itemUsed[]

  @@index([beneficiary_id], name: "idx_beneficiary_id")
  @@index([hospital], name: "idx_hospital")
}

model sessions_itemUsed {
  id           Int       @id @default(autoincrement())
  sessions     sessions  @relation(fields: [sessions_id], references: [id])
  sessions_id  Int
  itemName     String
  unitMeasure  String
  unitPrice    Float
  quantity     Float
  isMedtronics Int       @default(0)
  is_deleted   Int       @default(0)
  created_at   DateTime? @default(now()) @db.Timestamp(6)
  updated_at   DateTime? @default(now()) @db.Timestamp(6)

  @@index([sessions_id], name: "idx_sessions_id")
  @@index([itemName], name: "idx_itemName")
}

model hospital {
  id           Int       @id @default(autoincrement())
  hospitalName String
  created_at   DateTime? @default(now()) @db.Timestamp(6)
  updated_at   DateTime? @default(now()) @db.Timestamp(6)

  @@index([hospitalName], name: "idx_hospital_hospitalName")
}

model statement {
  id             Int       @id @default(autoincrement())
  statement_code String
  fullName       String
  hospital       String
  usedMedtronics Int
  UnitPrice      Float
  totalAmount    Float
  class          String?
  DateFrom       DateTime
  DateTo         DateTime
  created_at     DateTime
  updated_at     DateTime? @default(now()) @db.Timestamp(6)

  @@index([fullName], name: "idx_statementFullName")
  @@index([hospital], name: "idx_statementHospital")
}

// sensus
model sensus_patients {
  id                      Int                       @id @default(autoincrement())
  firstName               String
  lastName                String
  middleName              String?
  hospital                String
  status                  String
  classification          String
  is_deleted              Int                       @default(0)
  deleted_by              String?
  created_by              String
  created_at              DateTime?                 @default(now()) @db.Timestamp(6)
  updated_at              DateTime?                 @default(now()) @db.Timestamp(6)
  sensus_transactions     sensus_transactions[]
  laboratory_transactions laboratory_transactions[]

  @@index([firstName], name: "idx_sensus_patients_firstName")
  @@index([lastName], name: "idx_sensus_patients_lastName")
  @@index([hospital], name: "idx_sensus_patients_hospital")
}

//
model sensus_transactions {
  id                   Int             @id @default(autoincrement())
  sensus_patients      sensus_patients @relation(fields: [sensus_patients_id], references: [id])
  sensus_patients_id   Int
  patients_name        String
  area                 String
  hospital             String
  classification       String
  discount_senior      Float           @default(0)
  discount_pwd         Float           @default(0)
  philhealth_deduction Float           @default(0)
  maip_deduction       Float           @default(0)
  aics_deduction       Float           @default(0)
  himsugbo             Float           @default(0)
  malaskit             Float           @default(0)
  pcso                 Float           @default(0)
  cash                 Float           @default(0)
  qfs                  Float           @default(0)
  bill_amount          Float           @default(0)
  promisri_note        Float           @default(0)
  is_deleted           Int             @default(0)
  deleted_by           String?
  created_by           String
  bill_date            DateTime?
  created_at           DateTime?       @default(now()) @db.Timestamp(6)
  updated_at           DateTime?       @default(now()) @db.Timestamp(6)

  @@index([sensus_patients_id], name: "idx_sensus_transactions_patients_id")
  @@index([patients_name], name: "idx_sensus_transactions_patients_name")
  @@index([hospital], name: "idx_sensus_transactions_hospital")
  @@index([bill_date], name: "idx_sensus_transactions_bill_date")
}

//Laboratory
model laboratory_transactions {
  id                     Int               @id @default(autoincrement())
  sensus_patients        sensus_patients   @relation(fields: [laboratory_patients_id], references: [id])
  laboratory_patients_id Int
  laboratory_used        laboratory_used[]
  patients_name          String
  classification         String
  hospital               String
  remarks                String?
  lab_type               String
  area                   String
  discount_senioer       Float             @default(0)
  discount_pwd           Float             @default(0)
  philhealth_deduction   Float             @default(0)
  maip_deduction         Float             @default(0)
  aics_deduction         Float             @default(0)
  himsugbo               Float             @default(0)
  malaskit               Float             @default(0)
  pcso                   Float             @default(0)
  cash                   Float             @default(0)
  qfs                    Float             @default(0)
  bill_amount            Float             @default(0)
  used                   Int               @default(0)
  promisri_note          Float             @default(0)
  is_deleted             Int               @default(0)
  deleted_by             String?
  created_by             String
  bill_date              DateTime
  created_at             DateTime?         @default(now()) @db.Timestamp(6)
  updated_at             DateTime?         @default(now()) @db.Timestamp(6)

  @@index([laboratory_patients_id], name: "idx_lab_transactions_patients_id")
  @@index([patients_name], name: "idx_lab_transactions_patients_name")
  @@index([bill_date], name: "idx_lab_transactions_bill_date")
}

model laboratory_used {
  id                        Int                      @id @default(autoincrement())
  lab_items                 lab_items                @relation(fields: [lab_itemsId], references: [id])
  lab_itemsId               Int
  laboratory_transactions   laboratory_transactions? @relation(fields: [laboratory_transactionsId], references: [id])
  laboratory_transactionsId Int
  amount                    Float
  item_name                 String
  bill_date                 DateTime
  created_at                DateTime?                @default(now()) @db.Timestamp(6)
  is_deleted                Int                      @default(0)
  deleted_by                String?

  @@index([lab_itemsId], name: "idx_laboratory_used_lab_itemsId")
  @@index([laboratory_transactionsId], name: "idx_laboratory_used_laboratory_transactionsId")
  @@index([bill_date], name: "idx_laboratory_used_bill_date")
}

model lab_items {
  id              Int               @id @default(autoincrement())
  name            String
  type            String
  hospital        String?
  amount          Float             @default(0)
  is_deleted      Int               @default(0)
  deleted_by      String?
  created_at      DateTime?         @default(now()) @db.Timestamp(6)
  updated_at      DateTime?         @default(now()) @db.Timestamp(6)
  laboratory_used laboratory_used[]

  @@index([name], name: "idx_lab_items_name")
  @@index([type, hospital], name: "idx_lab_items_type_hospital")
}

model philhealth_management {
  id                          Int       @id @default(autoincrement())
  january_total_denied        Int       @default(0)
  january_denied_amount       Float     @default(0)
  january_total_returned      Int       @default(0)
  january_returned_amount     Float     @default(0)
  january_total_onprocess     Int       @default(0)
  january_onprocess_amount    Float     @default(0)
  january_total_successful    Int       @default(0)
  january_successful_amount   Float     @default(0)
  february_total_denied       Int       @default(0)
  february_denied_amount      Float     @default(0)
  february_total_returned     Int       @default(0)
  february_returned_amount    Float     @default(0)
  february_total_onprocess    Int       @default(0)
  february_onprocess_amount   Float     @default(0)
  february_total_successful   Int       @default(0)
  february_successful_amount  Float     @default(0)
  march_total_denied          Int       @default(0)
  march_denied_amount         Float     @default(0)
  march_total_returned        Int       @default(0)
  march_returned_amount       Float     @default(0)
  march_total_onprocess       Int       @default(0)
  march_onprocess_amount      Float     @default(0)
  march_total_successful      Int       @default(0)
  march_successful_amount     Float     @default(0)
  april_total_denied          Int       @default(0)
  april_denied_amount         Float     @default(0)
  april_total_returned        Int       @default(0)
  april_returned_amount       Float     @default(0)
  april_total_onprocess       Int       @default(0)
  april_onprocess_amount      Float     @default(0)
  april_total_successful      Int       @default(0)
  april_successful_amount     Float     @default(0)
  may_total_denied            Int       @default(0)
  may_denied_amount           Float     @default(0)
  may_total_returned          Int       @default(0)
  may_returned_amount         Float     @default(0)
  may_total_onprocess         Int       @default(0)
  may_onprocess_amount        Float     @default(0)
  may_total_successful        Int       @default(0)
  may_successful_amount       Float     @default(0)
  june_total_denied           Int       @default(0)
  june_denied_amount          Float     @default(0)
  june_total_returned         Int       @default(0)
  june_returned_amount        Float     @default(0)
  june_total_onprocess        Int       @default(0)
  june_onprocess_amount       Float     @default(0)
  june_total_successful       Int       @default(0)
  june_successful_amount      Float     @default(0)
  july_total_denied           Int       @default(0)
  july_denied_amount          Float     @default(0)
  july_total_returned         Int       @default(0)
  july_returned_amount        Float     @default(0)
  july_total_onprocess        Int       @default(0)
  july_onprocess_amount       Float     @default(0)
  july_total_successful       Int       @default(0)
  july_successful_amount      Float     @default(0)
  august_total_denied         Int       @default(0)
  august_denied_amount        Float     @default(0)
  august_total_returned       Int       @default(0)
  august_returned_amount      Float     @default(0)
  august_total_onprocess      Int       @default(0)
  august_onprocess_amount     Float     @default(0)
  august_total_successful     Int       @default(0)
  august_successful_amount    Float     @default(0)
  september_total_denied      Int       @default(0)
  september_denied_amount     Float     @default(0)
  september_total_returned    Int       @default(0)
  september_returned_amount   Float     @default(0)
  september_total_onprocess   Int       @default(0)
  september_onprocess_amount  Float     @default(0)
  september_total_successful  Int       @default(0)
  september_successful_amount Float     @default(0)
  october_total_denied        Int       @default(0)
  october_denied_amount       Float     @default(0)
  october_total_returned      Int       @default(0)
  october_returned_amount     Float     @default(0)
  october_total_onprocess     Int       @default(0)
  october_onprocess_amount    Float     @default(0)
  october_total_successful    Int       @default(0)
  october_successful_amount   Float     @default(0)
  november_total_denied       Int       @default(0)
  november_denied_amount      Float     @default(0)
  november_total_returned     Int       @default(0)
  november_returned_amount    Float     @default(0)
  november_total_onprocess    Int       @default(0)
  november_onprocess_amount   Float     @default(0)
  november_total_successful   Int       @default(0)
  november_successful_amount  Float     @default(0)
  december_total_denied       Int       @default(0)
  december_denied_amount      Float     @default(0)
  december_total_returned     Int       @default(0)
  december_returned_amount    Float     @default(0)
  december_total_onprocess    Int       @default(0)
  december_onprocess_amount   Float     @default(0)
  december_total_successful   Int       @default(0)
  december_successful_amount  Float     @default(0)
  hospital                    String
  year                        String
  is_deleted                  Int       @default(0)
  deleted_by                  String?
  created_at                  DateTime? @default(now()) @db.Timestamp(6)
  updated_at                  DateTime? @default(now()) @db.Timestamp(6)
}
