/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        "docs-order": "url('/lrx.jpg')",
        prescription: "url('/qrx.jpg')",
      },
      spacing: {
        18: "72px",
        35: "130px",
        36: "146px",
        37: "147px",
        42: "168px",
        43: "187px",
        68: "265px",
        73: "280px",
        98: "535px",
        99: "545px",
        100: "650px",
      },
      fontSize: {
        mlg: "1.100rem",
        xmidl: "1.550rem",
      },
      colors: {
        'custom-blue': '#001529',
        'custom-yellow': '#FFD700',
        'custom-orange': '#FFA500',
        'custom-green': '#28A745',
        'custom-light-gray': '#F0F0F0',
      },
    },
  },
  plugins: [],
};
